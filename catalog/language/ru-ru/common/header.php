<?php
// Text
$_['text_home']          = 'Главная';
$_['text_wishlist']      = 'Закладки (%s)';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'Транзакции';
$_['text_download']      = 'Загрузки';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Смотреть Все';

$_['text_blog'] = 'Блог';
$_['text_about_us'] = 'О нас';
$_['text_delivery'] = 'Доставка';
$_['text_payments'] = 'Оплата';
$_['text_worktime'] = '10:00 - 19:00';
$_['text_works'] = 'Сегодня мы %s работаем';
$_['text_not'] = 'не';

$_['text_beta'] = 'Сайт работает в тестовом режиме';

$_['text_holiday_deliver'] = '16 октября у нас выходной. Доставка с 17.10';