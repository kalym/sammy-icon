<?php
// Text
$_['text_items']    = 'Товаров %s (%s)';
$_['text_empty']    = 'Ваша корзина пуста!';
$_['text_cart']     = 'Перейти в корзину';
$_['text_checkout'] = 'Оформить';
$_['text_quantity'] = 'Количество';
$_['text_recurring']  = 'Платежный профиль';

