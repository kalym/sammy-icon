<?php
// Text
$_['text_information']  = 'Контакты';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Обратная связь';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнерская программа';
$_['text_special']      = 'Акции';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Закладки';
$_['text_newsletter']   = 'Рассылка';
$_['text_powered']      = 'Работает на <a href="//opencart-russia.ru">OpenCart "Русская сборка"</a><br /> %s &copy; %s';

$_['text_shop']      = 'Магазин';
$_['text_shops']      = 'Наши магазины';
$_['text_addresses']      = 'Адреса магазинов';
$_['text_partners']      = 'Стать партнером';

$_['text_blog']      = 'Блог';
$_['text_history']      = 'История';
$_['text_lookbook']      = 'Lookbook';
$_['text_brand']      = 'Бренд';

$_['text_worktime'] = 'Наш Call - Центр работает:';
$_['text_time'] = 'Пн - Пт: 10:00 - 19:00';
$_['text_days'] = 'Сб - Вс: Выходные';

$_['text_subscribe'] = 'Подпишись на новости';
$_['text_subscribe_button'] = 'Подпишись';
$_['text_subscribe_placeholder'] = 'Твой e-mail';
$_['text_payments_methods'] = 'Способы оплаты:';