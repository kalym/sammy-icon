<?php
// Text
$_['text_new_subject']          = '%s - Заказ %s';
$_['text_new_greeting']         = '<p>Привет %s, спасибо за заказ! </p>
<p>В течение одного рабочего дня наш внимательный консультант свяжется с Вами, чтобы уточнить адрес и способ доставки!</p>
<p>Напоминаем, команда Sammy Icon работает с понедельника по пятницу с 10 до 19 часов.</p>
<p>Если у вас есть есть срочный вопрос, звоните по телефону (044) 221-43-63 или +38 (073) 419 94 38.</p>';
$_['text_new_received']         = 'Вы получили заказ.';
$_['text_new_link']             = 'Для просмотра Вашего заказа перейдите по ссылке:';
$_['text_new_order_detail']     = 'Детализация заказа';
$_['text_new_instruction']      = 'Инструкции';
$_['text_new_order_id']         = '№ заказа:';
$_['text_new_date_added']       = 'Дата заказа:';
$_['text_new_order_status']     = 'Состояние заказа:';
$_['text_new_payment_method']   = 'Способ оплаты:';
$_['text_new_shipping_method']  = 'Способ доставки:';
$_['text_new_email']  			= 'E-mail:';
$_['text_new_telephone']  		= 'Телефон:';
$_['text_new_ip']  				= 'IP-адрес:';
$_['text_new_payment_address']  = 'Адрес плательщика';
$_['text_new_shipping_address'] = 'Адрес доставки';
$_['text_new_products']         = 'Товары';
$_['text_new_product']          = 'Товар';
$_['text_new_model']            = 'Модель';
$_['text_new_quantity']         = 'Количество';
$_['text_new_price']            = 'Цена';
$_['text_new_order_total']      = 'Заказ итого:';
$_['text_new_total']            = 'Итого:';
$_['text_new_download']         = 'После подтверждения оплаты, загружаемые товары будут доступны по ссылке:';
$_['text_new_comment']          = 'Комментарий к Вашему заказу:';
$_['text_new_footer']           = 'Если у Вас есть какие-либо вопросы, ответьте на это сообщение.';
$_['text_update_subject']       = '%s - Обновление заказа %s';
$_['text_update_order']         = '№ заказа';
$_['text_update_date_added']    = 'Дата заказа:';
$_['text_update_order_status']  = 'Ваш Заказ обновлен со следующим статусом:';
$_['text_update_comment']       = 'Комментарий к Вашему заказу:';
$_['text_update_link']          = 'Для просмотра заказа перейдите по ссылке ниже:';
$_['text_update_footer']        = 'Если у Вас есть какие-либо вопросы, ответьте на это сообщение.';
