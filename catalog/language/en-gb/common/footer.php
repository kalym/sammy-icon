<?php
// Text
$_['text_information']  = 'Contacts';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="//www.opencart.com">OpenCart</a><br /> %s &copy; %s';

$_['text_shop']      = 'Shop';
$_['text_shops']      = 'Our Shops';
$_['text_addresses']      = 'Addresses of shops';
$_['text_partners']      = 'Become a partner';

$_['text_blog']      = 'Blog';
$_['text_history']      = 'History';
$_['text_lookbook']      = 'Lookbook';
$_['text_brand']      = 'Brand';

$_['text_worktime'] = 'Our Call - Center works:';
$_['text_time'] = 'Mon - Fri: 10:00 - 19:00';
$_['text_days'] = 'Sat - Wen: Holidays';

$_['text_subscribe'] = 'Subscribe for newsletter';
$_['text_subscribe_button'] = 'Subscribe';
$_['text_subscribe_placeholder'] = 'Your e-mail';
$_['text_payments_methods'] = 'Payments methods:';