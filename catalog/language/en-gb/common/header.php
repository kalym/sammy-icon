<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';

$_['text_blog']     = 'Blog';
$_['text_about_us'] = 'About us';
$_['text_delivery'] = 'Delivery';
$_['text_payments'] = 'Payment';
$_['text_worktime'] = '10:00 - 19:00';
$_['text_works'] = 'We are open';
$_['text_not'] = 'We\'re shut, please come back on Monday <img src="%s" alt="facebook smile" >';

$_['text_beta'] = 'beta';

$_['text_holiday_deliver'] = 'We have holiday on Oct,16. Deliveries start from 17.10';