<?php
// Heading
$_['heading_title']  = 'Contact Us';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Telephone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';
$_['text_map']       = 'Socks Machine Globus TC';
$_['text_map1']       = 'Socks Machine Gulliver TC';
// Entry
$_['entry_name']     = 'Your Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Your message';
$_['entry_order_id'] = 'Order number';
$_['entry_subject']  = 'Topic';

// Email
$_['email_subject']  = 'Enquiry %s';


// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';

$_['subjects']       = 'Order status|Website|Distribution|Media&PR partnership|Service quality|Other';
