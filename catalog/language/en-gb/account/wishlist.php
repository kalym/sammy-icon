<?php
// Heading
$_['heading_title'] = 'My Wish List';

// Text
$_['text_account']  = 'Account';
$_['text_instock']  = 'In Stock';
$_['text_wishlist'] = 'Wish List (%s)';
$_['text_login']    = 'You must <a href="%s">login</a> or <a href="%s">create an account</a> to save <a href="%s">%s</a> to your <a href="%s">wish list</a>!';
$_['text_success']  = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">wish list</a>!';
$_['text_remove']   = 'Success: You have modified your wish list!';
$_['text_empty']    = 'Your wish list is empty.';

// Column
$_['column_image']  = 'Image';
$_['column_name']   = 'Product Name';
$_['column_model']  = 'Model';
$_['column_stock']  = 'Stock';
$_['column_price']  = 'Unit Price';
$_['column_action'] = 'Action';

$_['text_wishlist_label'] = 'Add your favourite products in wishlist. Here they will not get lost and you will be able to buy them later';
$_['back_to_store'] = 'Back to store';
