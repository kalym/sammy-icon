<?php
// Heading
$_['heading_title']        = 'Your order has been placed!';

// Text
$_['text_basket']          = 'Shopping Cart';
$_['text_checkout']        = 'Checkout';
$_['text_success']         = 'Success';
$_['text_customer']        = '<p class="top-text">Номер вашого замовлення <span class="color">#%s</span></p>
                                <p class="middle-text">
                                    Нагадаємо, що ми працюємо:<br/>
                                    ПН-ПТ 10:00-19:00<br/>
                                    СБ-НД вихідні дні
                                </p>
                                <p>
                                    Замовлення, що оформлені на кур’єрську доставку до 15:00 будуть доставлені  наступного дня.
                                    Замовлення, оформлені на доставку Новою поштою до 16:00 будуть відправлені в день замовлення.
                                    Міжнародні відправки будуть зібрані та відправлені протягом 48 годин з моменту замовлення.
                                </p>
                                <p class="bottom-text">Сума до сплати: <span class="bold">%s</span><br/></p>';
$_['text_guest']           = '<p>Your order has been successfully processed!</p><p>Please direct any questions you have to the <a href="%s"><strong>store owner</strong></a>.</p><p>Thanks for shopping with us online!</p>';