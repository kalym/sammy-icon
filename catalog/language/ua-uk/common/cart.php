<?php
// Text
$_['text_items']    = '%s товар (ів) - %s';
$_['text_empty']    = 'Ваш кошик порожній!';
$_['text_cart']     = 'Переглянути кошик';
$_['text_checkout'] = 'Оформити';
$_['text_quantity'] = 'Кількість';
$_['text_recurring']  = 'Профілі Оплати';