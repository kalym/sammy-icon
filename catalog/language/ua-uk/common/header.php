<?php
// Text
$_['text_home']          = 'Головна';
$_['text_wishlist']      = 'Список побажань (%s)';
$_['text_shopping_cart'] = 'Кошик';
$_['text_category']      = 'Категорії';
$_['text_account']       = 'Мої данні';
$_['text_register']      = 'Зареєструватись';
$_['text_login']         = 'Увійти';
$_['text_order']         = 'Журнал замовлень';
$_['text_transaction']   = 'Сплаченні рахунки';
$_['text_download']      = 'Завантаження';
$_['text_logout']        = 'Вийти';
$_['text_checkout']      = 'Оформити';
$_['text_search']        = 'Пошук';
$_['text_all']           = 'Переглянути всі';

$_['text_blog']     = 'Блог';
$_['text_about_us'] = 'Про нас';
$_['text_delivery'] = 'Доставка';
$_['text_payments'] = 'Оплата';
$_['text_worktime'] = '10:00 - 19:00';
$_['text_works']    = 'Сьогодні ми %s працюємо';
$_['text_not']      = 'не';

$_['text_beta'] = 'Сайт працює у тестовому режимі';

$_['text_holiday_deliver'] = '16 жовтня у нас вихідний. Доставка з 17.10';