<?php
// Text
$_['text_information']  = 'Контакти';
$_['text_service']      = 'Обслуговування клієнтів';
$_['text_extra']        = 'Додатково';
$_['text_contact']      = 'Наші контакти';
$_['text_return']       = 'Повернення';
$_['text_sitemap']      = 'Карта сайту';
$_['text_manufacturer'] = 'Бренди';
$_['text_voucher']      = 'Подарункові ваучери';
$_['text_affiliate']    = 'Партнери';
$_['text_special']      = 'Акції';
$_['text_account']      = 'Мої данні';

$_['text_shop']      = 'Магазин';
$_['text_shops']      = 'Наші магазини';
$_['text_addresses']      = 'Адреси магазинів';
$_['text_partners']      = 'Стати партнером';

$_['text_blog']      = 'Блог';
$_['text_history']      = 'Історія';
$_['text_lookbook']      = 'Lookbook';
$_['text_brand']      = 'Бренд';

$_['text_order']        = 'Журнал замовлень';
$_['text_wishlist']     = 'Список побажань';
$_['text_newsletter']   = 'Стрічка новин';
$_['text_powered']      = 'Працює на <a href="//www.opencart.com">OpenCart</a>< br / > %s &copy; %s';

$_['text_worktime'] = 'Наш Call - центр працює:';
$_['text_time'] = 'Пн - Пт: 10:00 - 19:00';
$_['text_days'] = 'Сб - Нд: Вихідні';

$_['text_subscribe'] = 'Підпишись на новини';
$_['text_subscribe_button'] = 'Підписатись';
$_['text_subscribe_placeholder'] = 'Ваш e-mail';
$_['text_payments_methods'] = 'Способи оплати:';