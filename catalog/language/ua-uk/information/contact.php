<?php
// Heading
$_['heading_title']  = 'Контакти';

// Text
$_['text_location']  = 'Наше розташування';
$_['text_store']     = 'Наші магазини';
$_['text_contact']   = 'Контактна форма';
$_['text_address']   = 'Адреса';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Час роботи';
$_['text_comment']   = 'Коментарі';
$_['text_success']   = '<p>Ваш запит було успішно надіслано власнику магазину!</p>';
$_['text_map']       = '"Шкарпеткомат" ТЦ Глобус';
$_['text_map1']       = '"Шкарпеткомат" ТЦ Гулівер';

// Entry
$_['entry_name']     = 'Ім’я';
$_['entry_subject'] = 'Тема';
$_['entry_email']    = 'E-mail';
$_['entry_order_id']  = '№ замовлення';
$_['entry_enquiry']  = 'Текст повідомлення';



// Email
$_['email_subject']  = 'Запит %s';

// Errors
$_['error_name']     = 'Ім\'я має бути від 3 до 32 символів!';
$_['error_email']    = 'Вказана адреса електронної пошти неправильна!';
$_['error_enquiry']  = 'Запит має бути від 10 до 3000 символів!';

$_['subjects']       = 'Статус замовлення|Робота сайту|Дистрибуція|Співпраця з медіа|Якість обслуговування|Інше';
