<?php  
//version 2.2.0.0


// header
$_['heading_title']  = 'Скинути пароль';

// Text
$_['text_account']   = 'Обліковий запис';
$_['text_password']  = 'Введіть новий пароль, який ви хочете використовувати.';
$_['text_success']   = 'Успіх: Ваш пароль був успішно оновлено.';

// Entry
$_['entry_password'] = 'Пароль';
$_['entry_confirm']  = 'Підтвердити';

// Error
$_['error_password'] = 'Пароль повинен бути від 4 та 20 символів!';
$_['error_confirm']  = 'Пароль та підтвердження пароля не співпадають!';
$_['error_code']     = 'Код скидання пароля є недійсний або був використаний раніше!';