<?php
class ModelLookbookLookbook extends Model {
	public function getlookbook($lookbook_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) WHERE b.lookbook_id = '" . (int)$lookbook_id . "' AND bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b.status = '1' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		return $query->row;
	}

	public function getlookbooks($limit = 0, $where_tags = array()) {
		$where = '';
		
		if ($where_tags) {
			$i = 1;
			
			foreach ($where_tags as $where_tag) {
				if ($i < count($where_tags)) {
					$where .= $where_tag . ', ';
				} else {
					$where .= $where_tag;
				}
				$i++;
			}
		}

		if ($where) {
			if ($limit == 0) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_tag btt ON (b.lookbook_id = btt.lookbook_id)  WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' AND btt.lookbooktag_id IN (" . $where .") GROUP BY b.lookbook_id ORDER BY b.sort_order, LCASE(bd.title) ASC");
			} else {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_tag btt ON (b.lookbook_id = btt.lookbook_id)  WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' AND btt.lookbooktag_id IN (" . $where .") GROUP BY b.lookbook_id ORDER BY b.sort_order, LCASE(bd.title) ASC LIMIT " . (int)$limit);
			}
		} else {
			if ($limit == 0) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' ORDER BY b.sort_order, LCASE(bd.title) ASC");
			} else {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' ORDER BY b.sort_order, LCASE(bd.title) ASC LIMIT " . (int)$limit);
			}
		}

		return $query->rows;
	}


	public function getlookbooksBottom() {
		$blogs = $this->cache->get('lookbook.bottomblogs.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));
		
		if (!$blogs) {			
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) WHERE b.bottom = '1' AND bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' ORDER BY b.sort_order, LCASE(bd.title) ASC");
			$blogs = $query->rows;
			
			$this->cache->set('lookbook.bottomblogs.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $blogs);

		}

		return $blogs;
	}

	public function getRandomlookbooks($limit = 0, $where_tags = array()) {
		$where = '';
		
		if ($where_tags) {
			$i = 1;
			
			foreach ($where_tags as $where_tag) {
				if ($i < count($where_tags)) {
					$where .= $where_tag . ', ';
				} else {
					$where .= $where_tag;
				}
				$i++;
			}
		}
		
		if ($where) {
			if ($limit == 0) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_tag btt ON (b.lookbook_id = btt.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' AND btt.lookbooktag_id IN (" . $where .") GROUP BY b.lookbook_id ORDER BY b.sort_order, LCASE(bd.title) ASC");
			} else {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_tag btt ON (b.lookbook_id = btt.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' AND btt.lookbooktag_id IN (" . $where .") GROUP BY b.lookbook_id ORDER BY RAND() LIMIT " . (int)$limit);
			}
		} else {
			if ($limit == 0) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' ORDER BY b.sort_order, LCASE(bd.title) ASC");
			} else {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' ORDER BY RAND() LIMIT " . (int)$limit);
			}
		}

		return $query->rows;
	}

	public function getlookbookRelated($lookbook_id) {
		$query = $this->db->query("SELECT related_id FROM " . DB_PREFIX . "lookbook_related br LEFT JOIN " . DB_PREFIX . "product p ON (br.related_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE br.lookbook_id = '" . (int)$lookbook_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.sort_order");

		return $query->rows;
	}

	public function getlookbookLayoutId($lookbook_id) {
		$query = $this->db->query("SELECT layout_id FROM " . DB_PREFIX . "lookbook_to_layout WHERE lookbook_id = '" . (int)$lookbook_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function countlookbooks($where_tags = array()) {
		if (!$where_tags) {
			$count = $this->cache->get('lookbook.count.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));
		
			if (!$count) {			
				$query = $this->db->query("SELECT COUNT(DISTINCT b.lookbook_id) AS total FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1'");

				$count = $query->row['total'];
			
				$this->cache->set('lookbook.count.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $count);
			}
		} else {
			$where = '';
			$i = 1;
			
			foreach ($where_tags as $where_tag) {
				if ($i < count($where_tags)) {
					$where .= $where_tag . ', ';
				} else {
					$where .= $where_tag;
				}
				$i++;
			}
				
			$query = $this->db->query("SELECT COUNT(DISTINCT b.lookbook_id) AS total FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_tag btt ON (b.lookbook_id = btt.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' AND btt.lookbooktag_id IN (" . $where .")");

			$count = $query->row['total'];
		}
		
		return $count;
	}

	public function getlookbooksForTag($lookbooktag_id) {
		$blogs = $this->cache->get('lookbook.blogsfortag.' . (int)$lookbooktag_id . '.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));

		if (!$blogs) {
			$blogs = array();
			
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_tag b2t ON (b.lookbook_id = b2t.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) WHERE b2t.lookbooktag_id = '" . (int)$lookbooktag_id . "' AND bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1' ORDER BY b.sort_order, LCASE(bd.title) ASC");
			
			$blogs = $query->rows;
			$this->cache->set('lookbook.blogsfortag.' . (int)$lookbooktag_id . '.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $blogs);

		}

		return $blogs;
	}

	public function getlookbooktag($lookbooktag_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "lookbooktag t LEFT JOIN " . DB_PREFIX . "lookbooktag_description td ON (t.lookbooktag_id = td.lookbooktag_id) LEFT JOIN " . DB_PREFIX . "lookbooktag_to_store t2s ON (t.lookbooktag_id = t2s.lookbooktag_id) WHERE t.lookbooktag_id = '" . (int)$lookbooktag_id . "' AND td.language_id = '" . (int)$this->config->get('config_language_id') . "' AND t.status = '1' AND t2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		return $query->row;
	}

	public function getAlllookbooktags() {
		$tags = $this->cache->get('lookbook.lookbooktag.all.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));

		if (!$tags) {
			$tags = array();
			
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "lookbooktag t LEFT JOIN " . DB_PREFIX . "lookbooktag_description td ON (t.lookbooktag_id = td.lookbooktag_id) LEFT JOIN " . DB_PREFIX . "lookbooktag_to_store t2s ON (t.lookbooktag_id = t2s.lookbooktag_id) WHERE t.lookbooktag_id = '" . (int)$lookbooktag_id . "' AND td.language_id = '" . (int)$this->config->get('config_language_id') . "' AND t.status = '1' AND t2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
			$tags = $query->rows;
			$this->cache->set('lookbook.lookbooktag.all.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $tags);

		}

		return $tags;
	}

	/* Get only tags, which are linked to any blog entry */
	
	public function getlookbooktags() {
		$tags = $this->cache->get('lookbook.lookbooktag.tags.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));

		if (!$tags) {
			$tags = array();
			
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "lookbooktag t LEFT JOIN " . DB_PREFIX . "lookbooktag_description td ON (t.lookbooktag_id = td.lookbooktag_id) LEFT JOIN " . DB_PREFIX . "lookbooktag_to_store t2s ON (t.lookbooktag_id = t2s.lookbooktag_id) WHERE t.lookbooktag_id IN (SELECT lookbooktag_id FROM " . DB_PREFIX . "lookbook_to_tag) AND t.status = '1' AND td.language_id = '" . (int)$this->config->get('config_language_id') . "' AND t2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
			
			$tags = $query->rows;
			$this->cache->set('lookbook.lookbooktag.tags.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $tags);

		}

		return $tags;
	}

	public function getlookbooktagsForBlog($lookbook_id) {
		$query = $this->db->query("SELECT t.lookbooktag_id AS lookbooktag_id, td.title AS title FROM  " . DB_PREFIX . "lookbook_to_tag b2t LEFT JOIN  " . DB_PREFIX . "lookbooktag t ON (b2t.lookbooktag_id = t.lookbooktag_id) LEFT JOIN  " . DB_PREFIX . "lookbooktag_description td ON (t.lookbooktag_id = td.lookbooktag_id) LEFT JOIN  " . DB_PREFIX . "lookbooktag_to_store t2s ON (t.lookbooktag_id = t2s.lookbooktag_id) WHERE b2t.lookbook_id = '" . (int)$lookbook_id . "' AND t.status = '1' AND td.language_id = '" . (int)$this->config->get('config_language_id') . "' AND t2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY t.sort_order, LCASE(td.title) ASC");

		return $query->rows;
	}

	public function getTotallookbooktags() {
		$count = $this->cache->get('lookbook.lookbooktag.count.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));

		if (!$count) {			
			$query = $this->db->query("SELECT COUNT(DISTINCT b.lookbook_id) AS total FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) LEFT JOIN " . DB_PREFIX . "lookbook_to_store b2s ON (b.lookbook_id = b2s.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND b2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND b.status = '1'");
			
			$count = $query->row['total'];
			
			$this->cache->set('lookbook.lookbooktag.count.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $count);
		}
		
		return $count;
	}


}
