<?php
class ModelShippingWworld extends Model {
	function getQuote() {
		$this->load->language('shipping/wworld');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('wworld_geo_zone_id') . "'");

		if (!$this->config->get('wworld_geo_zone_id') AND ( isset($this->request->cookie['customer_country']) && $this->request->cookie['customer_country']!='Ukraine')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['wworld'] = array(
				'code'         => 'wworld.wworld',
				'title'        => $this->language->get('text_description'),
				'cost'         => $this->config->get('wworld_cost'),
				'tax_class_id' => $this->config->get('wworld_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('wworld_cost'), $this->config->get('wworld_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
			);

			$method_data = array(
				'code'       => 'wworld',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('wworld_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}
}