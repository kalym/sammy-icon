<?php

/**
 * OpenCart Ukrainian Community
 * Made in Ukraine
 *
 * LICENSE
 *
 * This source file is subject to the GNU General Public License, Version 3
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/copyleft/gpl.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 *
 * @category   OpenCart
 * @package    OCU Nova Poshta
 * @copyright  Copyright (c) 2011 Eugene Lifescale (a.k.a. Shaman) by OpenCart Ukrainian Community (http://opencart-ukraine.tumblr.com)
 * @license    http://www.gnu.org/copyleft/gpl.html     GNU General Public License, Version 3
 * @version    $Id: catalog/model/shipping/ocu_ukrposhta.php 1.2 2014-12-27 19:18:40
 */
/**
 * @category   OpenCart
 * @package    OCU OCU Nova Poshta
 * @copyright  Copyright (c) 2011 Eugene Lifescale (a.k.a. Shaman) by OpenCart Ukrainian Community (http://opencart-ukraine.tumblr.com)
 * @license    http://www.gnu.org/copyleft/gpl.html     GNU General Public License, Version 3
 */

class ModelShippingNovaPoshta extends Model {

    function getQuote() {
        $this->load->language('shipping/novaposhta');

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('novaposhta_geo_zone_id') . "'");

        if (!$this->config->get('novaposhta_geo_zone_id') AND $this->request->cookie['customer_country']=='Ukraine' or !isset($this->request->cookie['customer_country'])) {
            $status = true;
        } elseif ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }

        $method_data = array();

        if ($status) {
            $quote_data = array();

            $quote_data['novaposhta'] = array(
                'code'         => 'novaposhta.novaposhta',
                'title'        => $this->language->get('text_description'),
                'cost'         => $this->config->get('novaposhta_cost'),
                'tax_class_id' => $this->config->get('novaposhta_tax_class_id'),
                'text'         => $this->currency->format($this->tax->calculate($this->config->get('novaposhta_cost'), $this->config->get('novaposhta_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
            );

            $method_data = array(
                'code'       => 'novaposhta',
                'title'      => $this->language->get('text_title'),
                'quote'      => $quote_data,
                'sort_order' => $this->config->get('novaposhta_sort_order'),
                'error'      => false
            );
        }

        return $method_data;
    }
}

