<?php
class ModelShippingfast extends Model {
	function getQuote() {
		$this->load->language('shipping/fast');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('fast_geo_zone_id') . "'");

		if (!$this->config->get('fast_geo_zone_id') AND $this->request->cookie['customer_country']=='Ukraine' or !isset($this->request->cookie['customer_country'])) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['fast'] = array(
				'code'         => 'fast.fast',
				'title'        => $this->language->get('text_description'),
				'cost'         => $this->config->get('fast_cost'),
				'tax_class_id' => $this->config->get('fast_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('fast_cost'), $this->config->get('fast_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
			);

			$method_data = array(
				'code'       => 'fast',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('fast_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}
}