<?php
class ModelPaymentLiqPay extends Model {
	public function getMethod($address, $total) {
		$this->load->language('payment/liqpay');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('liqpay_geo_zone_id') . "'");

		if ($this->config->get('liqpay_total') > 0 && $this->config->get('liqpay_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('liqpay_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$method_data = array(
				'code'       => 'liqpay',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('liqpay_sort_order')
			);
		}

		return $method_data;
	}
}