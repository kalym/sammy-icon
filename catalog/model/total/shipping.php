<?php
class ModelTotalShipping extends Model {
	public function getTotal($total) {
		if ($this->cart->hasShipping() && isset($this->session->data['shipping_method'])) {

			$cost = 0;
			$payableShipping = array('fast.fast', 'world.world', 'wworld.wworld');

			if($total['total'] < $this->config->get('fixed_free_total')
                or in_array($this->session->data['shipping_method']['code'],$payableShipping)){
				$cost = $this->session->data['shipping_method']['cost'];
				$total['total'] += $this->session->data['shipping_method']['cost'];
			}

			$total['totals'][] = array(
				'code'       => 'shipping',
				'title'      => $this->session->data['shipping_method']['title'],
				'value'      => $cost,
				'sort_order' => $this->config->get('shipping_sort_order')
			);

			if ($this->session->data['shipping_method']['tax_class_id']) {
				$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);

				foreach ($tax_rates as $tax_rate) {
					if (!isset($total['taxes'][$tax_rate['tax_rate_id']])) {
						$total['taxes'][$tax_rate['tax_rate_id']] = $tax_rate['amount'];
					} else {
						$total['taxes'][$tax_rate['tax_rate_id']] += $tax_rate['amount'];
					}
				}
			}

		}
	}
}