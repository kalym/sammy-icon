<?php  if($preview){ ?>

<div class="item-page">
    <div class=" buy-wrapp-block">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12  buy-left-part mobile-hide">
                <?php if ($thumb || $images) { ?>
                <script type="text/javascript">
                    $('#productCarusel').owlCarousel({
                        items : 1,
                        responsive:false,
                        autoPlay : false,
                        navigation : true,
                        navigationText : ['<img src="/catalog/view/theme/default/image/icon/arrow.png" class="l-arrow">', '<img src="/catalog/view/theme/default/image/icon/arrow.png" class="r-arrow">'],
                        pagination : false
                    });
                </script>
                <div class=" slide-block owl-carousel" id="productCarusel">
                    <?php if ($thumb) { ?>
                    <div class="item">
                        <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>"
                             alt="<?php echo $heading_title; ?>"/>
                    </div>
                    <?php } ?>
                    <?php if ($images) { ?>
                    <?php foreach ($images as $image) { ?>
                    <div class=" item">
                        <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>"
                             alt="<?php echo $heading_title; ?>"/>
                    </div>
                    <?php } ?>
                    <?php } ?>
                </div>

                <?php } ?>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 buy-right-part">
                <div class="col-md-12 mobile-hide">
                    <h1><?php echo $heading_title; ?></h1>
                </div>
                <div class="col-md-12 mobile-hide">
                    <div class="separator"></div>
                </div>

                <div class="col-md-11 mobile-hide">
                    <p class="buy-text-1"><?php echo (mb_strlen(strip_tags($description),'utf-8') > 200) ? mb_substr(strip_tags($description),0,200,'utf-8').'...' : strip_tags($description) ; ?></p>
                </div>

                <div class="row buy-block" id="product">
                    <div class="col col-md-4 col-sm-4 col-xs-4 price-elems mobile-hide">
                        <?php if ($price) { ?>
                        <p class="text"><?php echo $text_price; ?></p>

                        <p class="number"><?php echo $price; ?></p>
                        <?php } ?>
                      <!--  <p class="text first_delivery"><?php echo $first_delivery; ?></p>  -->
                    </div>
                    <div class="col col-md-4 col-sm-4 col-xs-4 price-amount">
                        <p class="text"><?php echo $entry_qty; ?></p>

                        <div class="number">
                            <span class="before"></span>
                            <span class="after"></span>
                            <input title="quantity"  name="quantity" type="text" id="input-quantity" value="1" >
                            <button class="before"></button>
                            <button class="after"></button>
                        </div>
                    </div>

                        <?php if ($options) { ?>
                        <?php foreach ($options as $option) { ?>
                        <?php if ($option['type'] == 'select') { ?>
                    <div class="col col-md-4 col-sm-4 col-xs-4 price-size <?php echo $option['product_option_id']; ?>"><p class="text"><?php echo $option['name']; ?></p>
                        <select class="number" name="option[<?php echo $option['product_option_id']; ?>]"
                                id="input-option<?php echo $option['product_option_id']; ?>">
                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                <?php if ($option_value['price']) { ?>
                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                <?php } ?>
                            </option>
                            <?php } ?>
                        </select></div>
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>

                </div>
                <div class="row">
                    <div class="buy-whishlist">
                        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                        <div class="col-xs-12 ">
                            <button type="button" id="button-cart"
                                data-loading-text="<?php //echo $text_loading; ?>">
                                <?php echo $button_cart; ?>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="row mobile-hide">
                    <div class="col col-xs-12">
                        <a href="<?php echo $this_url; ?>" class="show-more" ><?php echo $text_more; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var product_id = "<?php echo $product_id ?>";
    var product_url = "<?php echo $product['href']; ?>";
    var product_name = "<?php echo $product['name']; ?>";
    convead('event', 'view_product', {
        product_id: product_id,
        product_name: product_name,
        product_url: product_url
    });
</script>

<script>
    fbq('track', 'ViewContent');
</script>
<script type="text/javascript"><!--

    $('.number .before, .number .after').on('click', function () {
        var quantityInput = $('#input-quantity');
       // console.log($(this).hasClass('after') && (quantityInput.val() * 1 - 1) > 0);
        if ($(this).hasClass('before')) {
            quantityInput.val(quantityInput.val() * 1 + 1);
        } else if ($(this).hasClass('after') && (quantityInput.val() * 1 - 1) > 0) {
            quantityInput.val(quantityInput.val() * 1 - 1);
        }
    });

    $('#button-cart').on('click', function () {
        $.ajax({
            url : 'index.php?route=checkout/cart/add',
            type : 'post',
            data : $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType : 'json',
            complete : function () {
                $('#button-cart').button('reset');
            },
            success : function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }else{
                        $('#content').prepend('<div class="alert alert-danger">' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        $('#product-preview').hide();
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                        $('html, body').animate({ scrollTop : 0 }, 'slow');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('#content').prepend('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#product-preview').hide();
                    $('#cart > #cart-total').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                    location = 'index.php?route=checkout/cart';

                    //event trackers
                    fbq('track', 'AddToCart');
                    //window.yaCounter41685499.reachGoal('AddToCart');
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'quick-order',
                        eventAction: 'ajax-cart',
                        eventLabel: 'add-to-cart-big'
                    });
                }
            },
            error : function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>


<?php  }else{ ?>


<?php echo $header; ?>
<div class="container">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> nopadding">
            <div class="item-page">
                <div class=" buy-wrapp-block">
                    <div class="row">

                        <?php if ($column_left || $column_right) { ?>
                        <?php $class = 'col-sm-5'; ?>
                        <?php } else { ?>
                        <?php $class = 'col-md-7'; ?>
                        <?php } ?>
                        <div class="<?php echo $class; ?> buy-left-part">
                            <?php if ($thumb || $images) { ?>


                            <div class=" slide-block owl-carousel" id="productCarusel">

                                <?php if ($thumb) { ?>
                                <div class="item">
                                    <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>"
                                         alt="<?php echo $heading_title; ?>"/>
                                </div>
                                <?php } ?>
                                <?php if ($images) { ?>
                                <?php foreach ($images as $image) { ?>
                                <div class=" item">
                                    <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>"
                                         alt="<?php echo $heading_title; ?>"/>
                                </div>
                                <?php } ?>
                                <?php } ?>


                            </div>


                            <script type="text/javascript">
                                $('#productCarusel').owlCarousel({
                                    items : 1,
                                    responsive:false,
                                    autoPlay : false,
                                    navigation : true,
                                    navigationText : [
                                        '<img src="catalog/view/theme/default/image/icon/arrow.png" class="l-arrow">',
                                        '<img src="catalog/view/theme/default/image/icon/arrow.png" class="r-arrow">'
                                    ],
                                    pagination : false
                                });
                            </script>

                            <?php } ?>
                        </div>

                        <?php if ($column_left || $column_right) { ?>
                        <?php $class = 'col-sm-5'; ?>
                        <?php } else { ?>
                        <?php $class = 'col-md-5'; ?>
                        <?php } ?>
                        <div class="<?php echo $class; ?> buy-right-part">

                            <h1><?php echo $heading_title; ?></h1>

                            <div class="separator"></div>
                            <p class="buy-text-1"><?php echo $description; ?></p>

                            <div class="row buy-block" id="product">
                                <div class="col col-md-4 col-sm-4 col-xs-6 price-elems">
                                    <?php if ($price) { ?>
                                    <p class="text"><?php echo $text_price; ?></p>

                                    <p class="number"><?php echo $price; ?></p>
                                    <?php } ?>
                                   <?php if($category_id == 77): ?>
                                    <p class="text first_delivery"><?php echo $first_delivery; ?></p>
                                    <?php endif ?>
                                </div>
                                <div class="col col-md-4 col-sm-4 col-xs-6 price-amount">
                                    <p class="text"><?php echo $entry_qty; ?></p>

                                    <div class="number">
                                        <span class="before"></span>
                                        <span class="after"></span>
                                        <input name="quantity" type="text" id="input-quantity" value="1"/>
                                    </div>
                                </div>

                                    <?php if ($options) { ?>
                                    <?php foreach ($options as $option) { ?>
                                    <?php if ($option['type'] == 'select') { ?>
                                <div class="col col-md-4 col-sm-4 col-xs-6 price-size"><p class="text"><?php echo $option['name']; ?></p>
                                    <select name="option[<?php echo $option['product_option_id']; ?>]"
                                            id="input-option<?php echo $option['product_option_id']; ?>">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                            <?php } ?>
                                        </option>
                                        <?php } ?>
                                    </select></div>
                                    <?php } ?>
                                    <?php } ?>
                                    <?php } ?>
                                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>

                            </div>
                            <div class="buy-whishlist row">
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                                <button type="button" id="button-cart"
                                        data-loading-text="<?php //echo $text_loading; ?>"><?php echo $button_cart; ?></button>
                                <!--<div class="wishlist-btn" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');" >
                                    <span><?php echo $text_button_wishlist; ?></span>
                                </div>-->
                            </div>
                            <div class="buy-bottom-text">
                                <?php if ($attribute_groups) { ?>
                                <p>
                                    <?php foreach ($attribute_groups as $attribute_group) { ?>

                                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>

                                    <span><?php echo $attribute['name']; ?></span>
                                    <?php echo $attribute['text']; ?>

                                    <?php } ?>

                                    <?php } ?>

                                </p>
                                <?php } ?>
                                <p>
                                    <?php echo $text_questions.' '.$telephone; ?>
                                </p>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="free-space-block"></div>
            </div>
        </div>
</div>
<?php echo $content_top; ?>
<div class="container">
    <div class="row">
        <div class="item-page white">
            <div class="more-items-block">
                <?php if ($products) { ?>

                <div class="separator-1"></div>
                <h3><?php echo $text_related; ?></h3>

                <div class="row import-elems">

                    <?php foreach ($products as $product) { ?>
                    <?php if ($column_left && $column_right) { ?>
                    <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
                    <?php } elseif ($column_left || $column_right) { ?>
                    <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
                    <?php } else { ?>
                    <?php $class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
                    <?php } ?>
                    <div class="product-layout product-list <?php echo $class; ?>">
                        <div class="product-thumb goods-block">
                            <div class="image">
                                <a href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                         title="<?php echo $product['name']; ?>" class="goods-img"/>
                                </a>
                            </div>
                            <a href="<?php echo $product['href']; ?>"><img
                                        src="/catalog/view/theme/default/image/icon/review-goods-img.png"
                                        class="review-goods-img" alt="Preview"/></a>
                            <?php if ($product['special']) { ?>
                            <img src="/catalog/view/theme/default/image/icon/sale-img.png" class="sale-img" alt="sale"/>
                            <?php } ?>
                            <div>
                                <div class="caption goods-text-area">
                                    <h2><a href="<?php echo $product['href']; ?>" class="goods-name"><?php echo $product['name']; ?></a></h2>

                                    <?php if ($product['price']) { ?>
                                    <a href="<?php echo $product['href']; ?>" class="price goods-price pull-right">
                                        <?php if (!$product['special']) { ?>
                                        <?php echo $product['price']; ?>
                                        <?php } else { ?>
                                        <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                        <?php } ?>
                                        <?php if ($product['tax']) { ?>
                                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                        <?php } ?>
                                    </a>

                                    <?php } ?>

                                    <p>
                                        <?php  if ($product['options']) {
                                          echo $product['options'];
                                       } ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>

                <?php echo $content_bottom; ?>
            </div>
        </div>

    </div>
    <?php echo $column_right; ?>
</div>

<script type="text/javascript">
    var product_id = "<?php echo $product_id ?>";
    var product_url = "<?php echo $product['href']; ?>";
    var product_name = "<?php echo $product['name']; ?>";
    convead('event', 'view_product', {
        product_id: product_id,
        product_name: product_name,
        product_url: product_url
    });
</script>

<script>
    fbq('track', 'ViewContent');
</script>
<script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
        $.ajax({
            url : 'index.php?route=product/product/getRecurringDescription',
            type : 'post',
            data : $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType : 'json',
            beforeSend : function () {
                $('#recurring-description').html('');
            },
            success : function (json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>

<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime : false
    });

    $('.datetime').datetimepicker({
        pickDate : true,
        pickTime : true
    });

    $('.time').datetimepicker({
        pickDate : false
    });

    $('button[id^=\'button-upload\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url : 'index.php?route=tool/upload',
                    type : 'post',
                    dataType : 'json',
                    data : new FormData($('#form-upload')[0]),
                    cache : false,
                    contentType : false,
                    processData : false,
                    beforeSend : function () {
                        //$(node).button('reset');
                    },
                    complete : function () {
                        $(node).button('reset');
                    },
                    success : function (json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error : function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>

<script type="text/javascript"><!--

    $('.number .before, .number .after').on('click', function () {
        var quantityInput = $('#input-quantity');
        //console.log($(this).hasClass('after') && (quantityInput.val() * 1 - 1) > 0);
        if ($(this).hasClass('before')) {
            quantityInput.val(quantityInput.val() * 1 + 1);
        } else if ($(this).hasClass('after') && (quantityInput.val() * 1 - 1) > 0) {
            quantityInput.val(quantityInput.val() * 1 - 1);
        }
    });

    $('#button-cart').on('click', function () {
        $.ajax({
            url : 'index.php?route=checkout/cart/add',
            type : 'post',
            data : $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType : 'json',
            complete : function () {
                $('#button-cart').button('reset');
            },
            success : function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }else{
                        $('#content').prepend('<div class="alert alert-danger">' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }


                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('#content').prepend('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#product-preview').hide();
                    $('#cart > #cart-total').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);

                    $('html, body').animate({ scrollTop : 0 }, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');

                    //window.yaCounter41685499.reachGoal('AddToCart');

                    fbq('track', 'AddToCart');
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'quick-order',
                        eventAction: 'ajax-cart',
                        eventLabel: 'add-to-cart-big'
                    });
                }
            },
            error : function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>

<?php echo $footer; ?>
<?php  } ?>