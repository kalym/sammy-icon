<?php foreach ($products as $product) { ?>
<div class="product-layout product-list col-lg-4 col-md-4 col-sm-6 col-xs-6">
    <div class="product-thumb goods-block">
        <div class="image">
            <a href="<?php echo $product['href']; ?>" class="image-link">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="goods-img <?php echo (!empty($product['h_thumb'])) ? 'hover-none' : '' ; ?>" />
                <?php if (!empty($product['h_thumb'])) { ?>  <img src="<?php echo $product['h_thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="goods-img hover-image" /> <?php } ?>
            </a>
        </div>
        <img src="catalog/view/theme/default/image/icon/review-goods-img.png" class="review-goods-img review-button" alt="Preview"/>
        <?php if ($product['special']) { ?>
        <img src="catalog/view/theme/default/image/icon/sale-img.png" class="sale-img" alt="sale"/>
        <?php } ?>
        <div>
            <div class="caption goods-text-area">
                <h2><a href="<?php echo $product['href']; ?>" class="goods-name"><?php echo $product['name']; ?></a></h2>

                <?php if ($product['price']) { ?>
                <a href="<?php echo $product['href']; ?>" class="price goods-price pull-right">
                    <?php if (!$product['special']) { ?>
                    <?php echo $product['price']; ?>
                    <?php } else { ?>
                    <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                    <?php if ($product['tax']) { ?>
                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                    <?php } ?>
                </a>

                <?php } ?>

                <p>
                    <?php  if ($product['options']) {
                      echo $product['options'];
                   } ?>
                </p>
            </div>
        </div>
    </div>
</div>
<?php if($hide_showmore === true){ ?>
<script>$("#show-more").hide();</script>
<?php } ?>
<?php } ?>