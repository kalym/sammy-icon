<?php if($ajaxLoad) { ?>

    <?php if ($products) { ?>
    <div class="search-results">
        <?php foreach ($products as $product) { ?>
        <div class="search-item">
            <div class="col-md-2">
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
            </div>
            <div class="col-md-10">
                <div class="item-text">
                    <h4 class="title"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                    <p class="description"><?php echo $product['description']; ?></p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php } ?>
    </div>
    <?php } else { ?>
    <br/>
    <p><?php echo $text_empty; ?></p>
    <br/>
    <?php } ?>

<?php }else{ echo $header; ?>

<div class="container">
  <div class="row ">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row search">

        <button class="search-btn" type="submit">
          <span class=""><img src="/catalog/view/theme/default/image/icon/search-ico.png" alt="Search" /></span>
        </button>
        <input type="text" class="search-input " name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" />
        <button type="button" class="close"><img src="/catalog/view/theme/default/image/icon/esc-img.png" alt="close"></button>

      </div>
      <?php if ($products) { ?>
      <div class="search-results bottom-margin-lg">
          <?php foreach ($products as $product) { ?>
          <div class="search-item">
            <div class="col-md-2">
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
            </div>
            <div class="col-md-10">
              <div class="item-text">
                <h4 class="title"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                <p class="description"><?php echo $product['description']; ?></p>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <?php } ?>
      </div>
    <?php } else { ?>
        <br/>
    <p><?php echo $text_empty; ?></p>
        <br/>
    <?php } ?>
    </div>
    <?php echo $content_bottom; ?>
  <?php echo $column_right; ?>


  </div>
</div>
<script type="text/javascript"><!--

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
      url = 'index.php?route=product/search';

      var search = $(this).val();

      if (search) {
        url += '&search=' + encodeURIComponent(search);
      }

        url += '&sub_category=true';

        url += '&description=true';
        //console.log(url);
      location = url;
	}
});

    $('#content button[type=\'reset\']').on('click',function(){
        $('#input-search').val('');
    });

--></script>
<?php echo $footer; } ?>