<?php echo $header; ?>
<div class="container category-page-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><?php if($breadcrumb['href']!='#') { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
      <?php }else{ ?>
      <span><?php echo $breadcrumb['text']; ?></span>
      <?php } ?>
    </li>
    <?php } ?>
  </ul>
  <div class="row">
    <?php $class = 'col-sm-9'; ?>
    <aside class="col-sm-3 " id="column-left">
      <?php if ($categories) { ?>
      <?php if (count($categories) <= 10) { ?>
      <div class="row">
        <div class="col-sm-12">
          <ul class="list-unstyled left-menu">
            <?php foreach ($categories as $category) { ?>
            <li>
              <?php if($category_id != $category['id']){ ?><a href="<?php echo $category['href']; ?>" > <?php }else{ ?><span class="active"><?php } ?>
                <?php if(!empty($category['thumb'])) { ?>
                  <img src="image/<?php echo $category['thumb'] ?>" alt="<?php echo $category['name']; ?>"  />
                <?php } ?>
                <?php echo $category['name']; ?>
              <?php if($category_id != $category['id']){ ?></a><?php }else{ ?></span><?php } ?>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-12">
          <ul class="list-unstyled left-menu">
            <?php foreach ($categories as $category) { ?>
            <li>
              <a href="<?php echo $category['href']; ?>">
                <?php if(!empty($category['thumb'])) { ?>
                <img src="image/<?php echo $category['thumb'] ?>" alt="<?php echo $category['name']; ?>"/>
                <?php } ?>
                <?php echo $category['name']; ?>
              </a>
            </li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
    </aside>

    <div id="content" class="<?php echo $class; ?> category-product-container"><?php echo $content_top; ?>
      <?php if ($products) { ?>
      <div class="row nav-panel  hidden-xs">
        <div class="hidden">
          <div class="btn-group hidden">

            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>

          </div>
        </div>
        <div class="col-md-9 col-sm-12 text-right nopadding pull-right">
          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
          <select id="input-sort" class="selectpicker" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option  value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
          <div id="input-limit" class="more-items" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <a href="<?php echo $limits['href']; ?>" class="nambers active"><?php echo $limits['text']; ?></a>
            <?php } else { ?>
            <a href="<?php echo $limits['href']; ?>" class="nambers"><?php echo $limits['text']; ?></a>
            <?php } ?>
            <?php } ?>
          </div>
        </div>
      </div>
      <p class="free-shipping" ><img src="/catalog/view/theme/default/image/delivery_ico.png" alt="shipping Icon"><?php echo $text_free_shipping; ?></p>
     <!-- <div class="fast-cart-widjet">
        <form>
          <select id="fast-size">
            <option selected><?php echo $entry_size; ?></option>
          </select>
          <input type="number" id="fast-quantity">
          <button type="submit"><?php echo $text_checkout; ?></button>
        </form>
      </div> -->
      <div class="row" id="product-row">
        <?php
         foreach ($products as $product) { ?>
        <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="product-thumb goods-block">
            <div class="image">
              <a href="<?php echo $product['href']; ?>" class="image-link">
                  <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="goods-img <?php echo (!empty($product['h_thumb'])) ? 'hover-none' : '' ; ?>" />
                <?php if (!empty($product['h_thumb'])) { ?>  <img src="<?php echo $product['h_thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="goods-img hover-image" /> <?php } ?>
              </a>
            </div>
            <img src="/catalog/view/theme/default/image/icon/review-goods-img.png" class="review-goods-img review-button" alt="Preview"/>
            <!--<button class="review-goods-img buy-button" >
              <img src="/catalog/view/theme/default/image/icon/buy-btn.png" alt="Buy it!"/>
            </button>-->
            <?php if ($product['special']) { ?>
              <img src="/catalog/view/theme/default/image/icon/sale-img.png" class="sale-img" alt="sale"/>
            <?php } ?>
            <div>
              <div class="caption goods-text-area">
                <h2><a href="<?php echo $product['href']; ?>" class="goods-name"><?php echo $product['name']; ?></a></h2>

                <?php if ($product['price']) { ?>
                <a href="<?php echo $product['href']; ?>" class="price goods-price pull-right">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                  <?php if ($product['tax']) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                  <?php } ?>
                </a>

                <?php } ?>

                <p>
                  <?php  if (isset($product['options'])) {
                      echo $product['options'];
                   } ?>
                </p>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="col col-md-12 more-block">
        <a href="<?php echo $limit_url; ?>" id="show-more">
          <img src="catalog/view/theme/default/image/icon/more-img.png" alt="show more" />
          <?php echo $text_showmore; ?>
        </a>
      </div>
      <?php } ?>

      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>

      <?php } ?>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<div class="product-preview modal fade in" id="product-preview"></div>

<?php if($hide_showmore === true){ ?>
<script>$("#show-more").hide();</script>
<?php } ?>

<script>


  $(document).on("click","#product-preview",function(event){
   if( event.target.id == 'product-preview' ) {
     $(this).hide();
   }
  });
  $(document).on('click',".close",function() {
    $("#product-preview").hide();
  });

  $(document).on('click','img.review-button', function() {
    var url = $(this).parent().find('a.image-link').attr('href');

    if(url){
      $.ajax({
        url: url.trim() + "&preview=1",
        type: 'get',
        success: function(html) {
          $('#product-preview').html("<div class='popup-container'> "+ '<button type="button" class="close close-btn" data-dismiss="product-preview"><img src="catalog/view/theme/default/image/popup-login/close-img.png" alt="123"></button>' +html+"</div>").show();
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  });

</script>

<?php echo $footer; ?>
