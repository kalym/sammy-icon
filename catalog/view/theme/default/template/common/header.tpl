<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="alternate" hreflang="ua" href="//www.sammy-icon.com<?php echo $ros; ?>">
    <link rel="alternate" hreflang="ru" href="//www.sammy-icon.com/ru<?php echo $ros; ?>">
    <link rel="alternate" hreflang="en" href="//www.sammy-icon.com/en<?php echo $ros; ?>">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title><?php echo $title; ?></title>

    <base href="<?php echo $base; ?>" />

    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>

    <script src="/catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="/catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <link href="/markup/css/styles.css?v=1.406" rel="stylesheet">
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>

    <script src="/catalog/view/javascript/common.js?v=1.106" type="text/javascript"></script>

    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-34720551-1', 'auto');
        ga('require', 'ec');

    </script>
    <!-- End Google Analytics -->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TF2BXKV');</script>
    <!-- End Google Tag Manager -->
</head>
<body class="<?php echo $class; ?>">

<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TF2BXKV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '777034185670951'); // Insert your pixel ID here.
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=777034185670951&ev=PageView&noscript=1"
            /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<header>

<div class="header-wrapper">
    <div class="container narrow">
        <div class="background">
            <div class="custom-block">
                <?php echo $content_newpos; ?>
            </div>
          <!-- <div class="delivery-wrap" style="text-align: center">
                <p style="margin-bottom: 0; padding: 4px 0">
                    <?php echo $text_holiday_deliver; ?>
                </p>
            </div> -->
            <div class="navbar top-navbar">
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 nopadding">
                    <span class="contact">
                        <?php echo $telephone; ?>
                    </span>

                    <span class="working-time">
                        <span class="semibold"><?php echo $text_worktime; ?></span> <?php echo $text_works; ?>
                    </span>

                     <!--<span class="text-beta">
                        <?php echo $text_beta; ?>
                    </span>-->
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 pull-right-md nopadding">
                    <div class="language-picker pull-right">
                        <?php echo $language; ?>
                    </div>
                    <nav class="top-menu nav">
                        <ul class="nav">
                            <?php foreach ($top_menu as $menu_item) { ?>
                                <li><?php if($menu_item['link']!='#'){ ?>
                                        <a href="<?php echo $menu_item['link'] ?>" ><?php echo $menu_item['title'] ?></a>
                                    <?php }else{ ?>
                                        <span><?php echo $menu_item['title'] ?></span>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="pull-left logo">
                <?php if ($logo && $home_link != '#') { ?>
                <a href="<?php echo $home_link; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
                <?php } elseif($logo && $home_link=='#') { ?>
                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
                <?php } else { ?>
                <a href="<?php echo $home_link; ?>"><img src="/image/catalog/sammy-logo.png" alt="Sammy Icon Logo" title="Sammy Icon Logo"/></a>
                <?php } ?>
            </div>
            <div class="pull-right">
                <ul class="navbar-icon cart-nav nav">
                    <li>
                        <div class="search-block">
                            <?php echo $search; ?>
                        </div>
                        <img src="catalog/view/theme/default/image/icon/search.png" id="search-button" alt="Icon" />
                    </li>
   <!--                 <li><a href="<?php echo $account; ?>" <?php if(!$logged){ ?> data-toggle="modal" data-target="#login-popup" <?php } ?> ><img src="catalog/view/theme/default/image/icon/user.png" alt="Icon" /></a></li>

                    <li>
                        <a href="<?php echo $wishlist; ?>" <?php if(!$logged){ ?> data-toggle="modal" data-target="#login-popup" <?php } ?>  id="wishlist-total" title="<?php echo $text_wishlist; ?>">
                            <img src="catalog/view/theme/default/image/icon/wishlist.png" alt="Icon" />
                        </a>
                    </li> -->
                    <li class="cart-wrapper">
                       <!-- <a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>">  -->
                            <img src="catalog/view/theme/default/image/icon/cart.png" alt="Icon" />
                            <?php echo $cart; ?>
                       <!-- </a>-->
                    </li>
                </ul>
            </div>
            <?php if ($categories) { ?>
            <nav class="main-menu nav" id="menu">
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav">
                    <?php foreach ($categories as $category) { ?>
                        <?php if ($category['children']) { ?>
                            <li class="dropdown">
                                <?php if($category['children'][0]['href']!='#') { ?>
                                <a href="<?php echo $category['children'][0]['href']; ?>" class="dropdown-toggle">
                                    <?php echo $category['name']; ?>
                                </a>
                                <?php }else{ ?>
                                    <span class="dropdown-toggle" ><?php echo $category['name']; ?></span>
                                <?php } ?>
                                <div class="dropdown-menu">
                                    <div class="dropdown-inner">
                                        <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                        <ul class="list-unstyled">
                                            <?php foreach ($children as $child) { ?>
                                            <li>
                                            <?php if($child['href']!='#'){ ?>
                                            <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                            <?php }else{ ?>
                                                <span><?php echo $child['name']; ?></span>
                                            <?php } ?>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                        <?php } ?>
                                    </div>
                                </div>
                            </li>
                        <?php } else { ?>
                        <li>
                            <?php if($category['href']!='#'){ ?>
                            <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                            <?php }else{ ?>
                            <span><?php echo $category['name']; ?></span>
                            <?php } ?>
                        </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
                </div>
            </nav>
            <?php } ?>

        </div>

    </div>

</div>


    <div class="subscription-page modal fade" id="login-popup">
        <div class="auth-window" style="display: none">
           </div>
        <div class="container">
            <button class="close-btn" data-dismiss="modal">
                <img src="catalog/view/theme/default/image/popup-login/close-img.png" alt="123" />
            </button>
            <div class="header">
                <img src="catalog/view/theme/default/image/popup-login/subscription--bg-img.png" alt="123" />
            </div>
            <div class="middle-contant">
                <div class="row">
                    <div class="col col-md-6 text-right login">
                        <h2><?php echo $text_enter; ?></h2>
                            <input type="text" name="email"  placeholder="E-mail">
                            <input type="password"  name="password"  placeholder="<?php echo $text_password; ?>">
                            <a href="<?php echo $url_forgotten; ?>"><?php echo $text_forgotten; ?></a>
                            <button id="popup-login"><?php echo $text_login; ?></button>
                    </div>
                    <div class="col col-md-6 separator-left register">
                        <h2><?php echo $text_registration; ?></h2>
                            <input type="text" name="email"  placeholder="E-mail">
                            <input type="password" name="password"  placeholder="<?php echo $text_password; ?>">
                            <input type="password" name="confirm"  placeholder="<?php echo $text_confirm; ?>">
                            <button id="popup-register" class="l-margin"><?php echo $text_signup; ?></button>
                    </div>
                </div>
            </div>
            <div class="row social-buttons log-page">
                <div class="col col-md-12">
                    <h2><?php echo $text_login_with; ?></h2>
                </div>
                <div class="col col-md-4 col-sm-12">
                    <button class="social-button facebook">
                        <a href="#">
                            <img src="catalog/view/theme/default/image/popup-login/facebook-img.png" alt="123" />
                            Facebook
                        </a>
                    </button>
                </div>
                <div class="col col-md-4 col-sm-12">
                    <button class="social-button vkontakte">
                        <a href="#">
                            <img src="catalog/view/theme/default/image/popup-login/vkontakte-img.png" alt="123" />
                            Vkontakte
                        </a>

                    </button>
                </div>
                <div class="col col-md-4 col-sm-12">
                    <button class="social-button google">
                        <a href="#">
                            <img src="catalog/view/theme/default/image/popup-login/google-img.png" alt="123" />
                            Google
                        </a>
                    </button>
                </div>
            </div>
        </div>
    </div>


</header>
<div class="modal smart-checkout-modal fade" id="smart-checkout">
    <button class="close-btn" data-dismiss="modal">
        <img src="catalog/view/theme/default/image/popup-login/close-img.png" alt="close modal" />
    </button>
    <button class="go-back-checkout">
        <img src="catalog/view/theme/default/image/icon/arrow.png" alt="Go back">
    </button>
    <div class="modal-content checkout-checkout">
    </div>
</div>
<div class="arrow-up">
    <a href="#" class="scrollToTop"><img src="catalog/view/theme/default/image/icon/arrow_up.png" alt="Scroll To Top"></a>
</div>

