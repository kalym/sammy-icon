<footer>
  <div class="container narrow">
    <div class="footer-wrapper">
    <div class="row">
      <?php if ($categories) { ?>
      <div class="col col-lg-2 col-md-3 col-sm-4 col-xs-6">
        <h3><?php echo $text_shop; ?></h3>
        <ul class="nav footer-nav" >
          <?php foreach ($categories as $category) { ?>
          <li>
              <?php if($category['href']!='#'){ ?>
              <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
              <?php }else{ ?>
              <span><?php echo $category['name']; ?></span>
              <?php } ?>
          </li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col col-lg-2 col-md-3 col-sm-4 col-xs-6">
        <h3><?php echo $text_brand; ?></h3>
        <ul class="nav footer-nav" >
          <!--<li><a href="<?php echo $blog; ?>"><?php echo $text_blog; ?></a></li>-->
          <li>
            <?php if($history!='#'){ ?>
            <a href="<?php echo $history; ?>"><?php echo $text_history; ?></a>
            <?php }else{ ?>
            <span><?php echo $text_history; ?></span>
            <?php } ?>
          </li>
          <?php if(count($informations) == 2){ ?>
          <li><?php $lastInf = array_pop($informations); ?>
              <a href="<?php echo $lastInf['href']; ?>"><?php echo $lastInf['title']; ?></a>
          </li>
          <?php } ?>
          <!--<li><a href="<?php echo $lookbook; ?>"><?php echo $text_lookbook; ?></a></li>-->
        </ul>
      </div>

      <div class="col col-lg-2 col-md-3 col-sm-4 col-xs-6">
        <h3><?php echo $text_information; ?></h3>
        <ul class="nav footer-nav" >
          <li>
            <?php if($contact!='#') { ?>
            <a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a>
            <?php }else{ ?>
            <span><?php echo $text_contact; ?></span>
            <?php } ?>
          </li>
          <?php if ($informations) { ?>
            <?php foreach ($informations as $key => $information) { ?>
            <li>
              <?php if($information['href']!='#' ) {  ?>
                <a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
              <?php }else{ ?>
                <span><?php echo $information['title']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
          <?php } ?>
        </ul>
      </div>

      <div class="col col-lg-2 col-md-3 col-sm-4 col-xs-6">
        <h3><?php echo $text_shops; ?></h3>
        <ul class="nav footer-nav" >
          <li>
            <?php if($shops!='#') { ?>
            <a href="<?php echo $shops; ?>"><?php echo $text_addresses; ?></a>
            <?php }else{ ?>
            <span><?php echo $text_addresses; ?></span>
            <?php } ?>
          </li>
          <li>
            <?php if($be_partner!='#') { ?>
            <a href="<?php echo $be_partner; ?>"><?php echo $text_partners; ?></a>
            <?php }else{ ?>
            <span><?php echo $text_partners; ?></span>
            <?php } ?>
          </li>
        </ul>
      </div>
      <div class="col col-lg-4 col-md-6 col-sm-6 col-xs-12 subscribe-block">
        <h3><?php echo $text_subscribe; ?></h3>
        <form role="form" class="form-inline" id="subscribe_form">
          <input type="text" class="form-control" name="email" id="subscribe_email" placeholder="<?php echo $text_subscribe_placeholder; ?>">
          <button type="submit" id="subscribe" class="btn btn-primary"><?php echo $text_subscribe_button; ?></button>
        </form>
        <p class="payments-methods"><?php echo $text_payments_methods; ?>
          <img src="/catalog/view/theme/default/image/icon/visa.png" alt="pay by visa"/>
          <img src="/catalog/view/theme/default/image/icon/mastercard.png" alt="pay by mastercard"/>
          <img src="/catalog/view/theme/default/image/icon/paypal.png" alt="pay by paypal"/>
        </p>
      </div>
    </div>
      <hr/>
      <div class="row">
        <div class="col col-lg-7 col-md-7 col-sm-6 col-xs-12 logo">
          <div class="pull-left">
            <img src="catalog/view/theme/default/image/logo-footer.png" alt="Sammy Icon Logo"/>
          </div>
          <div class="call-center-working-time">
            <p><?php echo $text_worktime; ?></p>
            <p><?php echo $text_time; ?></p>
            <p><?php echo $text_days; ?></p>
          </div>
        </div>
        <div class="col col-lg-5 col-md-5 col-sm-6 col-xs-12 social-buttons">
          <p id="sammyicon">#SAMMYICON</p>
          <div class="col col-lg-6">
            <a href="https://www.facebook.com/SammyIcon.Socks/" target="_blank">
              <button class="social  btn-primary">
                Facebook
              </button>
            </a>
          </div>
          <div class="col col-lg-6">
            <a href="https://www.instagram.com/sammyicon/" target="_blank">
              <button class="social inst btn-primary">
                Instagram
              </button>
            </a>
          </div>
        </div>
      </div>
  </div>
  </div>
</footer>


<?php if($country_page) { ?>
<div class="pick-your-country modal fade" id="pick-your-country">
  <?php echo $country_page; ?>
</div>
<script>
  $('#pick-your-country').modal('show');
</script>
<?php } ?>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body>
</html>