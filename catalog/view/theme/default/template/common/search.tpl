<div id="search" class="input-group search-header">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" />
  <button type="button" class="close"><img src="/catalog/view/theme/default/image/icon/esc-img.png" alt="close"></button>
  <div class="preresult-place"></div>
</div>
<script>
  fbq('track', 'Search');
</script>
