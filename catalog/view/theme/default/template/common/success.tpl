<?php echo $header; ?>
<div class="container <?php if ($column_left) { echo 'cabinet'; } ?>">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-offset-2 col-sm-8'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if ($column_left) { ?>
        <h2 class="strong blue uppercase top-margin-lg font-md"><?php echo $heading_title; ?></h2>
      <?php } else { ?>
        <h1 class="strong blue uppercase top-margin-lg font-md"><?php echo $heading_title; ?></h1>
      <?php } ?>
      <?php echo $text_message; ?>
      <?php echo $scripts; ?>
      <div class="buttons">
        <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn-success btn bottom-margin-xxxlg btn-lg"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>