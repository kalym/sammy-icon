<div id="cart" >
  <span id="cart-total" ><?php echo $text_items; ?></span>
  <div  data-toggle="dropdown" class="dropdown-toggle"></div>
<?php if (!$is_checkout_page) { ?>
   <ul id="cart-dropdown" class="dropdown-menu pull-right">
       <!--<button type="button" class="close" data-dismiss="cart-dropdown">&times;</button>
-->
     <?php if ($products || $vouchers) { ?>
     <li>
       <table class="table products-list">
         <?php foreach ($products as $product) { ?>

             <tr>
               <td class="vcenter" rowspan="3"><?php if ($product['thumb']) { ?>
                 <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                 <?php } ?></td>
               <td class="text-left product-name" colspan="3">
                 <a href="<?php echo $product['href']; ?>"><?php echo mb_strlen($product['name'],'utf-8') > 12 ? mb_substr($product['name'],0,12,'utf-8').'...' : $product['name']; ?></a>
               </td>
             </tr>
             <tr>
              <td>
                 <?php if ($product['option']) {
                    foreach ($product['option'] as $option) { ?>
                        <small> <?php echo $option['value']; ?></small>
                    <?php } ?>
                 <?php } ?>
                 <?php if ($product['recurring']) { ?>
                    <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                 <?php } ?>
              </td>
              <td class="text-left"><?php echo $product['total']; ?></td>
              <td class="text-left"><button type="button" class="remove-item" data-product-id="<?php echo $product['cart_id'] ?>" >&times;</button></td>
             </tr>
             <tr>
                 <td class="text-left"><small><?php echo $text_quantity; ?></small></td>
                 <td class="text-left" colspan="2"><?php echo $product['quantity']; ?></td>
             </tr>
         <?php } ?>
         <?php foreach ($vouchers as $voucher) { ?>
         <tr>
           <td class="text-center"></td>
           <td class="text-left"><?php echo $voucher['description']; ?></td>
           <td class="text-right">x&nbsp;1</td>
           <td class="text-right"><?php echo $voucher['amount']; ?></td>
           <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
         </tr>

         <?php } ?>
       </table>

     </li>
       <div class="separator"></div>
     <li>

           <?php $total = end($totals); ?>
           <table class="table table-total"><tr>
            <td> <?php echo $total['title']; ?>:</td>
             <td class="text-right"><?php echo $total['text']; ?></td>
               </tr>
           </table>
         <a id="checkout-btn" href="<?php echo $checkout; ?>"><button class="text-center btn-success btn btn-lg"><strong><i class="fa fa-share"></i> <?php echo $text_checkout; ?></strong></button></a>
     </li>
     <?php } else { ?>
     <li>
       <p class="text-center"><?php echo $text_empty; ?></p>
     </li>
     <?php } ?>
   </ul>
<?php } ?>
</div>
