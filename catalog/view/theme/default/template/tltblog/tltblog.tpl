<?php echo $header; ?>
<div class="blog-read blog">
    <div class="content-wrapper" >
        <div class="background-color"></div>
        <div class="container padding-bot">

            <div class="row article-title">
                <div class="col-md-12 ">
                    <img class="l-head-img" src="<?php echo $blog_image; ?>" alt="<?php echo $heading_title; ?>"
                         title="<?php echo $heading_title; ?>">

                    <h1><?php echo $heading_title; ?></h1>
                </div>
            </div>
            <?php echo $article_pt1; ?>


                <div id="carousel1" class="owl-carousel col-md-12 slider ">
                    <?php foreach ($banners as $banner) { ?>
                    <div class="item text-center">

                        <div class="item slide">
                            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                        </div>

                    </div>
                    <?php } ?>
                </div>

                <script type="text/javascript"><!--
                    $(document).ready(function() {

                        $("#carousel1").owlCarousel({

                            navigation : true, // Show next and prev buttons
                            slideSpeed : 300,
                            paginationSpeed : 400,
                            singleItem:true,
                            navigationText:
                                    ['<img src="image/catalog/blog/icon/arrow.png" class="r-arrow">',
                                        '<img src="image/catalog/blog/icon/arrow.png" class="l-arrow">'
                                    ]

                            // "singleItem:true" is a shortcut for:
                            // items : 1,
                            // itemsDesktop : false,
                            // itemsDesktopSmall : false,
                            // itemsTablet: false,
                            // itemsMobile : false

                        });

                    });

                    --></script>



            <?php echo $article_pt2; ?>
            <div class="col-sm-offset-2 col-sm-8">
                <div class="see-also-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="see-also">Читайте також:</p>
                        </div>
                    </div>
                    <div class="row">
                        <?php foreach ($tltblogs as $tltblog) { ?>
                        <div class="col-md-6">
                            <a href="<?php echo $tltblog['href']; ?>">
                                <img src="<?php echo $tltblog['image']; ?>" alt="related-image"/>
                                <span class="same-title"><?php echo $tltblog['title']; ?></span>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php echo $footer; ?>