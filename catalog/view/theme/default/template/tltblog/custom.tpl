<?php echo $header; ?>
<div class="history blog">
    <div class="content-wrapper" >
        <div class="background-color"></div>
        <div class="container bottom">

            <div class="row article-title">
                <div class="col-md-12 ">
                    <img class="history-wrapp" src="<?php echo $blog_image; ?>" alt="<?php echo $heading_title; ?>"
                         title="<?php echo $heading_title; ?>">

                    <h1><?php echo $heading_title; ?></h1>
                </div>
            </div>

            <?php echo $article_pt1; ?>


            <div id="carousel1" class="owl-carousel col-md-12 slider ">
                <?php foreach ($banners as $banner) { ?>
                <div class="item text-center">

                    <div class="item slide">
                        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                    </div>

                </div>
                <?php } ?>
            </div>

            <script type="text/javascript"><!--
                $(document).ready(function() {

                    $("#carousel1").owlCarousel({

                        navigation : true, // Show next and prev buttons
                        slideSpeed : 300,
                        paginationSpeed : 400,
                        singleItem:true,
                        navigationText:
                                ['<img src="image/catalog/blog/icon/arrow.png" class="r-arrow">',
                                    '<img src="image/catalog/blog/icon/arrow.png" class="l-arrow">'
                                ]

                        // "singleItem:true" is a shortcut for:
                        // items : 1,
                        // itemsDesktop : false,
                        // itemsDesktopSmall : false,
                        // itemsTablet: false,
                        // itemsMobile : false

                    });

                });

                --></script>



            <?php echo $article_pt2; ?>
        </div>

    </div>

    <?php echo $footer; ?>