<!-- •••articles-preview -->
<div class="blog-page">
    <div class="container top-slider-wrapp">

        <div class="row">
            <div class="col col-md-12 slider-block ">

                <div id="carousel1" class="owl-carousel col-md-12 slider ">
                    <?php for($i=0; $i<4;  $i++) {
                $tltblog = $tltblogs[$i];
            ?>
                    <div class="item text-center">
                    <img src="<?php echo $tltblogs[$i]['thumb']; ?>" alt="123"/>

                    <div class="small-col-wrapp">
                        <h1><?php echo $tltblogs[$i]['title']; ?></h1>
                        <?php echo $tltblogs[$i]['intro']; ?>
                    </div>
                        <button><a href="<?php echo $tltblogs[$i] ['href']; ?>">Читати далі</a></button>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="row two-col-1">
            <div class="col col-md-4 small-col">
                <img src="image/catalog/blog/blog-img-1.png" alt="article 2"/>
                <p>
                    <?php echo $tltblogs[0]['quote1'] ?>
                </p>
            </div>
            <div class="col col-md-8 big-col">
                <img src="<?php echo $tltblogs[0]['large_preview_img']; ?>" alt="123"/>

                <div class="small-col-wrapp">
                    <h1><?php echo $tltblogs[0] ['title']; ?></h1>
                    <?php echo $tltblogs[0] ['intro']; ?>
                </div>
                <button><a href="<?php echo $tltblogs[0] ['href']; ?>">Читати далі</a></button>
            </div>

        </div>
        <div class="row two-col-2">
            <div class="col col-md-8 big-col">
                <img src="<?php echo $tltblogs[1]['large_preview_img']; ?>" alt="article 2"/>

                <div class="small-col-wrapp">
                    <h1><?php echo $tltblogs[1] ['title']; ?></h1>

                    <?php echo $tltblogs[1] ['intro']; ?>
                </div>
                <button><a href="<?php echo $tltblogs[1] ['href']; ?>">Читати далі</a></button>
            </div>
            <div class="col col-md-4 small-col">
                <img src="image/catalog/blog/blog-img-4.png" alt="quote 2"/>

                <p>
                    <?php echo $tltblogs[1]['quote1']?>
                </p>
            </div>
        </div>
        <?php $rowcounter = 0; ?>
        <div class="row three-col">
            <?php for($i=2; $i<count ($tltblogs);  $i++) {
                $tltblog = $tltblogs[$i];
            ?>

            <div class="col col-md-4 small-col">
                <img src="<?php echo $tltblog['small_preview_img']; ?>"
                     alt="123"/>

                <div class="small-col-wrapp">
                    <h1><?php echo $tltblog['title']; ?></h1>

                    <?php echo $tltblog['intro']; ?>
                </div>
                <button><a href="<?php echo $tltblog['href']; ?>">Переглянути</a></button>

            </div>
            <?php $rowcounter++; ?>
            <?php if ($rowcounter == $num_columns) { ?>
        </div>
        <div class="row three-col">
            <?php $rowcounter = 0; ?>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $(document).ready(function() {

        $("#carousel1").owlCarousel({

            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            navigationText:
                    ['<img src="image/catalog/blog/icon/arrow.png" class="r-arrow">',
                        '<img src="image/catalog/blog/icon/arrow.png" class="l-arrow">'
                    ]

            // "singleItem:true" is a shortcut for:
            // items : 1,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false

        });

    });

    --></script>