<!-- •••articles-preview -->
<div class="lookbook-page">
    <div class="container top-slider-wrapp">

        <div class="row">
            <div class="col col-md-12 slider-window">
                <img src="<?php echo $lookbooks[0]['thumb']; ?>" alt="123" />
                <h1><?php echo $lookbooks[0]['title']; ?></h1>
                <p><?php echo $lookbooks[0]['intro']; ?></p>
                <button><a href="<?php echo $lookbooks[0] ['href']; ?>">Читати далі</a></button>
            </div>
        </div>

        <?php $rowcounter = 0; ?>
        <div class="row three-col">
            <?php for($i=1; $i<count ($lookbooks);  $i++) {
                $lookbook = $lookbooks[$i];
            ?>

            <div class="col col-md-4 small-col">
                <img src="<?php echo $lookbook['small_preview_img']; ?>"
                     alt="123"/>

                <div class="small-col-wrapp">
                    <h1><?php echo $lookbook['title']; ?></h1>
                </div>
                <button><a href="<?php echo $lookbook['href']; ?>">Переглянути</a></button>

            </div>
            <?php $rowcounter++; ?>
            <?php if ($rowcounter == $num_columns) { ?>
        </div>
        <div class="row three-col">
            <?php $rowcounter = 0; ?>
            <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    $(document).ready(function() {

        $("#carousel1").owlCarousel({

            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            navigationText:
                    ['<img src="image/catalog/blog/icon/arrow.png" class="r-arrow">',
                        '<img src="image/catalog/blog/icon/arrow.png" class="l-arrow">'
                    ]

            // "singleItem:true" is a shortcut for:
            // items : 1,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false

        });

    });

    --></script>