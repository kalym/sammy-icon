<div class="cart-step-1-page" id="shoppingcart">
    <div class=" cart-step-page-wrapp">

            <div class="col col-md-12 left-col-wrapp">
                <?php if ($products || $vouchers) { ?>
                <h1><?php echo $heading_title; ?></h1>
                <div class="row heading">
                    <div class="col col-xs-offset-2 col-xs-3 goods-info">
                        <div class="text-left">
                            <p class="goods-title"><?php echo $goods_title; ?></p>
                        </div>
                    </div>
                    <div class="col col-xs-7  goods-seting">
                        <div class="col col-md-4 col-sm-4 col-xs-4">
                            <p class="goods-title"><?php echo $goods_count_title; ?></p>
                        </div>
                        <div class="col col-md-4 col-sm-4 col-xs-4 midle">
                            <p class="goods-title"><?php echo $goods_size_title; ?></p>
                        </div>
                        <div class="col col-md-4 col-sm-4 col-xs-4 bottom">
                            <p class="goods-title"><?php echo $goods_price_title; ?></p>
                        </div>
                    </div>
                </div>
                <?php $i=0; foreach ($products as $product) { ?>
                <div class="row product-row<?php echo ++$i;?>">
                    <div class="col col-xs-5 goods-info">
                        <?php if ($product['thumb']) { ?>
                        <a href="<?php echo $product['href']; ?>" class="pull-left"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                        <?php } ?>

                            <p class="goods-option">
                                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                <?php if ($product['option']) { ?>
                                <?php foreach ($product['option'] as $option) { ?>
                                <br />
                                 <small>
                                    <?php if ($option['option_id']!=2)
                                            {
                                                echo $option['name'];
                                                echo $option['value'];
                                            }else{
                                                $size = $option['value'];
                                            }
                                    ?>
                                </small>
                                <?php } ?>
                                <?php } ?>
                                <?php if ($product['recurring']) { ?>
                                <br />
                                - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                                <?php } ?>
                            </p>

                    </div>
                    <div class="col col-xs-7  goods-seting">
                        <div class="col col-md-4 col-sm-4 col-xs-4">
                            <div class="goods-option product-quantity">
                                <span class="minus" ><img src="/catalog/view/theme/default/image/icon/minus.png" alt="minus"></span>
                                <span data-pid="<?php echo $product['cart_id']; ?>" class="quantity"><?php echo $product['quantity']; ?></span>
                                <span class="plus" ><img src="/catalog/view/theme/default/image/icon/plus.png" alt="plus"></span>
                            </div>
                        </div>
                        <div class="col col-md-4 col-sm-4 col-xs-4 midle">
                            <p class="goods-option"><?php echo (isset($size))?$size:""; ?></p>
                        </div>
                        <div class="col col-md-4 col-sm-4 col-xs-4 bottom">
                            <div class="goods-option goods-price">
                                <a onclick="cart.remove('<?php echo $product['cart_id']; ?>');" class="pull-right remove_product_from_cart" title="<?php echo $button_remove; ?>" >
                                    <img class="pull-right" src="/catalog/view/theme/default/image/icon/esc-img.png" alt="Delete item" />
                                </a>
                                <span class="lprice"><?php echo $product['total']; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="separator"></div>
                <div class="row">
                    <?php if ($modules) { ?>
                    <div class="col col-md-7 promo">
                        <?php foreach ($modules as $module) { ?>
                        <?php echo $module; ?>
                      <!--  <input type="text" placeholder="Промо-код" />
                        <button>ок</button> -->
                        <?php } ?>
                    </div>
                    <?php } ?>



                    <div class="col col-md-5 result">
                        <table class="table">
                            <tbody>
                            <?php foreach ($totals as $total) { ?>
                            <tr>
                                <td><?php echo $total['title']; ?></td>
                                <td class="right"><?php echo $total['text']; ?></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>

                <?php } ?>

</div>
</div>
</div>