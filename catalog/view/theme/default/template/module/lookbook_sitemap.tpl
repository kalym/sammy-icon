<?php if ($show_path) { ?>
<h2><?php echo $path_title; ?></h2>
<?php } ?>
<?php if ($type == 'blogs') { ?>
    <ul>
    <?php foreach ($lookbooks as $lookbook) { ?>
    <li><a href="<?php echo $lookbook['href']; ?>"><?php echo $lookbook['title']; ?></a></li>
    <?php } ?>
    </ul>
<?php } elseif ($type == 'tags') { ?>
    <ul>
    <?php foreach ($lookbooktags as $lookbooktag) { ?>
    <li><a href="<?php echo $lookbooktag['href']; ?>"><?php echo $lookbooktag['title']; ?></a></li>
    <?php } ?>
    </ul>
<?php } else { ?>
    <div class="row">
        <div class="col-sm-6">
            <ul>
                <li><?php echo $heading_blogs; ?></li>
                <ul>
                    <?php foreach ($lookbooks as $lookbook) { ?>
                    <li><a href="<?php echo $lookbook['href']; ?>"><?php echo $lookbook['title']; ?></a></li>
                    <?php } ?>
                </ul>
            </ul>
        </div>
        <div class="col-sm-6">
    		<ul>
            	<li><?php echo $heading_tags; ?></li>
                <ul>
                    <?php foreach ($lookbooktags as $lookbooktag) { ?>
                    <li><a href="<?php echo $lookbooktag['href']; ?>"><?php echo $lookbooktag['title']; ?></a></li>
                    <?php } ?>
                </ul>
    		</ul>
    	</div>
    </div>
<?php } ?>
