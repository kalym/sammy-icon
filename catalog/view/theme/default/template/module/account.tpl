<div class="list-group">
    <h1><?php echo $heading_title; ?></h1>
    <div class="clearfix"></div>
    <ul class="left-menu nav">
        <li><a href="<?php echo $edit; ?>" class="uppercase <?php echo $account_page == 'account_page' ? 'selected' : ''; ?>"><?php echo $text_edit; ?></a></li>
        <li><a href="<?php echo $address; ?>" class="uppercase <?php echo $account_page == 'address_page' ? 'selected' : ''; ?>"><?php echo $text_address; ?></a></li>
        <li><a href="<?php echo $order; ?>" class="uppercase <?php echo $account_page == 'order_page' ? 'selected' : ''; ?>"><?php echo $text_order; ?></a></li>
        <li><a href="<?php echo $wishlist; ?>" class="uppercase <?php echo $account_page == 'wishlist_page' ? 'selected' : ''; ?>"><?php echo $text_wishlist; ?></a></li>
        <li><a href="<?php echo $password; ?>" class="uppercase <?php echo $account_page == 'password_page' ? 'selected' : ''; ?>"><?php echo $text_password; ?></a></li>
        <li><a href="<?php echo $logout; ?>" class="uppercase"><?php echo $text_logout; ?></a></li>
    </ul>

</div>
