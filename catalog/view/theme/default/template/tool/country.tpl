
    <style>
        *{
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        *:before, *:after {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .country{
            padding: 0;
            margin: 0;
            font-family: proxima_nova_rgregular,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
        }
        .container{
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }
        .container:before, .container:after {
            content: " ";
            display: table;
        }
        .container:after {
            clear: both;
        }

        a{outline:none; text-decoration:none}
        @font-face{font-family:'proxima_nova_semibold';src:url("/markup/fonts/proxima-nova-semibold/proxima_nova_semibold_0-webfont.eot");src:url("/markup/fonts/proxima-nova-semibold/proxima_nova_semibold_0-webfont.eot?#iefix") format("embedded-opentype"),url("/markup/fonts/proxima-nova-semibold/proxima_nova_semibold_0-webfont.woff2") format("woff2"),url("/markup/fonts/proxima-nova-semibold/proxima_nova_semibold_0-webfont.woff") format("woff"),url("/markup/fonts/proxima-nova-semibold/proxima_nova_semibold_0-webfont.ttf") format("truetype"),url("/markup/fonts/proxima-nova-semibold/proxima_nova_semibold_0-webfont.svg#proxima_nova_ltsemibold") format("svg");font-weight:normal;font-style:normal}
        @font-face{font-family:'proximanova-bold';src:url("/markup/fonts/proximanova/proximanova-bold/proximanova-bold-webfont.eot");src:url("/markup/fonts/proximanova/proximanova-bold/proximanova-bold-webfont.eot?#iefix") format("embedded-opentype"),url("/markup/fonts/proximanova/proximanova-bold/proximanova-bold-webfont.woff2") format("woff2"),url("/markup/fonts/proximanova/proximanova-bold/proximanova-bold-webfont.woff") format("woff"),url("/markup/fonts/proximanova/proximanova-bold/proximanova-bold-webfont.ttf") format("truetype"),url("/markup/fonts/proximanova/proximanova-bold/proximanova-bold-webfont.svg#proxima_nova_rgbold") format("svg");font-weight:normal;font-style:normal}
        .row{margin-left:-15px;margin-right:-15px}.row:before,.row:after{content:" ";display:table}.row:after{clear:both}
        .col-sm-12,.col-md-12,.col-lg-12{position:relative;min-height:1px;padding-left:15px;padding-right:15px}
        .country .col-xs-1-7, .country .col-sm-1-7, .country .col-md-1-7, .country .col-lg-1-7{position:relative;min-height:1px;padding-right:10px;padding-left:10px}
        .country .col-xs-1-7{width:14.28%;float:left}
        .country .wrapper{position:relative;padding-top:47%;background:url("/markup/country/img/background-wrapp.png") no-repeat center center fixed;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover}
        .country .country-container{text-align:center;background:#ffffff !important;margin-top:0;padding-bottom:5%}
        .country .country-container .logo{padding: 5% 25px 20px 30px; margin:0;}
        .country .country-container .location-ru h1, .country .country-container .location-eng h1{line-height: 1.1;font-family:"proxima_nova_semibold";color:#396080;font-size:16.2px;text-transform:uppercase;margin-top:15px;font-weight:800;margin-bottom:8px}
        .country .country-container .location-eng h1{margin-top:0}
        .country .country-container .country-row{margin:1.5% 0 0;padding:0 5%;border-bottom:1px solid #a8b9c7}
        .country .country-container .country-row ul{padding:0}.country .country-container .country-row .country-block{font-family:"proximanova-bold";color:#a8b9c7;list-style:none}
        .country .country-container .country-row .country-block .country-name{margin-top:6%;text-transform:uppercase;font-size:15px;letter-spacing:1.5px;line-height:20px;display:block;color:#a8b9c7;height:175px}
        .country .country-container .country-row .country-block .country-name img{margin:0 auto 6%;display:block}
        .country .country-container .country-row .country-name:hover, .country .country-container .country-row .country-name:focus{color:#396080;cursor:pointer;text-decoration:none}
        .country .country-container .country-row .country-name.active{color:#396080;border-bottom:3px solid #396080}
        .country .country-container .country-name:hover{border-bottom:3px solid #396080}
        .country {background:none !important;padding-left: 20px;padding-right: 20px; margin-top:6%; max-width:1040px; position:relative;}
        .country .close-btn {
            position: absolute;
            top: -25px;
            right: -12px;
        }

        @media (min-width: 1200px){
            .country .container{width:1000px}}
            @media (min-width: 1200px){
                .country .col-lg-1-7{width:14.28%;float:left}}
        @media (max-width: 1450px){
            .country .wrapper{padding-top:75%}}
        @media (max-width: 1000px){
            .country .wrapper{background-image:none}
            .country .country-container .country-row{border:none;padding:0 30%}}
        @media (min-width: 1001px){
            .country .col-md-1-7{width:14.28%;float:left}}
        @media (min-width: 768px){

            .country .col-sm-1-7{width:14.28%;float:left}}
        @media (max-width: 768px){
            .country .close-btn {
                right: -5px;
            }
        }
        @media (max-width: 500px){
            .country .country-container .country-row{padding:0 5%}}
    </style>

<div class="container country">
    <button class="close-btn" data-dismiss="modal">
        <img src="catalog/view/theme/default/image/popup-login/close-img.png" alt="123">
    </button>
        <div class=" country-container">

            <div class="row logo">
                <div class="col col-md-12">
                    <img src="catalog/view/theme/default/image/logo-img.png" alt="logo">
                </div>
            </div>
            <div class="row location-ru">
                <div class="col col-md-12">
                    <h1>Виберіть своє місцезнаходження<br/><br/>Select Your location</h1>
                </div>
            </div>
            <div class="row country-row">
                <div class="col col-md-1-7 col-sm-12 country-block">
                    <a href="<?php echo $countrySaveLink; ?>&country=Ukraine" class="country-name">
                        <img src="catalog/view/theme/default/image/country-img-01.png" alt="UA">
                        Ukraine
                    </a>
                </div>
                <div class="col col-md-1-7 col-sm-12 country-block">
                    <a href="//www.sammy-icon.ru" class="country-name">
                        <img src="catalog/view/theme/default/image/country-img-02.png" alt="RU">
                        Russia
                    </a>
                </div>
                <div class="col col-md-1-7 col-sm-12 country-block">
                    <a href="<?php echo $countrySaveLink; ?>&country=Americas" class="country-name">
                        <img src="catalog/view/theme/default/image/country-img-03.png" alt="Americas">
                        Americas
                    </a>
                </div>
                <div class="col col-md-1-7 col-sm-12 country-block">
                    <a href="//www.sammy-icon.de" class="country-name">
                        <img src="catalog/view/theme/default/image/country-img-04.png" alt="EU">
                        Europe
                    </a>
                </div>
                <div class="col col-md-1-7 col-sm-12 country-block">
                    <a href="<?php echo $countrySaveLink; ?>&country=Asia" class="country-name">
                        <img src="catalog/view/theme/default/image/country-img-05.png" alt="Asia">
                        Asia
                    </a>
                </div>
                <div class="col col-md-1-7 col-sm-12 country-block">
                    <a href="<?php echo $countrySaveLink; ?>&country=Middle+east" class="country-name">
                        <img src="catalog/view/theme/default/image/country-img-06.png" alt="Middle east">
                        Middle east
                    </a>
                </div>
                <div class="col col-md-1-7 col-sm-12 country-block">
                    <a href="<?php echo $countrySaveLink; ?>&country=Africa" class="country-name">
                        <img src="catalog/view/theme/default/image/country-img-07.png" alt="Africa">
                        Africa
                    </a>
                </div>

            </div>
        </div>


</div>
