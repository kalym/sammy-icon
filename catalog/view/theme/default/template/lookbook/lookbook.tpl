<?php echo $header;?>
<div class="lookbook-view blog">
    <div class="content-wrapper" >
        <div class="background-color"></div>
        <div class="container padding-bot">

            <div class="row article-title">
                <div class="col-md-12 top-img">
                    <img class="l-head-img" src="<?php echo $blog_image; ?>" alt="<?php echo $heading_title; ?>"
                         title="<?php echo $heading_title; ?>">

                    <h1><?php echo $heading_title; ?></h1>
                </div>
            </div>

            <?php echo $article_pt1; ?>
        </div>

    </div>

<?php echo $footer; ?>