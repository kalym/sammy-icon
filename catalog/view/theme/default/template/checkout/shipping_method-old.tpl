<?php
 //echo $info;
 if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($shipping_methods) { ?>
<p class="choise-delivery"><?php echo $text_shipping_method; ?></p>
<table>
<?php foreach ($shipping_methods as $shipping_method) { ?>

<?php if (!$shipping_method['error']) { ?>
<?php foreach ($shipping_method['quote'] as $quote) { ?>
<div class="radio">
  <tr class="delivery-list">
    <td class="first-td">
    <?php if ($quote['code'] == $code || !$code) { ?>
    <?php $code = $quote['code']; ?>
    <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" checked="checked" />
    <?php } else { ?>
    <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" />
    <?php } ?>
      </td>
      <td class="second-td"><img src="catalog/view/theme/default/image/icon/mail-img-1.png" alt=""></td>
      <td class="third-td">
        <span>
          <span class="blue">Sammy Icon:</span>
            <?php echo $quote['title']; ?></span>
      </td>
      <td class="fourth-td"><span class="blue"><?php echo $quote['text']; ?></span></td>
   </tr>
</div>
<?php } ?>
<?php } else { ?>
<div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
<?php } ?>
<?php } ?>
</table>
<?php } ?>

<div class="buttons next-button">

    <button id="button-shipping-method" data-loading-text="<?php echo $text_loading; ?>" class="center-block" ><?php echo $button_continue; ?></button>

</div>


