<div id="shipping-new" class="row info-log address-info">
  <form>
    <div class="row">
    <div class="col col-md-6 ">
      <input type="text" name="firstname"
             value="<?php echo isset($firstname) ?  $firstname : ''; ?>"
             placeholder="<?php echo $entry_firstname; ?>"
             id="input-shipping-firstname" />
      <span class="require">*</span>
    </div>
    <div class="col col-md-6 ">
      <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-shipping-telephone" />
      <span class="require">*</span>
    </div>
    </div>
    <div class="row">
    <?php if(isset($inpost)){ ?>
    <div class="col col-md-12">
      <select name="address_1" id="input-poshtomat"  class="form-control">
        <?php foreach ($inpost as $poshtomat) { ?>
          <option value="<?php echo $poshtomat; ?>"><?php  echo $poshtomat;  ?></option>
        <?php } ?>
      </select>
    </div>
    <?php } ?>
    <?php if(isset($worldwide)){ ?>
    <div class="col col-md-6">
      <select name="country_id" id="input-shipping-address-1"   class="form-control">
        <option value=""><?php echo $text_select; ?></option>
        <?php foreach ($countries as $country) { ?>
        <?php if ($country['country_id'] == $country_id) { ?>
        <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
      <?php if(!isset($worldwide)){ ?>
        <div class="col col-md-6">
          <input type="text" name="city" value="" placeholder="<?php echo $entry_city; ?>" id="input-shipping-city" class="form-control" />
          <span class="require">*</span>
        </div>
      <?php } ?>
      <div class="col col-md-6">
      <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-shipping-postcode" />
    </div>
    <?php } ?>

    <?php if(!isset($inpost) and !isset($pickup) and !isset($novaposhta)): ?>
      <div class="col col-md-6">
        <input type="text" name="address_1" value="" placeholder="<?php echo $entry_address_1; ?>" id="input-shipping-address-1"  />
        <span class="require">*</span>
        <input type="text" name="address_2" value="" placeholder="<?php echo $entry_address_2; ?>" id="input-shipping-address-2"  />
        <input type="text" name="address_3" value="" placeholder="<?php echo $entry_flat; ?>"  id="input-shipping-address-3" >
      </div>
    <?php endif ?>

    <?php if(isset($novaposhta)): ?>
      <div class="col col-md-6">
        <input type="text" name="city" value="" placeholder="<?php echo $entry_city; ?>" id="input-shipping-city" class="form-control" />
        <span class="require">*</span>
      </div>
      <div class="col col-md-6">
        <input type="text" name="warehouse" value="" placeholder="<?php echo $entry_warehouse; ?>" id="input-shipping-warehouse" class="form-control" />
        <span class="require">*</span>
      </div>
     <?php endif ?>

    <div class="col <?php  if(isset($inpost) or isset($pickup) or isset($novaposhta)){ echo 'col-md-12'; }else{ echo 'col-md-6'; }  ?>">
      <textarea type="text" class="" name="comment" placeholder="<?php echo $entry_comment; ?>" ></textarea>
    </div>
    </div>
  </form>
</div>

<div class="buttons clearfix next-button">
  <button id="button-shipping-address" data-loading-text="<?php echo $text_loading; ?>"  ><?php echo $button_continue; ?> </button>
</div>

<script type="text/javascript">
  $("#input-shipping-telephone").mask("+00(000) 000-00-00");
</script>

<script type="text/javascript"><!--
  $('#collapse-shipping-address button[id^=\'button-shipping-custom-field\']').on('click', function() {
    var node = this;

    $('#form-upload').remove();

    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

    $('#form-upload input[name=\'file\']').trigger('click');

    if (typeof timer != 'undefined') {
      clearInterval(timer);
    }

    timer = setInterval(function() {
      if ($('#form-upload input[name=\'file\']').val() != '') {
        clearInterval(timer);

        $.ajax({
          url: 'index.php?route=tool/upload',
          type: 'post',
          dataType: 'json',
          data: new FormData($('#form-upload')[0]),
          cache: false,
          contentType: false,
          processData: false,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {
            $(node).parent().find('.text-danger').remove();

            if (json['error']) {
              $(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
            }

            if (json['success']) {
              alert(json['success']);

              $(node).parent().find('input[name^=\'custom_field\']').attr('value', json['code']);
            }
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    }, 500);
  });
  //--></script>
<script type="text/javascript"><!--
  $('#collapse-shipping-address select[name=\'country_id\']').on('change', function() {
    $.ajax({
      url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
      dataType: 'json',
      beforeSend: function() {
        $('#collapse-shipping-address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
      },
      complete: function() {
        $('.fa-spin').remove();
      },
      success: function(json) {
        if (json['postcode_required'] == '1') {
          $('#collapse-shipping-address input[name=\'postcode\']').parent().parent().addClass('required');
        } else {
          $('#collapse-shipping-address input[name=\'postcode\']').parent().parent().removeClass('required');
        }

        html = '<option value=""><?php echo $text_select; ?></option>';

        if (json['zone'] && json['zone'] != '') {
          for (i = 0; i < json['zone'].length; i++) {
            html += '<option value="' + json['zone'][i]['zone_id'] + '"';

            if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
              html += ' selected="selected"';
            }

            html += '>' + json['zone'][i]['name'] + '</option>';
          }
        } else {
          html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
        }

        $('#collapse-shipping-address select[name=\'zone_id\']').html(html);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });

  $('#collapse-shipping-address select[name=\'country_id\']').trigger('change');
  //--></script>