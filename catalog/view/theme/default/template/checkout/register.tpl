<div class="col col-md-6 col-sm-6 col-xs-6 top-elems-button pull-right">
  <button class="pull-right  cart-log-button" id="button-go-login"  data-loading-text="<?php echo $text_loading; ?>" >Вхід</button>
</div>

<div class="row info-log">
  <form>
    <div class="col col-md-6">
      <input  type="text" name="firstname" value="" placeholder="<?php echo $entry_firstname; ?>" id="input-payment-firstname"  >
    </div>
    <div class="col col-md-6">
      <input type="text" name="lastname" value="" placeholder="<?php echo $entry_lastname; ?>" id="input-payment-lastname"  />
    </div>
    <div class="col col-md-6">
      <input type="text" name="telephone" value="" placeholder="<?php echo $entry_telephone; ?>" id="input-payment-telephone"  >
    </div>
    <div class="col col-md-6">
      <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-payment-email"  />
    </div>
    <div class="col col-md-6">
      <input type="password" name="password" value="" placeholder="<?php echo $entry_password; ?>" id="input-payment-password" />
    </div>
    <div class="col col-md-6">
      <input type="password" name="confirm" value="" placeholder="<?php echo $entry_confirm; ?>" id="input-payment-confirm"  />
    </div>
  </form>
</div>
<div class="row social-buttons log-page">
  <div class="col col-md-4 col-sm-12">
    <button class="social-button facebook">
      <a href="#">
        <img src="catalog/view/theme/default/image/icon/facebook-img.png" alt="123" />
        Facebook
      </a>
    </button>
  </div>
  <div class="col col-md-4 col-sm-12">
    <button class="social-button vkontakte">
      <a href="#">
        <img src="catalog/view/theme/default/image/icon/vkontakte-img.png" alt="123" />
        Vkontakte
      </a>
    </button>
  </div>
  <div class="col col-md-4 col-sm-12">
    <button class="social-button google">
      <a href="#">
        <img src="catalog/view/theme/default/image/icon/google-img.png" alt="123" />
        Google
      </a>
    </button>
  </div>
</div>
<div class="row next-button log-page">
  <div class="col col-md-12">
    <button id="button-register" data-loading-text="<?php echo $text_loading; ?>"  ><?php echo $button_continue; ?></button>
  </div>
</div>

<script type="text/javascript">
  $("#input-payment-telephone").mask("+99(999) 999-99-99");
</script>