<div class="row info-log">
  <form class="address-info first-step">
    <div class="col col-md-6">

      <input type="text"  name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-checkout-email" >
      <span class="require">*</span>

    </div>
    <div class="col col-md-6">
      <input type="text" name="first" value="<?php echo $telephone; ?>"  placeholder="<?php echo $entry_telephone; ?>"  id="input-checkout-telephone"  />
      <span class="require">*</span>
    </div>
    <div class="col col-md-12">
      <div class="col col-md-6">
        <input type="text"  name="text" value="<?php echo $firstname; ?>"  placeholder="<?php echo $entry_firstname; ?>" id="input-checkout-firstname" >
        <span class="require">*</span>
      </div>

      <div class="col col-md-6">
        <input type="text"  name="text" value="<?php echo $lastname; ?>"  placeholder="<?php echo $entry_lastname; ?>" id="input-checkout-lastname" >
        <span class="require">*</span>
      </div>
    <div>
  </form>
</div>
<div class="row next-button log-page">
  <div class="col col-md-12">
    <button id="button-save-guest" ><?php echo $button_continue; ?></button>
  </div>
</div>

<script type="text/javascript">
    $("#input-checkout-telephone").mask("+00(000) 000-00-00");
</script>