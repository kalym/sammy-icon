<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>

<?php if ($payment_methods) { ?>
<table>
    <?php foreach ($payment_methods as $payment_method) { ?>
        <tr class="col col-md-12 delivery-list">
            <td class="first-td">
                <?php if ($payment_method['code'] == $code || !$code) { ?>
                <?php $code = $payment_method['code']; ?>
                <input  id="<?php echo $payment_method['code']; ?>"  type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" checked="checked" />
                <label for="<?php echo $payment_method['code']; ?>"></label>
                <?php } else { ?>
                <input  id="<?php echo $payment_method['code']; ?>"   type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" />
                <label for="<?php echo $payment_method['code']; ?>"></label>
                <?php } ?>

                <?php if ($payment_method['terms']) { ?>
                (<?php echo $payment_method['terms']; ?>)
                <?php } ?>
            </td>
            <td class="second-td">
                <img
                    <?php if ($payment_method['code'] == $code || !$code) {
                            echo 'class="active"';
                        } ?>
                src="/image/icon/<?php echo $payment_method['code']; ?>.png" alt="<?php echo $payment_method['title']; ?>" >
            </td>
            <td class="third-td">
                <span>
                     <?php echo $payment_method['title']; ?>
                </span>
            </td>
        </tr>
    <?php } ?>
</table>
<?php } ?>
<input type="checkbox" name="donotcall" id="donotcall" />
<label for="donotcall"><?php echo $text_do_not_call; ?></label>
<div class="buttons next-button">
    <button type="button"  id="button-payment-method"  class="center-block" ><?php echo $button_continue; ?></button>
</div>

