<?php echo $header; ?>
<div class="container cabinet">

    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> col-md-8 col-md-offset-1"><?php echo $content_top; ?>

            <p class=" text-center form-title"><?php echo $text_hello; ?>, <?php echo (!empty($firstname)) ? $firstname . ' ' . $lastname : $text_customer ; ?>!</p>

            <a href="<?php echo $continue; ?>" class="semibold pull-right blue"><?php echo $button_return; ?></a>
            <p class="blue semibold"><?php echo $text_order_id; ?> <?php echo $order_id; ?></p>
            <div class="separator"></div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover order-info-table">
                    <thead>
                    <thead>
                    <tr>
                        <td class="text-left"><?php echo $column_image; ?></td>
                        <td class="text-left"><?php echo $column_name; ?></td>
                        <td class="text-center"><?php echo $column_quantity; ?></td>
                        <td class="text-left"><?php echo $column_size; ?></td>
                        <td class="text-left"><?php echo $column_total; ?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($products as $product) { ?>
                    <tr>
                        <td class="text-left"><img src="<?php echo $product['thumb']; ?>"></td>
                        <td class="text-left"><?php echo $product['name']; ?></td>
                        <td class="text-right"><?php echo $product['quantity']; ?></td>
                        <td class="text-right"><?php foreach ($product['option'] as $option) { ?>
                            <?php echo $option['value']; ?>
                            <?php } ?></td>
                        <td class="text-right"><?php echo $product['total']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php foreach ($vouchers as $voucher) { ?>
                    <tr>
                        <td class="text-left"><?php echo $voucher['description']; ?></td>
                        <td class="text-left"></td>
                        <td class="text-right">1</td>
                        <td class="text-right"><?php echo $voucher['amount']; ?></td>
                        <td class="text-right"><?php echo $voucher['amount']; ?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <?php foreach ($totals as $total) { ?>
                    <tr>
                        <td colspan="3"></td>
                        <td class="text-right"><b><?php echo $total['title']; ?></b></td>
                        <td class="text-right"><?php echo $total['text']; ?></td>
                    </tr>
                    <?php } ?>
                    </tfoot>
                </table>
            </div>

            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>