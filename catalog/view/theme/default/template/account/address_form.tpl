<?php echo $header; ?>
<div class="container cabinet">

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> col-md-8 col-md-offset-1 l-border"> <?php echo $content_top; ?>

            <p class="strong text-center form-title"><?php echo $text_hello; ?>, <?php echo (!empty($firstname)) ? $firstname . ' ' . $lastname : $text_customer ; ?>!</p>


            <!--<h2>--><?php //echo $text_edit_address; ?><!--</h2>-->

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="user_info">

                <a href="<?php echo $back; ?>" class="pull-right blue semibold"><?php echo $button_back; ?></a>
                <p class="form-title-small blue semibold"><?php echo $address_form_label;?></p>
                <div class="form-group">
                    <div class=" col-md-3"><label class="label" for="input-firstname"><?php echo $entry_firstname; ?></label></div>
                    <div class="col-md-6"><input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" /></div>
                </div>
                <div class="form-group">
                    <div class=" col-md-3"><label class="label" for="input-lastname"><?php echo $entry_lastname; ?></label></div>
                    <div class="col-md-6"><input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" /></div>
                </div>
                <div class="form-group">
                    <div class=" col-md-3"><label class="label" for="input-postcode"><?php echo $entry_postcode; ?></label></div>
                    <div class="col-md-6"><input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control" /></div>
                </div>
                <?php foreach ($custom_fields as $custom_field) { ?>
                <?php if ($custom_field['location'] == 'address') { ?>
                <?php if ($custom_field['type'] == 'text') { ?>
                <div class="form-group custom-field" >
                    <div class=" col-md-3"><label class="label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label></div>
                    <div class="col-md-6"><input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" /></div>
                </div>
                <?php } ?>
                <?php } ?>
                <?php } ?>
                <div class="form-group">

                    <div class="col-md-6 col-md-offset-3">
                        <?php if ($default) { ?>
                        <input id="cfirst" type="checkbox" name="default" value="1" checked hidden />
                        <?php } else { ?>
                        <input id="cfirst" type="checkbox" name="default" value="1" hidden />
                        <?php } ?>
                        <label class="adress-new" for="cfirst"><span><?php echo $entry_default; ?></span></label>
                    </div>
                </div>
                <div class="form-group text-left ">
                    <div class="col-md-offset-3 col-md-6 ">
                        <button type="submit" form="user_info" class=" btn btn-default btn-lg"><?php echo $button_continue; ?></button>
                    </div>
                </div>

            </form>


            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
    // Sort the custom fields
    $('.form-group[data-sort]').detach().each(function() {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length-2) {
            $('.form-group').eq(parseInt($(this).attr('data-sort'))+2).before(this);
        }

        if ($(this).attr('data-sort') > $('.form-group').length-2) {
            $('.form-group:last').after(this);
        }

        if ($(this).attr('data-sort') == $('.form-group').length-2) {
            $('.form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('.form-group').length-2) {
            $('.form-group:first').before(this);
        }
    });
    //--></script>
<script type="text/javascript"><!--
    $('button[id^=\'button-custom-field\']').on('click', function() {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        ////$(node).button('reset');
                    },
                    complete: function() {
                        $(node).button('reset');
                    },
                    success: function(json) {
                        $(node).parent().find('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });
    //--></script>
<script type="text/javascript"><!--
    $('select[name=\'country_id\']').on('change', function() {
        $.ajax({
            url: 'index.php?route=account/account/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function() {
                $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function() {
                $('.fa-spin').remove();
            },
            success: function(json) {
                if (json['postcode_required'] == '1') {
                    $('input[name=\'postcode\']').parent().parent().addClass('required');
                } else {
                    $('input[name=\'postcode\']').parent().parent().removeClass('required');
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] && json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('select[name=\'zone_id\']').html(html);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'country_id\']').trigger('change');
    //--></script>
<?php echo $footer; ?>
