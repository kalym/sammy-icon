﻿<?php echo $header; ?>
<div class="container cabinet">
  <div class="row">
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-offset-3 col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-offset-3 col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-offset-3 col-sm-9'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $text_password; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <div class="form-group">
            <label class="col-sm-4 control-label " for="input-password"><?php echo $entry_password; ?></label>
            <div class="col-sm-4">
              <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="form-control" />
              <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label " for="input-confirm"><?php echo $entry_confirm; ?></label>
            <div class="col-sm-4">
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm" class="form-control" />
              <?php if ($error_confirm) { ?>
              <div class="text-danger"><?php echo $error_confirm; ?></div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix bottom-margin-lg">
          <div class="row">
            <div class="col-sm-4"><a href="<?php echo $back; ?>" class="btn btn-default btn-back"><?php echo $button_back; ?></a></div>
            <div class="col-sm-4"><input type="submit" class="btn pull-right btn-default" value="<?php echo $button_continue; ?>" /></div>
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>