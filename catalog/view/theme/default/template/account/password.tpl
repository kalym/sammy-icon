<?php echo $header; ?>
<div class="container cabinet">

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> col-md-8 col-md-offset-1 l-border"><?php echo $content_top; ?>

            <p class="strong text-center form-title"><?php echo $text_hello; ?>, <?php echo (!empty($firstname)) ? $firstname . ' ' . $lastname : $text_customer ; ?>!</p>

            <form id="password" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

                <a href="<?php echo $back; ?>" class="strong pull-right blue"><?php echo $button_back; ?></a>
                <p class="blue strong"><?php echo $text_password; ?></p>
                <div class="form-group">
                    <div class="col-md-4 "><label class="control-label" for="input-password"><?php echo $entry_password; ?></label></div>
                    <div class="col-md-4"><input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" /></div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 "><label class="control-label" for="input-confirm"><?php echo $entry_confirm; ?></label></div>
                    <div class="col-md-4"><input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" /></div>
                </div>
                <div class="form-group ">
                    <div class="col-md-offset-4 col-md-4 ">
                        <button type="submit" form="password" style="width: 100%" class=" btn btn-default btn-lg"><?php echo $button_continue; ?></button>
                    </div>
                </div>

            </form>




            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>