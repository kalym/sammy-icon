<?php echo $header; ?>
<div class="container cabinet">
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content col-md-8 col-md-offset-1 text-center bottom-margin-md" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <!-- <h2><?php echo $heading_title; ?></h2> -->
            <p class="strong text-center form-title"><?php echo $text_hello; ?>, <?php echo (!empty($firstname)) ? $firstname . ' ' . $lastname : $text_customer ; ?>!</p>

            <?php if ($products) { ?>
            <p class="cabinet-l-text text-center"><?php echo $text_wishlist_label;?></p>

            <?php foreach ($products as $product) { ?>

            <div class="col col-md-4 goods-block">
                <?php if ($product['thumb']) { ?>
                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="goods-img"/></a>
                <?php } else { ?>
                <a href="<?php echo $product['href']; ?>"><img src="image/no_image.png" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="goods-img"/></a>
                <?php }?>
                <a href="<?php echo $product['remove']; ?>"><img src="catalog/view/theme/default/image/icon/delete.png" class="review-goods-img" alt="123" /></a>
                <div class="goods-text-area">
                    <a href="<?php echo $product['href']; ?>" class="goods-name"><?php echo $product['name']; ?></a>
                    <?php if ($product['price']) { ?>
                    <?php if (!$product['special']) { ?>
                    <a href="<?php echo $product['href']; ?>" class="goods-price pull-right"><?php echo $product['price']; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $product['href']; ?>" class="goods-price pull-right"><b><?php echo $product['special']; ?></b> <s><?php echo $product['price']; ?></s></a>
                    <?php } ?>
                    <?php } ?>
                    <p>
                        <?php  if ($product['options']) {
                            echo $product['options'];
                        } ?>
                    </p>
                </div>
            </div>
            <?php } ?>
            <?php } else { ?>
            <p class="strong blue uppercase top-margin-lg font-md  text-center"><?php echo $text_empty;?></p>
            <p class="text-center"><?php echo $text_wishlist_label;?></p>
            <a href="/index.php?route=common/home" class="bottom-margin-md block-center text-center"><?php echo $back_to_store; ?></a>
            <?php } ?>
            <div class="buttons clearfix">
                <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>