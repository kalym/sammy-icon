<?php echo $header; ?>
<div class="container cabinet">
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> col-md-8 col-md-offset-1 text-center bottom-margin-md"><?php echo $content_top; ?>

            <p class="strong text-center form-title"><?php echo $text_hello; ?>, <?php echo (!empty($firstname)) ? $firstname . ' ' . $lastname : $text_customer ; ?>!</p>


            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?> 