<?php echo $header; ?>
<div class="container cabinet">

  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row">

    <?php $class = 'col-sm-9'; ?>

    <div id="content" class="col-sm-12 "><?php echo $content_top; ?>
        <div class="header">
          <img src="catalog/view/theme/default/image/popup-login/subscription--bg-img.png" alt="123" />
        </div>
        <div class="middle-contant">
          <div class="row">
            <div class="col col-md-6 text-right login">
              <h2><?php echo $text_enter; ?></h2>
              <input type="text" name="email"  placeholder="E-mail">
              <input type="password"  name="password"  placeholder="<?php echo $text_password; ?>">
              <a href="<?php echo $url_forgotten; ?>"><?php echo $text_forgotten; ?></a>
              <button id="account-login"><?php echo $text_login; ?></button>
            </div>
            <div class="col col-md-6 separator-left register">
              <h2><?php echo $text_registration; ?></h2>
              <input type="text" name="email"  placeholder="E-mail">
              <input type="password" name="password"  placeholder="<?php echo $text_password; ?>">
              <input type="password" name="confirm"  placeholder="<?php echo $text_confirm; ?>">
              <button id="account-register" class="l-margin"><?php echo $text_signup; ?></button>
            </div>
          </div>
        </div>
        <div class="row social-buttons log-page">
          <div class="col col-md-12">
            <h2><?php echo $text_login_with; ?></h2>
          </div>
          <div class="col col-md-4 col-sm-12">
            <button class="social-button facebook">
              <a href="#">
                <img src="catalog/view/theme/default/image/popup-login/facebook-img.png" alt="123" />
                Facebook
              </a>
            </button>
          </div>
          <div class="col col-md-4 col-sm-12">
            <button class="social-button vkontakte">
              <a href="#">
                <img src="catalog/view/theme/default/image/popup-login/vkontakte-img.png" alt="123" />
                Vkontakte
              </a>

            </button>
          </div>
          <div class="col col-md-4 col-sm-12">
            <button class="social-button google">
              <a href="#">
                <img src="catalog/view/theme/default/image/popup-login/google-img.png" alt="123" />
                Google
              </a>
            </button>
          </div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>