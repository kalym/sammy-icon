<?php echo $header; ?>
<div class="container cabinet">

  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row">
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-offset-3 col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-offset-3 col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-offset-3 col-sm-9'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
      <div class="col-sm-10">
        <h1><?php echo $heading_title; ?></h1>
        <p><?php echo $text_email; ?></p>
      </div>
      </div>
      <div class="row">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
            <label class="col-sm-4 control-label mail-input-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-4">
              <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
            </div>
        </fieldset>
        <div class="buttons clearfix col-sm-8 bottom-margin-lg">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default btn-back"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn  btn-default" />
          </div>
        </div>
      </form>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>