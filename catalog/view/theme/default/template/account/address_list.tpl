<?php echo $header; ?>
<div class="container cabinet">

    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> col-md-8 col-md-offset-1"><?php echo $content_top; ?>

            <p class="strong text-center form-title"><?php echo $text_hello; ?>, <?php echo (!empty($firstname)) ? $firstname . ' ' . $lastname : $text_customer ; ?>!</p>


            <!--        <h2>--><?php //echo $text_address_book; ?><!--</h2>-->

            <?php if ($addresses) { ?>
            <?php foreach ($addresses as $key => $result) { ?>
            <div class="col-md-4 nopadding">
                <p class="strong"><?php echo $address_list_label; ?> <?php echo $key+1; ?>:</p>
            </div>
            <div class="col-md-6">
                <p class="strong right-text">
                    <?php echo $result['address']; ?>
                </p>
                <a class="strong address-button" href="<?php echo $result['update']; ?>"><?php echo $button_edit; ?></a>
                <a class="strong address-button" href="<?php echo $result['delete']; ?>"><?php echo $button_delete; ?></a>
            </div>

            <!--          <tr>-->
            <!--            <td class="text-left">--><?php //echo $result['address']; ?><!--</td>-->
            <!--            <td class="text-right"><a href="--><?php //echo $result['update']; ?><!--" class="btn btn-info">--><?php //echo $button_edit; ?><!--</a> &nbsp; <a href="--><?php //echo $result['delete']; ?><!--" class="btn btn-danger">--><?php //echo $button_delete; ?><!--</a></td>-->
            <!--          </tr>-->
            <?php } ?>
            <a href="<?php echo $add; ?>"><button type="submit" class="top-margin-md button-l bottom-margin-xxxlg btn btn-default btn-lg"><span class="plus">+</span><?php echo $button_new_address; ?></button></a>

            <?php } else { ?>
            <p><?php echo $text_empty; ?></p>
            <?php } ?>
            <!--<div class="buttons clearfix">
                <div class="pull-left"><a href="<?php //echo $back; ?>" class="btn btn-default"><?php //echo $button_back; ?></a></div>
                <div class="pull-right"><a href="<?php //echo $add; ?>" class="btn btn-primary"><?php //echo $button_new_address; ?></a></div>
            </div> -->

            <!--        --><?php //if ($addresses) { ?>
            <!--      <div class="table-responsive">-->
            <!--        <table class="table table-bordered table-hover">-->
            <!--          --><?php //foreach ($addresses as $result) { ?>
            <!--          <tr>-->
            <!--            <td class="text-left">--><?php //echo $result['address']; ?><!--</td>-->
            <!--            <td class="text-right"><a href="--><?php //echo $result['update']; ?><!--" class="btn btn-info">--><?php //echo $button_edit; ?><!--</a> &nbsp; <a href="--><?php //echo $result['delete']; ?><!--" class="btn btn-danger">--><?php //echo $button_delete; ?><!--</a></td>-->
            <!--          </tr>-->
            <!--          --><?php //} ?>
            <!--        </table>-->
            <!--      </div>-->
            <!--      --><?php //} else { ?>
            <!--      <p>--><?php //echo $text_empty; ?><!--</p>-->
            <!--      --><?php //} ?>
            <!--      <div class="buttons clearfix">-->
            <!--        <div class="pull-left"><a href="--><?php //echo $back; ?><!--" class="btn btn-default">--><?php //echo $button_back; ?><!--</a></div>-->
            <!--        <div class="pull-right"><a href="--><?php //echo $add; ?><!--" class="btn btn-primary">--><?php //echo $button_new_address; ?><!--</a></div>-->
            <!--      </div>-->



            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>