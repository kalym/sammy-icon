<?php echo $header; ?>
<div class="container cabinet">

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

            <p class="strong text-center form-title"><?php echo $text_hello; ?>, <?php echo (!empty($firstname)) ? $firstname . ' ' . $lastname : $text_customer ; ?>!</p>


            <?php if ($orders) { ?>
            <table class="table table-order">
                <thead>
                <tr>
                    <th><?php echo $column_order_id; ?></th>
                    <th><?php echo $column_date_added; ?></th>
                    <th><?php echo $column_total; ?></th>
                    <th><?php echo $column_status; ?></th>
                    <th class="text-center"><?php echo $column_details; ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($orders as $order) { ?>
                <tr>
                    <td>#<?php echo $order['order_id']; ?></td>
                    <td><?php echo $order['date_added']; ?></td>
                    <td><?php echo $order['total']; ?></td>
                    <td><?php echo $order['status']; ?></td>
                    <td class="text-right"><a href="<?php echo $order['view']; ?>"><?php echo $button_view; ?></a></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php } else { ?>
            <p><?php echo $text_empty; ?></p>
            <?php } ?>


            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>