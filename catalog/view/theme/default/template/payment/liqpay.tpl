<form id="liqpay" action="<?php echo $action; ?>" method="post">
  <input type="hidden" name="operation_xml" value="<?php echo $xml; ?>">
  <input type="hidden" name="signature" value="<?php echo $signature; ?>">
  <div class="buttons next-button">
      <button type="submit" class="center-block" ><?php echo $button_confirm; ?></button>
  </div>
</form>
