<?php echo $header; ?>
<div class="container">

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">

      <div class="top-elems">
        <img src="catalog/view/theme/default/image/contacts-page-background-img.png" alt="Contact Sammy Icon" class="img-responsive" />

         <?php echo $content_top; ?>

      <?php if ($locations) { ?>
      <h3><?php echo $text_store; ?></h3>
      <div class="panel-group" id="accordion">
        <?php foreach ($locations as $location) { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><a href="#collapse-location<?php echo $location['location_id']; ?>" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $location['name']; ?> <i class="fa fa-caret-down"></i></a></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-location<?php echo $location['location_id']; ?>">
            <div class="panel-body">
              <div class="row">
                <?php if ($location['image']) { ?>
                <div class="col-sm-3"><img src="<?php echo $location['image']; ?>" alt="<?php echo $location['name']; ?>" title="<?php echo $location['name']; ?>" class="img-thumbnail" /></div>
                <?php } ?>
                <div class="col-sm-3"><strong><?php echo $location['name']; ?></strong><br />
                  <address>
                  <?php echo $location['address']; ?>
                  </address>
                  <?php if ($location['geocode']) { ?>
                  <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
                  <?php } ?>
                </div>
                <div class="col-sm-3"> <strong><?php echo $text_telephone; ?></strong><br>
                  <?php echo $location['telephone']; ?><br />
                  <br />
                  <?php if ($location['fax']) { ?>
                  <strong><?php echo $text_fax; ?></strong><br>
                  <?php echo $location['fax']; ?>
                  <?php } ?>
                </div>
                <div class="col-sm-3">
                  <?php if ($location['open']) { ?>
                  <strong><?php echo $text_open; ?></strong><br />
                  <?php echo $location['open']; ?><br />
                  <br />
                  <?php } ?>
                  <?php if ($location['comment']) { ?>
                  <strong><?php echo $text_comment; ?></strong><br />
                  <?php echo $location['comment']; ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      </div>

      <div class="row info-wrapp">
      <div class="col col-md-5 info-block">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

            <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name"  />
            <?php if ($error_name) { ?>
            <div class="text-danger"><?php echo $error_name; ?></div>
            <?php } ?>

            <select name="subject" id="input-subject">
              <option selected="selected"><?php echo $entry_subject; ?></option>
              <?php foreach($subjects as $subject) { ?>
                <option value="<?php echo $subject; ?>"><?php echo $subject; ?></option>
              <?php } ?>
            </select>

            <input type="text" name="email" placeholder="<?php echo $entry_email; ?>" value="<?php echo $email; ?>" id="input-email" />
            <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
            <?php } ?>

            <input type="text" name="order_id" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" />

            <textarea name="enquiry" rows="6"  placeholder="<?php echo $entry_enquiry; ?>" id="input-enquiry"></textarea>
            <?php if ($error_enquiry) { ?>
              <div class="text-danger"><?php echo $error_enquiry; ?></div>
            <?php } ?>


          <?php echo $captcha; ?>

        <div class="button-wrapp">

            <button type="submit" ><?php echo $button_submit; ?></button>

        </div>
      </form>

      </div>
      <div class="col col-md-7 map-wrapp">
        <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAXcldIX6JAYfzzO-E4n_sFJNjeYVbl3e8'></script><div style='overflow:hidden;height:406px;width:100%;'><div id='gmap_canvas' style='height:406px;width:100%;'></div><div><small><a href="//embedgooglemaps.com">									embedgooglemaps.com							</a></small></div><div><small><a href="//buyproxies.io/">elite proxies</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><script type='text/javascript'>function init_map(){var myOptions = {zoom:14,center:new google.maps.LatLng(50.445842, 30.523156),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(50.450998, 30.522501)});infowindow = new google.maps.InfoWindow({content:'<strong>Sammy Icon</strong><br><?php echo $text_map; ?><br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);;marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(50.438786, 30.523096)});infowindow = new google.maps.InfoWindow({content:'<strong>Sammy Icon</strong><br><?php echo $text_map1; ?><br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
      </div>
      </div>
      <?php echo $content_bottom; ?></>
    <?php echo $column_right; ?></div>
  </div>
</div>
</div>
<?php echo $footer; ?>
