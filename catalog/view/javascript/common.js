$(document).ready(function () {

    $(document).on('click', '#checkout-btn', function (e) {
        if ($(window).width() < 501) {
            e.preventDefault();
            showSmartCheckout();
        }
    });

    // be a partner
    $('#be-a-partner').on("click", function () {
        var warningContainer = $('#alert-panel');
        var emailInput = $(this).parent().find("#partner_email");
        var email = emailInput.val();
        $.ajax({
            url: "/index.php?route=feed/partner",
            type: "post",
            data: {email: email},
            success: function (response) {
                if (response['error_email']) {
                    warningContainer.html('<div class="alert alert-danger">' + response['error_email'] + '</div>');
                } else {
                    warningContainer.html('<div class="alert alert-success">' + response['success'] + '</div>');
                }
                emailInput.val('');
            }
        });
    });

    $(document).on('click', '#cart .remove-item', function (event) {
        event.stopPropagation();
        var productId = $(this).data('product-id');
        cart.remove(productId);
    });

    $('.remove_product_from_cart').on('click', function () {
        $(this).closest('.row').remove();

        var productQuantityContainer = $(this).parent().parent().parent().find('.quantity');
        var productQuantity = productQuantityContainer.text() * 1;
        var productId = productQuantityContainer.data('pid');
        var productPriceContainer = $(this).parent().parent().parent().find('.lprice');
        var productPriceValue = productPriceContainer.text();
        var productPrice = parseInt(productPriceValue);

        convead('event', 'update_cart', {
            items: [
                {product_id: productId, qnt: productQuantity, price: productPrice}
            ]
        });
    });

    //blog page button tricks
    $('.blog-page button, .lookbook-page button').on('click', function () {
        $('.blog-page button').off();
        location = $(this).find('a').attr('href');
    });

    // Scroll top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
            $('.cart-wrapper').addClass('fixed');
        } else {
            $('.scrollToTop').fadeOut();
            $('.cart-wrapper').removeClass('fixed');
        }
    });
    $('.scrollToTop').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });


    $('#subscribe_form').on('submit', function (event) {
        event.preventDefault();
        $.ajax({
            url: 'index.php?route=common/footer/subscribe',
            dataType: 'html',
            type: 'post',
            data: {email: $(this).parent().find("#subscribe_email").val()},
            success: function (html) {
                alert("Вітаю! Ви успішно підписались на новини.");
            }
        });
    });


    // ENTER event on login form
    $('.subscription-page .login input[name=password]').on('keypress', function (key) {
        if (key.which == 13) {
            $('#popup-login').trigger('click');
        }
    });
    $('.cabinet .login input[name=password]').on('keypress', function (key) {
        if (key.which == 13) {
            $('#account-login').trigger('click');
        }
    });


    // Adding the clear Fix
    cols1 = $('#column-right, #column-left').length;

    if (cols1 == 2) {
        $('#content .product-layout:nth-child(2n+2)').after('<div class="clearfix visible-md visible-sm"></div>');
    } else if (cols1 == 1) {
        $('#content .product-layout:nth-child(3n+3)').after('<div class="clearfix visible-lg"></div>');
    } else {
        $('#content .product-layout:nth-child(4n+4)').after('<div class="clearfix"></div>');
    }

    // Highlight any found errors
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });

    // Currency
    $('#currency .currency-select').on('click', function (e) {
        e.preventDefault();

        $('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

        $('#currency').submit();
    });

    // Language
    $('#language a').on('click', function (e) {
        e.preventDefault();

        $('#language input[name=\'code\']').attr('value', $(this).attr('href'));

        $('#language').submit();
    });

    // Language 2
    $('#form-language .language-select').on('click', function (e) {
        e.preventDefault();

        $('#form-language input[name=\'code\']').attr('value', $(this).attr('name'));

        $('#form-language').submit();
    });

    // Popup Login and Register
    $('#popup-login, #account-login').on('click', function () {

        var modal = $(this).closest('.container');

        $.ajax({
            url: 'index.php?route=account/login/login',
            type: 'post',
            data: $(this).parent().find('input'),
            dataType: 'json',
            success: function (json) {
                if (json["reload"]) {
                    location.reload();
                } else if (json["redirect"]) {
                    location = json["redirect"];
                } else if (json["error"]) {
                    modal.find('.alert-danger').remove();
                    modal.find('.header').append('<div class="alert alert-danger"><i class="fa fa-exclamation-circle">' + json["error"] + '</div>');
                }
            }
        });
    });


    $('#popup-register, #account-register').on('click', function () {

        var modal = $(this).closest('.container');
        console.log(modal);
        $.ajax({
            url: 'index.php?route=account/register/save',
            type: 'post',
            data: $(this).parent().find('input'),
            dataType: 'json',
            success: function (json) {
                if (json["error"]) {
                    modal.find('.alert-danger').remove();
                    modal.find('.header').append('<div class="alert alert-danger"><i class="fa fa-exclamation-circle">' + json["error"] + '</div>');

                } else if (json["redirect"]) {
                    location = json["redirect"];
                }
            }
        });
    });

    $(document).on('click', '.vkontakte a', function (event) {
        event.preventDefault();

        $.ajax({
            url: 'index.php?route=account/login/redirectToVk',
            async: false,
            datatype: "html",
            success: function (response) {
                window.parent.location.href = response;
            },
            error: function (errpr) {
                console.log(errpr);
            }
        });
    });

    $('#show-more').on('click', function (event) {
        event.preventDefault();
        var button = $(this);
        var url = button.attr('href'); //
        var newUrlParts = url.split('limit=');
        newUrlParts[1] = parseInt(newUrlParts[1]) + 6;

        $.ajax({
            url: url + '&showmore',
            datatype: "html",
            type: 'get'
        }).done(function (res) {
            if (res) {
                $('#product-row').html(res);
            } else {
                button.hide();
            }
            button.attr('href', newUrlParts[0] + 'limit=' + newUrlParts[1]);
        });
    });

    $(document).on('click', '.facebook', function (event) {
        event.preventDefault();

        $.ajax({
            url: 'index.php?route=account/login/redirectToFacebook',
            async: false,
            datatype: "html",
            success: function (response) {
                window.parent.location.href = response;
            },
            error: function (errpr) {
                console.log(errpr);
            }
        });
    });

    $(document).on('click', '.google', function (event) {
        event.preventDefault();

        $.ajax({
            url: 'index.php?route=account/login/redirectToGooglePlus',
            async: false,
            datatype: "html",
            success: function (response) {
                window.parent.location.href = response;
            },
            error: function (errpr) {
                console.log(errpr);
            }
        });
    });


    /* Search */
    $('#search .close').on('click', function () {
        $(this).parent().parent().hide();
    });

    $('#search-button').on('click', function () {
        if ($(window).width() > '767') {
            $('.search-block').show();
        } else {
            location = 'index.php?route=product/search';
        }
    });

    $('#search input[name=\'search\']').on('keydown', function (e) {

        url = $('base').attr('href') + 'index.php?route=product/search';
        var value = $('header input[name=\'search\']').val();

        fbq('track', 'Search');

        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }

        if (e.keyCode == 13) {
            location = url;
        } else {
            $.ajax({
                url: url + '&ajax=1',
                success: function (html) {
                    $('#search .preresult-place').html(html);
                }
            });
        }

    });


    $('.product-quantity .minus,.product-quantity .plus').on('click', function () {
        var productQuantityContainer = $(this).parent().find('.quantity');
        var productQuantity = productQuantityContainer.text() * 1;
        var productId = productQuantityContainer.data('pid');
        var productPriceContainer = $(this).parent().parent().parent().find('.lprice');
        var productPriceValue = productPriceContainer.text();
        var productPrice = parseInt(productPriceValue);

        if ($(this).hasClass('minus') && productQuantity > 1) {
            --productQuantity;
            convead('event', 'update_cart', {
                items: [
                    {product_id: productId, qnt: productQuantity, price: productPrice}
                ]
            });
        } else if ($(this).hasClass('plus')) {
            ++productQuantity;
            convead('event', 'update_cart', {
                items: [
                    {product_id: productId, qnt: productQuantity, price: productPrice}
                ]
            });
        }

        productQuantityContainer.text(productQuantity);
        cart.update(productId, productQuantity);
    });

    // Menu
    $('#menu .dropdown-menu').each(function () {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();

        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 5) + 'px');
        }
    });

    // Product List
    $('#list-view').click(function () {
        $('#content .product-layout > .clearfix').remove();

        //$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
        $('#content .row > .product-layout').attr('class', 'product-layout product-list col-xs-12');

        localStorage.setItem('display', 'list');
    });

    // Product Grid
    $('#grid-view').click(function () {
        $('#content .product-layout > .clearfix').remove();

        // What a shame bootstrap does not take into account dynamically loaded columns
        cols = $('#column-right, #column-left').length;

        if (cols == 2) {
            $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
        } else if (cols == 1) {
            $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
        } else {
            $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-3 col-xs-12');
        }

        localStorage.setItem('display', 'grid');
    });

    if (localStorage.getItem('display') == 'list') {
        $('#list-view').trigger('click');
    } else {
        $('#grid-view').trigger('click');
    }

    // tooltips on hover
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

    // Makes tooltips work on ajax generated content
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
    });
});

// Cart add remove functions
var cart = {
    'add': function (product_id, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {

                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {


                    $('#content').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart > #cart-total ').html(json['total']);
                    }, 100);

                    $('html, body').animate({scrollTop: 0}, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');

                    $('#shoppingcart > span').load('index.php?route=module/shoppingcart/info span');

                    //window.yaCounter41685499.reachGoal('AddToCart');

                }

            }
        });
    },
    'update': function (key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'product_id=' + key + '&quantity[' + key + ']=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            success: function (json) {
                //document.location.href='/index.php?route=checkout/checkout';
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > #cart-total').html(json['total']);

                }, 1);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/checkout';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');


                    $('#shoppingcart > span').load('index.php?route=module/shoppingcart/info span');

                }
            }
        });
    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart  > #cart-total').html(json['total']);
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/checkout';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');

                    $('#shoppingcart > span').load('index.php?route=module/shoppingcart/info span');

                }
            }
        });
    }
}

var voucher = {
    'add': function () {

    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');


                }
            }
        });
    }
}

var wishlist = {
    'add': function (product_id, container) {
        container = (container) ? container : '#content';
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();

                if (json['success']) {
                    $(container).prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                if (json['info']) {
                    $(container).prepend('<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' + json['info'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);
                if (!container) {
                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            }
        });
    },
    'remove': function () {

    }
}

var compare = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            }
        });
    },
    'remove': function () {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function (e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function (data) {
            html = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});

// Autocomplete */
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function () {
                this.request();
            });

            // Blur
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function (event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }

            // Show
            this.show = function () {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu').show();
            }

            // Hide
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }

            // Request
            this.request = function () {
                clearTimeout(this.timer);

                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function (json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            }

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

        });
    };

    // $(document).on('change', 'input[name=\'account\']', function() {
    //     if ($('#collapse-payment-address').parent().find('.panel-heading .panel-title > *').is('a')) {
    //         if (this.value == 'register') {
    //             $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');
    //         }
    //     }
    // });


    // CHECKOUT START!!!

    $(document).on('submit', '#liqpay', function () {
        //window.yaCounter41685499.reachGoal('LiqPayPayment');
    });


    $(document).delegate('#button-go-login', 'click', function () {
        $.ajax({
            url: 'index.php?route=checkout/login',
            dataType: 'html',
            success: function (html) {
                switchPanel('#collapse-checkout-option', html)
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

// Checkout
    $(document).delegate('#button-account', 'click', function () {
        $.ajax({
            url: 'index.php?route=checkout/register',
            dataType: 'html',
            beforeSend: function () {
                $('#button-account').button('reset');
            },
            complete: function () {
                $('#button-account').button('reset');
            },
            success: function (html) {
                $('.alert, .text-danger').remove();

                switchPanel('#collapse-payment-method', html)
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

// Login
    $(document).delegate('#button-login', 'click', function () {
        $.ajax({
            url: 'index.php?route=checkout/login/save',
            type: 'post',
            data: $('#collapse-checkout-option :input'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-login').button('reset');
            },
            complete: function () {
                $('#button-login').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    $('#collapse-checkout-option .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    // Highlight any found errors
                    $('input[name=\'email\']').parent().addClass('has-error');
                    $('input[name=\'password\']').parent().addClass('has-error');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

// Register
    $(document).delegate('#button-register', 'click', function () {
        $.ajax({
            url: 'index.php?route=checkout/register/save',
            type: 'post',
            data: $('#collapse-checkout-option input[type=\'text\'], #collapse-checkout-option input[type=\'date\'], #collapse-checkout-option input[type=\'datetime-local\'], #collapse-checkout-option input[type=\'time\'], #collapse-checkout-option input[type=\'password\'], #collapse-checkout-option input[type=\'hidden\'], #collapse-checkout-option input[type=\'checkbox\']:checked, #collapse-checkout-option input[type=\'radio\']:checked, #collapse-checkout-option textarea, #collapse-checkout-option select'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-register').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    $('#button-register').button('reset');

                    if (json['error']['warning']) {
                        $('#collapse-checkout-option .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }

                    for (i in json['error']) {
                        var element = $('#input-payment-' + i.replace('_', '-'));

                        if ($(element).parent().hasClass('input-group')) {
                            $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                        } else {
                            $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                        }
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                } else {

                    var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');

                    $.ajax({
                        url: 'index.php?route=checkout/shipping_method',
                        dataType: 'html',
                        success: function (html) {

                            switchPanel('#collapse-shipping-method', html);
                            removeLink($('#collapse-shipping-address').parent().find('.panel-heading .panel-title'));
                            removeLink($('#collapse-payment-method').parent().find('.panel-heading .panel-title'));
                            removeLink($('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title'))

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });


    $(document).delegate('#button-shipping-address', 'click', function () {
        addressSave();
        addressSave('shipping');
    });


    // Guest Shipping
    $(document).delegate('#button-guest-shipping', 'click', function () {
        $.ajax({
            url: 'index.php?route=checkout/guest_shipping/save',
            type: 'post',
            data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-guest-shipping').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    $('#button-guest-shipping').button('reset');

                    if (json['error']['warning']) {
                        $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }

                    for (i in json['error']) {
                        var element = $('#input-shipping-' + i.replace('_', '-'));

                        if ($(element).parent().hasClass('input-group')) {
                            $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                        } else {
                            $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                        }
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                } else {
                    $.ajax({
                        url: 'index.php?route=checkout/shipping_method',
                        dataType: 'html',
                        complete: function () {
                            $('#button-guest-shipping').button('reset');
                        },
                        success: function (html) {

                            switchPanel('#collapse-shipping-method', html);
                            removeLink($('#collapse-shipping-address').parent().find('.panel-heading .panel-title'));
                            removeLink($('#collapse-payment-method').parent().find('.panel-heading .panel-title'));
                            removeLink($('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title'));
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $(document).delegate('#button-shipping-method', 'click', function () {

        var data = $('#collapse-shipping-method input[type=\'radio\']:checked');

        if (data.val()) {
            $.ajax({
                url: 'index.php?route=checkout/shipping_method/save',
                type: 'post',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#button-shipping-method').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-shipping-method').button('reset');

                        if (json['error']['warning']) {
                            $('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                    } else {
                        openShippingAddress();
                    }

                    ga('ec:setAction', 'checkout_option', {
                        'step': 8,
                        'option': data.val()
                    });

                    ga('send', 'event', 'Checkout', 'Option');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });


        }else{
            openShippingAddress();
        }

    });

    $(document).delegate('#button-payment-method', 'click', function () {
        $.ajax({
            url: 'index.php?route=checkout/payment_method/save',
            type: 'post',
            data: $('#collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
            dataType: 'json',
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    $('#button-payment-method').button('reset');

                    if (json['error']['warning']) {
                        $('#collapse-payment-method .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }
                } else {


                    $.ajax({
                        url: 'index.php?route=checkout/confirm',
                        dataType: 'html',
                        success: function (html) {

                            openPanel($('#collapse-checkout-confirm').closest('.panel'));
                            switchPanel('#collapse-checkout-confirm', html);


                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    /* payments method */

    $(document).on('click', '#collapse-payment-method input', function () {
        $('#collapse-payment-method').find('.active').removeClass('active');
        $(this).closest('tr').find('img').addClass('active');
    });

    /* payments method */

    $(document).delegate('#button-save-guest', 'click', function (e) {
        e.preventDefault();
        $('.text-danger').remove();

        $.ajax({
            url: 'index.php?route=checkout/login/saveGuestData',
            type: 'POST',
            data: {
                "email": $('.info-log #input-checkout-email').val(),
                "firstname": $('.info-log #input-checkout-firstname').val(),
                "lastname": $('.info-log #input-checkout-lastname').val(),
                "telephone": $('.info-log #input-checkout-telephone').val()
            },
            success: function (json) {

                if (json['error']) {

                    if (json['error']['warning']) {
                        $('#collapse-checkout-option .panel-body').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }

                    for (i in json['error']) {
                        var element = $('#input-checkout-' + i.replace('_', '-'));

                        if ($(element).parent().hasClass('input-group')) {
                            $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                        } else {
                            $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                        }
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                } else if (json['response'] == 'success') {
                    $.ajax({
                        url: 'index.php?route=checkout/shipping_method',
                        dataType: 'html',
                        success: function (html) {

                            $('.go-back-checkout').show();
                            switchPanel('#collapse-shipping-method', html);
                            openPanel($('#collapse-shipping-method').closest('.panel'));

                            removeLink($('#collapse-shipping-address').parent().find('.panel-heading .panel-title'));
                            removeLink($('#collapse-payment-method').parent().find('.panel-heading .panel-title'));
                            removeLink($('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title'))
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }

        });

    });

    $(document).on('click','.go-back-checkout',function () {
        openPanel("prev");
    })


})(window.jQuery);

function openPanel(panel) {
    var open = $(".open");

    if(panel == "prev"){
        var prev = open.prev();

        if(prev.hasClass('panel')) {
            open.removeClass("open");

            prev.addClass("open")
                .find('.panel-heading a')
                .trigger('click');
        }

    }else if (panel) {
        open.removeClass("open");
        panel.addClass("open");
    } else {
        open.removeClass("open");
        open.next()
            .addClass("open");
    }
}

function addressSave(type) {

    var url;

    if (type == 'shipping') {
        url = 'index.php?route=checkout/shipping_address/save';
    } else {
        url = 'index.php?route=checkout/payment_address/save';
    }

    $.ajax({
        url: url,
        type: 'post',
        data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
        dataType: 'json',
        beforeSend: function () {
            $('#button-shipping-address').button('reset');
        },
        success: function (json) {
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                $('#button-shipping-address').button('reset');

                if (json['error']['warning']) {
                    $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                for (i in json['error']) {
                    var element = $('#input-shipping-' + i.replace('_', '-'));

                    if ($(element).parent().hasClass('input-group')) {
                        $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                    } else {
                        $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                    }
                }

                // Highlight any found errors
                $('.text-danger').parent().parent().addClass('has-error');

            } else {
                if (type == 'shipping') {
                    paymentPanel();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

}

function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

function paymentPanel() {
    var currentAddress = '';

    $.each($('#collapse-shipping-address input,#collapse-shipping-address select'), function () {
        currentAddress += $(this).val() != '' ? $(this).val() + ', ' : '';
    });

    $.ajax({
        url: 'index.php?route=checkout/payment_method',
        dataType: 'html',
        complete: function () {
            $('#button-shipping-method').button('reset');
        },
        success: function (html) {

            openPanel($('#collapse-payment-method').closest('.panel'));
            switchPanel('#collapse-payment-method', html);

            var container = $('#collapse-shipping-address ').parent();

            container.find('.panel-heading p').remove();
            container.find('.panel-heading').append('<p>' + currentAddress.slice(0, -2) + '</p>');

            removeLink($('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title'));

            //window.yaCounter41685499.reachGoal('PaymentsMethodPage');
            ga('send', {
                hitType: 'event',
                eventCategory: 'quick-order',
                eventAction: 'checkout',
                eventLabel: 'fourth-payment'
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function switchPanel(contentId, content) {
    var panelContent = $(contentId);
    var panelHeading = panelContent.parent().find('.panel-heading .panel-title');

    panelContent.find('.panel-body').html(content);
    panelHeading.html('<a href="' + contentId + '" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">' +
    panelHeading.html() + '<i class="fa fa-caret-down"></i></a>');
    panelHeading.find('a').trigger('click');
}

function showSmartCheckout() {
    var checkoutModal = $('#smart-checkout');

    checkoutModal.modal('show');
    $.get('index.php?route=checkout/checkout&smart=true')
        .done(function (response) {
            checkoutModal.find('.modal-content')
                .html(response);
        });
}

function removeLink(container) {
    var text = container.find('a').html();

    container.html(text);
}


function openShippingAddress() {
    $.ajax({
        url: 'index.php?route=checkout/shipping_address',
        dataType: 'html',
        success: function (html) {
            var panelHeading = $('#collapse-shipping-method').parent().find(".panel-heading");
            var checkedMethod = $('#collapse-shipping-method input[type=\'radio\']:checked');

            panelHeading.html('<h3 class="panel-title">' +
                '<a href="#collapse-shipping-method" data-toggle="collapse" ' +
                'data-parent="#accordion" class="accordion-toggle">' +
                panelHeading.html() + '<i class="fa fa-caret-down"></i></a></h3>');


            if(checkedMethod.val()){
                panelHeading.find('table').remove();
                panelHeading.append('<table style="width: 100%"><tr class="delivery-list">' +
                    checkedMethod.parent().parent().html() + '</tr></table>');
                panelHeading.find('input[type=\'radio\']')
                    .removeAttr('id')
                    .prop('checked', true);
            }


            openPanel($('#collapse-shipping-address').closest('.panel'));
            switchPanel('#collapse-shipping-address', html);
            removeLink($('#collapse-payment-method').parent().find('.panel-heading .panel-title'));
            removeLink($('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title'));

            ga('send', {
                hitType: 'event',
                eventCategory: 'quick-order',
                eventAction: 'checkout',
                eventLabel: 'third-shipment'
            });

           // window.yaCounter41685499.reachGoal('AddAddressPage');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}