<?php
class ControllerCommonLanguage extends Controller {
	public function index() {
		$this->load->language('common/language');

		$data['text_language'] = $this->language->get('text_language');

		$data['action'] = $this->url->link('common/language/language', '', $this->request->server['HTTPS']);

		$data['code'] = $this->session->data['language'];

		$this->load->model('localisation/language');

		$data['languages'] = array();

		$results = $this->model_localisation_language->getLanguages();

		foreach ($results as $result) {
			if ($result['status']) {
				$data['languages'][] = array(
					'name' => $result['name'],
					'code' => $result['code']
				);
			}
		}

		if (!isset($this->request->get['route'])) {
			$data['redirect'] = HTTP_SERVER;
		} else {
			$url_data = $this->request->get;

			$route = $url_data['route'];

			unset($url_data['route']);

			$url = '';

			if ($url_data) {
				$url = '&' . urldecode(http_build_query($url_data, '', '&'));
			}

			$data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
		}

		return $this->load->view('common/language', $data);
	}

	public function language() {
		if (isset($this->request->post['code'])) {
			$this->session->data['language'] = $this->request->post['code'];
		}

        $lang = explode('-',$this->session->data['language']);

		if (isset($this->request->post['redirect'])) {
            $redirectUrl = explode('?',$this->request->post['redirect']);

            $reguestUri='';
            if(isset($redirectUrl[1]))
		        $reguestUri = str_replace('&amp;', '&', $redirectUrl[1]);

		    parse_str ($reguestUri,$redirectParts);
            if(isset($redirectParts['_route_'])) {
                $redirect = ($lang[0] != 'ua') ? "/$lang[0]/$redirectParts[_route_]" : "/$redirectParts[_route_]";
            }else{
                $redirect =  $this->request->post['redirect'] ;
            }

			$this->response->redirect($redirect);
		} else {
            $redirect = ($lang[0]!='ua') ? "/$lang[0]" : HTTP_SERVER;
			$this->response->redirect($redirect);
		}
	}
}