<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_shops'] = $this->language->get('text_shops');
		$data['text_shop'] = $this->language->get('text_shop');
		$data['text_addresses'] = $this->language->get('text_addresses');
		$data['text_partners'] = $this->language->get('text_partners');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_blog'] = $this->language->get('text_blog');
		$data['text_history'] = $this->language->get('text_history');
		$data['text_lookbook'] = $this->language->get('text_lookbook');
		$data['text_brand'] = $this->language->get('text_brand');
		$data['text_worktime'] = $this->language->get('text_worktime');
		$data['text_time'] = $this->language->get('text_time');
		$data['text_days'] = $this->language->get('text_days');
		$data['text_subscribe'] = $this->language->get('text_subscribe');
		$data['text_subscribe_button'] = $this->language->get('text_subscribe_button');
		$data['text_subscribe_placeholder'] = $this->language->get('text_subscribe_placeholder');
		$data['text_payments_methods'] = $this->language->get('text_payments_methods');

		$this->load->model('catalog/category');
		$data['categories'] = array();

		// Pick your country part
		$data['country_page'] = false;
		if(!isset($this->request->cookie['customer_country'])){
			//$data = array();
			$data['countrySaveLink'] = $this->url->link('tool/country/save');
			$data['country_page'] = $this->load->view('tool/country', $data);
			setcookie('customer_country', 'Ukraine' ,time()+3600*24*360);

		}elseif(!isset($this->session->data['customer_country'])){
			$this->session->data['customer_country'] = $this->request->cookie['customer_country'];
		}

		$categories = $this->model_catalog_category->getCategories(0);
		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->preventLinkCycling('product/category&path=' . $category['category_id'])
				);
			}
		}

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->preventLinkCycling('information/information&information_id=' . $result['information_id'])
				);
			}
		}

		//TODO REFACTOR THIS
		$data['be_partner'] = $this->preventLinkCycling('information/information&information_id=9');
		$data['lookbook'] = $this->preventLinkCycling('common/lookbook');
		$data['contact'] = (isset($this->request->get['route']) && ($this->request->get['route']=='contacts' || $this->request->get['route']=='information/contact')) ?  '#' : $this->url->link('contacts') ;
		$data['return'] = $this->preventLinkCycling('account/return/add');
		$data['sitemap'] = $this->preventLinkCycling('information/sitemap');
		$data['shops'] = $this->preventLinkCycling('information/information&information_id=7');
		$data['manufacturer'] = $this->preventLinkCycling('product/manufacturer');
		$data['voucher'] = $this->preventLinkCycling('account/voucher');
		$data['affiliate'] = $this->preventLinkCycling('affiliate/account');
		$data['special'] = $this->preventLinkCycling('product/special');
		$data['account'] = $this->preventLinkCycling('account/account');
		$data['order'] = $this->preventLinkCycling('account/order');
		$data['wishlist'] = $this->preventLinkCycling('account/wishlist');
		$data['newsletter'] = $this->preventLinkCycling('account/newsletter');
		$data['blog'] = $this->preventLinkCycling('common/blog');
		$data['history'] = $this->preventLinkCycling('tltblog/tltblog&tltpath=blogs&tltblog_id=29');

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = '//' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		return $this->load->view('common/footer', $data);
	}

	public function subscribe() {
		$this->load->model('account/customer');

		$json = array();

		if(isset($this->request->post['email'])){
			$json['response'] = $this->model_account_customer->enableNewsletter($this->request->post['email']);
		}else{
			$json['error'] = "Enter you'r email!";
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    protected function preventLinkCycling($route){
        $link = $this->url->link($route);
        if (isset($this->request->get['route'])) {
            $currentPage = $this->request->get['route'];

            if (isset($this->request->get['product_id'])) {
                $currentPage .= '&product_id=' . $this->request->get['product_id'];
            } elseif (isset($this->request->get['path']) and $this->request->get['path'] != 0) {
                $currentPage .= '&path=' . $this->request->get['path'];
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $currentPage .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            } elseif (isset($this->request->get['information_id'])) {
                $currentPage .= '&information_id=' . $this->request->get['information_id'];
            } elseif (isset($this->request->get['_route_'])) {
                $lang = substr($this->session->data['language'],0,2);
                $currentPage = HTTP_SERVER;
                $currentPage .= ($lang != 'ua') ? $lang.'/' : '';
                $currentPage .= $this->request->get['_route_'];
            } else {
                $currentPage .= '';
            }

            $currentPage = str_replace('=0_','=',$currentPage);

            if ($currentPage == $route  or $this->request->get['route'] == $route or $currentPage == $link)
                $link = '#';
        }

        return $link;
    }
}
