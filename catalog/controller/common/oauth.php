<?php

include_once "/oauth/OAuthManager.php";
include_once "/oauth/provider/User.php";
include_once "/oauth/AbstractUser.php";


/**
 * Created by PhpStorm.
 * User: dgmax
 * Date: 04.07.2016
 * Time: 10:58
 */
class Controllercommonoauth extends Controller
{
    protected $oauthManager;

    /**
     * ControllerOAuth constructor.
     * @param $oauthManager
     */
    public function __construct($registry)
    {
        parent::__construct( $registry );
        $this->oauthManager = new OAuthManager();
    }


    public function redirectToVk()
    {
        $test = $this->oauthManager->getVkDriver()->redirect();

        $this->response->setOutput($test);
    }

    public function handleVkCallback()
    {
        $user = $this->oauthManager->getVkDriver()->user();
        $user->getId();

        $this->load->model('account/customer');

        $existUser  = @$this->model_account_customer->getCustomerBySocialId($user->getId());

        if (count($existUser) === 0) {
            $this->register($user);
        }

        // Add to activity log
        $this->load->model('account/activity');

        $activity_data = array(
            'customer_id' => $existUser['customer_id'],
            'name'        => $existUser['firstname'].' '.$existUser['lastname']
        );

        $this->model_account_activity->addActivity('login', $activity_data);
        $productController = $this->load->model('account/customer');
        $productController->login();
    }

    public function register($user)
    {
        $data = [
            'firstname' => $user->getFirstname(),
            'lastname' => $user->getLastname(),
            'type_provider' => 'vk',
            'social_id' => $user->getId()
        ];
        $this->fillForm($data);
    }

    protected function fillForm(&$data)
    {

        if (!isset($this->data['email']))
            $data['email'] = '';


        if (!isset($data['telephone']))
            $data['telephone'] = '';


        if (!isset($data['fax']))
            $data['fax'] = '';


        if (!isset($this->request->post['company']))
            $data['company'] = '';


        if (!isset($data['address_1']))
            $data['address_1'] = '';


        if (!isset($data['address_2']))
            $data['address_2'] = '';


        if (!isset($data['postcode'])) {
            $data['postcode'] = '';
        }

        if (!isset($data['city'])) {
            $data['city'] = '';

            if (isset($_SESSION['shipping_address']['country_id'])) {
                $data['country_id'] = $this->session->data['shipping_address']['country_id'];
            } else {
                $data['country_id'] = $this->config->get('config_country_id');
            }

            if (isset($_SESSION['shipping_address']['zone_id'])) {
                $data['zone_id'] = $this->session->data['shipping_address']['zone_id'];
            } else {
                $data['zone_id'] = '';
            }

            $this->load->model('localisation/country');

            $data['countries'] = $this->model_localisation_country->getCountries();

            // Custom Fields
            $this->load->model('account/custom_field');

            $data['custom_fields'] = $this->model_account_custom_field->getCustomFields();

            $data['register_custom_field'] = array();

            $data['password'] = '';


            $data['confirm'] = '';


            $data['newsletter'] = '';

            $data['captcha'] = '';


            if ($this->config->get('config_account_id')) {
                $this->load->model('catalog/information');

                $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

                if ($information_info) {
                    $data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), true), $information_info['title'], $information_info['title']);
                } else {
                    $data['text_agree'] = '';
                }
            } else {
                $data['text_agree'] = '';
            }
        }
    }
}
