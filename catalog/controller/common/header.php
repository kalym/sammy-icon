<?php
class ControllerCommonHeader extends Controller {
	public function index() {

		if (isset($this->request->get['lang'])) {

			$langCodes = array(
				'en'=>'en-gb',
				'ru'=>'ru-ru',
				'ua'=>'ua-uk',
			);

			$this->session->data['language'] = (!empty($langCodes[$this->request->get['lang']])) ? $langCodes[$this->request->get['lang']] : $this->request->get['lang'];
			$this->response->redirect('//'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);

		}


        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();
		$data['base'] = ($server)?$server:'https://www.sammy-icon.com/';
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');
		$data['name'] = $this->config->get('config_name');
        $data['home_link'] = $this->url->link('common/home');

		if ( is_file(DIR_IMAGE . $this->config->get('config_logo')) ){

            if( !isset($this->request->get['route']) || (isset($this->request->get['route']) and $this->request->get['route'] == 'common/home'))
                $data['home_link'] = '#';

            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		$data['text_home'] = $this->language->get('text_home');
		$data['text_worktime'] = $this->language->get('text_worktime');
		$data['text_works'] = $this->getTextWorks();

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		if((isset($this->request->cookie['customer_country']) and $this->request->cookie['customer_country']!='Ukraine')){
			$this->session->data['currency'] = 'USD';
		}elseif(isset($this->request->cookie['customer_country']) and $this->request->cookie['customer_country']=='Ukraine'){
			$this->session->data['currency'] = 'UAH';
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));
		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');
		$data['text_beta'] = $this->language->get('text_beta');

		// all page
		$data['text_holiday_deliver'] = $this->language->get('text_holiday_deliver');
		
		// Login Form Text
		$data['text_enter'] = $this->language->get('text_enter');
		$data['text_forgotten'] = $this->language->get('text_forgotten');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_registration'] = $this->language->get('text_registration');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_signup'] = $this->language->get('text_signup');
		$data['text_login_with'] = $this->language->get('text_login_with');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');
		$data['telephone'] = $this->config->get('config_telephone');
		$data['url_forgotten'] = $this->url->link('account/forgotten');
        $data['ros'] = isset($_SERVER['REQUEST_URI']) ? '/'.$_SERVER['REQUEST_URI'] : '';

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->preventLinkCycling('product/category&path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->preventLinkCycling('product/category&path=' . $category['category_id'])
				);
			}
		}

		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');
		$data['content_newpos'] =  $this->load->controller('common/content_newpos');

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} elseif (isset($this->request->get['information_id'])) {
				$class = '-' . $this->request->get['information_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}


        $data['top_menu'] = $this->populateTopMenu();

		return $this->load->view('common/header', $data);
	}

    protected function populateTopMenu(){
        return array(
         //   'blog'         => $this->populateMenuItem($this->language->get('text_blog'),'common/blog'),
            'delivery'     => $this->populateMenuItem($this->language->get('text_delivery'),'information/information&information_id=6','#delivery'),
            'payment'      => $this->populateMenuItem($this->language->get('text_payments'),'information/information&information_id=6','#payments'),
            'about_us'     => array('title'  => $this->language->get('text_about_us'),'link' => (isset($this->request->get['route']) && ($this->request->get['route']=='contacts' || $this->request->get['route']=='information/contact')) ? '#' : $this->url->link('contacts') ),
        );
    }

    protected function populateMenuItem($title,  $route, $aditional = ''){
        return array(
            'title'  => $this->language->get($title),
            'link' => $this->url->link($route) . $aditional
        );
    }

	protected function getTextWorks(){
		$today = date('D');
		$text_not = ($today=='Sun' || $today=='Sat') ? $this->language->get('text_not') : '';

		if($this->session->data['language'] == 'en-gb'){
			return ($text_not!='')
                ? sprintf($this->language->get('text_not'),'/image/icon/fb_smile.png')
                : $this->language->get('text_works');
		}

		return sprintf($this->language->get('text_works'),$text_not);
	}

	protected function preventLinkCycling($route){
        $link = $this->url->link($route);
        if (isset($this->request->get['route'])) {
            $currentPage = $this->request->get['route'];

            if (isset($this->request->get['product_id'])) {
                $currentPage .= '&product_id=' . $this->request->get['product_id'];
            } elseif (isset($this->request->get['path']) and $this->request->get['path'] != 0) {
                $currentPage .= '&path=' . $this->request->get['path'];
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $currentPage .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            } elseif (isset($this->request->get['information_id'])) {
                $currentPage .= '&information_id=' . $this->request->get['information_id'];
            } elseif (isset($this->request->get['_route_'])) {
                $lang = substr($this->session->data['language'],0,2);
                $currentPage = HTTP_SERVER;
                $currentPage .= ($lang != 'ua') ? $lang.'/' : '';
                $currentPage .= $this->request->get['_route_'];
            } else {
                $currentPage .= '';
            }

            $currentPage = str_replace('=0_','=',$currentPage);

            if ($currentPage == $route  or $this->request->get['route'] == $route or $currentPage == $link)
                  $link = '#';
        }

        return $link;
    }
}
