<?php
class ControllerCheckoutSuccess extends Controller
{
    public function index()
    {
        $this->load->language('checkout/success');

        if (isset($this->session->data['order_id'])) {
            $this->cart->clear();

            // Add to activity log
            $this->load->model('account/activity');

            if ($this->customer->isLogged()) {
                $activity_data = array(
                    'customer_id' => $this->customer->getId(),
                    'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
                    'order_id' => $this->session->data['order_id']
                );

                $this->model_account_activity->addActivity('order_account', $activity_data);
            } else {
                $activity_data = array(
                    'name' => $this->session->data['guest']['firstname'] . ' '
                        . $this->session->data['guest']['lastname'],
                    'order_id' => $this->session->data['order_id']
                );

                $this->model_account_activity->addActivity('order_guest', $activity_data);
            }

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['guest']);
            unset($this->session->data['comment']);
            unset($this->session->data['coupon']);
            unset($this->session->data['reward']);
            unset($this->session->data['voucher']);
            unset($this->session->data['vouchers']);


        }

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_basket'),
            'href' => $this->url->link('checkout/cart')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_checkout'),
            'href' => $this->url->link('checkout/checkout', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_success'),
            'href' => $this->url->link('checkout/success')
        );

        $data['heading_title'] = $this->language->get('heading_title');

        if ($this->customer->isLogged() && isset($this->session->data['order_id'])) {
            $data['text_message'] = sprintf($this->language->get('text_customer'), $this->session->data['order_id'], $this->session->data['totals']['text']);


            if (isset($this->session->data['order_id'])) {
                unset($this->session->data['order_id']);
                unset($this->session->data['totals']);
            }

        }
        else {
            $data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
        }

        $conveadOrderId = $this->session->data['order_id'];
        $conveadTotals = $this->session->data['totals']['text'];
        $shipping = isset($this->session->data['shipping']['text']) ? $this->session->data['shipping']['text'] : 0;
        $this->load->model('account/order');
        $products = $this->model_account_order->getOrderProducts($conveadOrderId);
        $conveadUserFirstName = $this->session->data['payment_address']['firstname'];
        $conveadUserLastName = $this->session->data['payment_address']['lastname'];
        $conveadUserEmail = $this->session->data['payment_address']['email'];
        $conveadUserTelephone = $this->session->data['payment_address']['telephone'];


        $data['scripts'] .= " <!-- pixel -->
                <script>
                fbq('track', 'Purchase');
                
                <!-- custom analytics events -->
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'quick-order',
                    eventAction: 'checkout',
                    eventLabel: 'fifth-checkout'
                });
                
                <!-- convead -->
                convead('event', 'purchase', {
                    order_id: '$conveadOrderId',
                    revenue: '$conveadTotals',
                    items: [";
                        foreach ($products as $product) {
                            $data['scripts'] .= '{ "product_id":"' . $product['product_id'] .
                                '", "qnt":"' . $product['quantity'] .
                                '", "price":"' . $product['price'] . '"},';
                        }
        $data['scripts'] .= "]
                }, {
                    first_name: '$conveadUserFirstName',
                    last_name: '$conveadUserLastName',
                    email: '$conveadUserEmail',
                    phone: '$conveadUserTelephone'
                 }); </script>";

        $data['scripts'] .= "<!-- ecommerce -->
                 <script>
                    ga('require', 'ecommerce');
                    ";

        $data['scripts'] .= $this->getTransactionJs($conveadOrderId, $conveadTotals,$shipping);

        foreach ($products as &$item) {
            $data['scripts'] .= $this->getItemJs($conveadOrderId, $item);
        }

        $data['scripts'] .= "ga('ecommerce:send'); ga('ecommerce:clear');
                </script>";
        $data['button_continue'] = $this->language->get('button_continue');

        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('common/success', $data));
    }


    // Function to return the JavaScript representation of a TransactionData object.
    private function getTransactionJs(&$id, $revenue, $shipping)
    {
        return "
        ga('ecommerce:addTransaction', {
          'id': '{$id}',
          'affiliation': 'Sammy Icon',   
          'revenue': '{$revenue}',
        });
        ";
    }

    // Function to return the JavaScript representation of an ItemData object.
    private function getItemJs(&$conveadOrderId, &$item)
    {
        return "
            ga('ecommerce:addItem', {
              'id': '$conveadOrderId',
              'name': '{$item['name']}',
              'sku': '{$item['product_id']}',
              'category': '{$item['model']}',
              'price': '{$item['price']}',
              'quantity': '{$item['quantity']}',
              'currency':'UAH'
            }); 
        ";

    }
}
