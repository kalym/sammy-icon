<?php
class ControllerCheckoutShippingAddress extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');

		$data['text_address_existing'] = $this->language->get('text_address_existing');
		$data['text_address_new'] = $this->language->get('text_address_new');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_flat'] = $this->language->get('entry_flat');
		$data['entry_comment'] = $this->language->get('entry_comment');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_warehouse'] = $this->language->get('entry_warehouse');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_zone'] = $this->language->get('entry_zone');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->session->data['shipping_address']['address_id'])) {
			$data['address_id'] = $this->session->data['shipping_address']['address_id'];
		} else {
			$data['address_id'] = $this->customer->getAddressId();
		}

		if(isset($this->session->data['shipping_method']) and $this->session->data['shipping_method']['code'] == 'inpost.inpost'){
			$poshtomat="KIE590, пр-т. Л.Курбаса, 6Г|KIE583, вул. Вербицького, 18|KIE675, Харьківське ш., 168|KIE589, пр-т. А. Глушкова, 36|KIE660, пр-т. Л.Курбаса, 15А|KIE594, п-т. Перемоги, 136|KIE662, пр-т. Палладіна, 7|KIE585, вул. Фізкультури, 1|KIE592, пл. Печерська, 1|KIE593, Вокзальна площа, 2|KIE595, вул. Пимоненка, 13|KIE676, вул. В.Васильківська, 136|KIE671, вул. В.Липківського, 45|KIE596, Єреванська, 14|KIE658, вул. Дорогожицька, 2|KIE661, пр-т. Броварський, 17|KIE578, пр-т Науки, 8|KIE663, вул. М. Раскової, 2Б|KIE586, вул. Борщагівська, 154|KIE584, пр-т Повітрофлотський, 56А|KIE664, Оболонська площа, 4А|KIE579, вул. O.Теліги, 15|KIE657, вул. Стальського, 22/10|KIE580, п-т. Оболонський, 21|KIE587, вул. Червоногвардійська, 1B|KIE668, вул. Героїв Дніпра, 34|KIE669, вул. Драйзера, 8|KIE672, пр-т. Григоренка, 18|KIE670, вул. Маршала Конєва, 7|KIE665, пр-т. Маяковського, 43/2|KIE582, вул. Дніпровська набережна, 33|KIE666, пр-т. Правди, 66|KIE581, вул. Лаврухіна, 4|KIE673, Харківське ш., 144Б|KIE591, вул. Тростянецька, 1|KIE659, вул. Гришка, 3";
			$data['inpost'] = explode('|',$poshtomat);
		}elseif((isset($this->session->data['shipping_method']) and $this->session->data['shipping_method']['code'] == 'world.world')){
			$data['worldwide'] = true;
		}elseif((isset($this->session->data['shipping_method']) and $this->session->data['shipping_method']['code'] == 'pickup.pickup')){
			$data['pickup'] = true;
		}elseif((isset($this->session->data['shipping_method']) and $this->session->data['shipping_method']['code'] == 'pickup_two.pickup_two')){
            $data['pickup'] = true;
        }
        elseif((isset($this->session->data['shipping_method']) and $this->session->data['shipping_method']['code'] == 'novaposhta.novaposhta')){
            $data['novaposhta'] = true;
        }

		$this->load->model('account/address');

		$data['addresses'] = $this->model_account_address->getAddresses();

		if (isset($this->session->data['shipping_address']['postcode'])) {
			$data['postcode'] = $this->session->data['shipping_address']['postcode'];
		} else {
			$data['postcode'] = '';
		}

        if(isset($this->session->data['customer']['telephone'])){
            $data['telephone'] = $this->session->data['customer']['telephone'];
        }else{
            $data['telephone'] = $this->session->data['language'] == 'en-gb' ? '' : '+380';
        }

		if (isset($this->session->data['shipping_address']['country_id'])) {
			$data['country_id'] = $this->session->data['shipping_address']['country_id'];
		} else {
			$data['country_id'] = $this->config->get('config_country_id');
		}

		if (isset($this->session->data['shipping_address']['zone_id'])) {
			$data['zone_id'] = $this->session->data['shipping_address']['zone_id'];
		} else {
			$data['zone_id'] = '';
		}

		$this->load->model('localisation/country');

		if($this->customer->isLogged()) {
            $data['firstname'] = $this->customer->getFirstName();
//            $data['lastname'] = $this->customer->getLastName();
            $data['email'] = $this->customer->getEmail();
            $data['telephone'] = $this->customer->getTelephone();
        }elseif (isset($this->session->data['customer'])){
            $this->session->data['guest'] = 1;
            $data['firstname'] = $this->session->data['customer']['firstName'];
            $data['telephone'] = $this->session->data['customer']['telephone'];
        }else{
            $this->session->data['guest'] = 1;
        }

		$data['countries'] = $this->model_localisation_country->getCountries();

		// Custom Fields
		$this->load->model('account/custom_field');

		$data['custom_fields'] = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

		if (isset($this->session->data['shipping_address']['custom_field'])) {
			$data['shipping_address_custom_field'] = $this->session->data['shipping_address']['custom_field'];
		} else {
			$data['shipping_address_custom_field'] = array();
		}

		$this->response->setOutput($this->load->view('checkout/shipping_address', $data));
	}

	public function save() {
		$this->load->language('checkout/checkout');

		$json = array();

	    // Validate if shipping is required. If not the customer should not have reached this page.
		if (!$this->cart->hasShipping()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();



		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}

		if (!$json) {

				if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
					$json['error']['firstname'] = $this->language->get('error_firstname');
				}

				if ($this->session->data['shipping_method']['code'] != 'pickup.pickup' &&
				    $this->session->data['shipping_method']['code'] != 'pickup_two.pickup_two' &&
                    $this->session->data['shipping_method']['code'] != 'novaposhta.novaposhta' &&
                    ((utf8_strlen(trim($this->request->post['address_1'])) < 3) ||
                        (utf8_strlen(trim($this->request->post['address_1'])) > 128))  ) {
					$json['error']['address_1'] = $this->language->get('error_address_1');
				}

                if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
                    $json['error']['telephone'] = $this->language->get('error_telephone');
                }

                if ($this->session->data['shipping_method']['code'] == 'novaposhta.novaposhta' &&
                    ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128))) {
                    $json['error']['city'] = $this->language->get('error_city');
                }
                if ($this->session->data['shipping_method']['code'] == 'novaposhta.novaposhta' &&
                    utf8_strlen(trim($this->request->post['warehouse'])) == '') {
                    $json['error']['warehouse'] = $this->language->get('error_warehouse');
                }


                if(empty($this->request->post['lastname'])){
					$this->request->post['lastname'] = $this->session->data['customer']['lastName'];
				}

				if(empty($this->request->post['city'])){
					$this->request->post['city'] = "";
				}

				if(empty($this->request->post['address_1'])){
					$this->request->post['address_1'] = "" . isset($this->request->post['warehouse']) ?
                        $this->language->get('entry_warehouse').': '.$this->request->post['warehouse'] : '';
				}

				if(empty($this->request->post['address_2'])){
					$this->request->post['address_2'] = "";
				}

				if(empty($this->request->post['address_3'])){
					$this->request->post['address_3'] = "";
				}

				if(empty($this->request->post['country_id'])){
					$this->request->post['country_id'] = "";
				}

				if(empty($this->request->post['postcode'])){
					$this->request->post['postcode'] = "";
				}

				if(empty($this->request->post['zone_id'])){
					$this->request->post['zone_id'] = "";
				}

				if(empty($this->request->post['company'])){
					$this->request->post['company'] = "";
				}

				if (isset($this->request->post['firstname']) ) {
					$data['firstname'] = $this->request->post['firstname'];
				} elseif (!empty($customer_info)) {
					$data['firstname'] = $customer_info['firstname'];
				}

				if (isset($this->request->post['lastname'])) {
					$data['lastname'] = $this->request->post['lastname'];
				} elseif (!empty($customer_info)) {
					$data['lastname'] = $customer_info['lastname'];
				} else {
					$data['lastname'] = '';
				}


				if (isset($this->request->post['telephone'])) {
					$data['telephone'] = $this->request->post['telephone'];
				} else {
                    $this->request->post['telephone'] = $this->session->data['customer']['telephone'];
                }


                if(!isset($this->request->post['email']))
                   $this->request->post['email'] = $this->session->data['customer']['email'];




				// Custom field validation
				$this->load->model('account/custom_field');

				$custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

				foreach ($custom_fields as $custom_field) {
					if (($custom_field['location'] == 'address') && $custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['custom_field_id']])) {
						$json['error']['custom_field' . $custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
					} elseif (($custom_field['type'] == 'text' && !empty($custom_field['validation']) && $custom_field['location'] == 'address') && !filter_var($this->request->post['custom_field'][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
                        $json['error']['custom_field' . $custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field_validate'), $custom_field['name']);
                    }
				}

				if (!$json) {

					if($this->customer->isLogged()){
						if(!$this->customer->getEmail()){
							$this->customer->setEmail($this->request->post['email']);
						}
						if(!$this->customer->getTelephone()){
							$this->customer->setTelephone($this->request->post['telephone']);
						}
					}

					// Default Shipping Address
					$this->load->model('account/address');
                    $this->load->model('account/customer');
//                    var_dump($this->request->post);

                    if($this->session->data['shipping_method']['code'] != 'pickup.pickup' or
                       $this->session->data['shipping_method']['code'] != 'pickup.pickup_two.pickup_two'){
                        if($this->customer->isLogged() and $this->model_account_address->getTotalAddresses($this->customer->getId())>0){
                            $address_id = $this->customer->getAddressId();
                            $activity = 'address_edit';
                        }else{
                            $address_id = $this->model_account_address->addAddress($this->request->post);
                            $activity = 'address_add';
                        }

                        $this->model_account_address->editAddress($address_id,$this->request->post);

                        $this->session->data['shipping_address'] = $this->model_account_address->getAddress($address_id);

                        $this->load->model('account/activity');

                        $this->addCustomerActivity($activity);
                    }

                    $this->session->data['comment'] = strip_tags($this->request->post['comment']);

				}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    private function addCustomerActivity($activity){
        $this->load->model('account/activity');

        $activity_data = array(
            'customer_id' => $this->customer->getId(),
            'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
        );

        $this->model_account_activity->addActivity($activity, $activity_data);
    }
}