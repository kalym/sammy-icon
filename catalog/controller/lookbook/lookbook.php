<?php

class ControllerLookbookLookbook extends Controller
{
    public function index()
    {
        $this->load->language('lookbook/lookbook');

        $this->load->model('lookbook/lookbook');
        $this->load->model('catalog/product');
        $this->load->model('setting/setting');
        $this->load->model('tool/image');

//        carousel
        $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
        $this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

        if ($this->config->get('lookbook_seo')) {
            require_once(DIR_APPLICATION . 'controller/lookbook/lookbook_seo.php');
            $lookbook_seo = new ControllerLookbookLookbookSeo($this->registry);
            $this->url->addRewrite($lookbook_seo);
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        if (isset($this->request->get['lookbookpath'])) {
            $path = $this->request->get['lookbookpath'];
        } elseif ($this->config->has('lookbook_path')) {
            $path = $this->config->get('lookbook_path');
        } else {
            $path = 'blogs';
        }

        $data['show_path'] = $this->config->get('lookbook_show_path');

        if ($data['show_path']) {
            if ($this->config->has('lookbook_path_title')) {
                $tmp_title = $this->config->get('lookbook_path_title');
                $root_title = $tmp_title[$this->config->get('config_language_id')]['path_title'];
            } else {
                $root_title = $this->language->get('text_title');
            }

            $data['breadcrumbs'][] = array(
                'text' => $root_title,
                'href' => $this->url->link('lookbook/lookbooktag', 'lookbookpath=' . $path)
            );
        }

        if (isset($this->request->get['lookbook_id'])) {
            $lookbook_id = (int)$this->request->get['lookbook_id'];
        } else {
            $lookbook_id = 0;
        }

        $lookbook_info = $this->model_lookbook_lookbook->getlookbook($lookbook_id);

        if ($lookbook_info) {
            $this->document->setTitle($lookbook_info['meta_title']);
            $this->document->setDescription($lookbook_info['meta_description']);
            $this->document->setKeywords($lookbook_info['meta_keyword']);
            $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

            $this->document->addLink($this->url->link('lookbook/lookbook', 'lookbookpath=' . $path . '&lookbook_id=' . $lookbook_id), 'canonical');

            $data['breadcrumbs'][] = array(
                'text' => $lookbook_info['title'],
                'href' => $this->url->link('lookbook/lookbook', 'lookbookpath=' . $path . '&lookbook_id=' . $lookbook_id)
            );

            $data['heading_title'] = $lookbook_info['title'];
            $data['show_title'] = $lookbook_info['show_title'];


            $data['blog_image'] = $this->model_tool_image->cropSize($lookbook_info['image'],1100, 400);



            $data['description'] = html_entity_decode($lookbook_info['description'], ENT_QUOTES, 'UTF-8');
            $data['article_pt1'] = html_entity_decode($lookbook_info['article_pt1'], ENT_QUOTES, 'UTF-8');
            $data['article_pt2'] = html_entity_decode($lookbook_info['article_pt2'], ENT_QUOTES, 'UTF-8');
            $data['quote1'] = html_entity_decode($lookbook_info['quote1'], ENT_QUOTES, 'UTF-8');
            $data['quote2'] = html_entity_decode($lookbook_info['quote2'], ENT_QUOTES, 'UTF-8');
            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['text_related'] = $this->language->get('text_related');
            $data['text_tags'] = $this->language->get('text_tags');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));



            $data['tags'] = array();

            $lookbook_tags = $this->model_lookbook_lookbook->getlookbooktagsForBlog($lookbook_id);

            foreach ($lookbook_tags as $lookbook_tag) {
                $data['tags'][] = array(
                    'title' => $lookbook_tag['title'],
                    'href' => $this->url->link('lookbook/lookbooktag', 'lookbookpath=' . $path . '&lookbooktag_id=' . $lookbook_tag['lookbooktag_id'])
                );
            }

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

             //$data['lookbooks'] = $this->populateRelatedArticles();
            //banner

            $this->load->model('design/banner');

            $data['banners'] = array();

            $results = $this->model_design_banner->getBanner($lookbook_info['banner_id']);

            foreach ($results as $result) {
                if (is_file(DIR_IMAGE . $result['image'])) {
                    $data['banners'][] = array(
                        'title' => $result['title'],
                        'link'  => $result['link'],
                        'image' => $this->model_tool_image->cropSize($result['image'], 1000, 400)
                    );
                }
            }


   // print_r($data);
            $this->response->setOutput($this->load->view('lookbook/lookbook', $data));

        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('lookbook/lookbook', 'lookbookpath=' . $path . '&lookbook_id=' . $lookbook_id)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');


            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    protected function populateRelatedArticles(){
        if ($this->config->has('lookbook_path')) {
            $lookbookpath = $this->config->get('lookbook_path');
        } else {
            $lookbookpath = 'blogs';
        }

        $lookbooks = array();



        $relatedArticles = $this->model_lookbook_lookbook->getlookbooksBottom();
        $rand_keys = array_rand($relatedArticles, 2);

        foreach ($rand_keys as $key) {
            $lookbooks[] = $this->getRelatedArticleEntry($relatedArticles[$key],$lookbookpath);
        }
        return $lookbooks;
    }


    protected function getRelatedArticleEntry($result, $lookbookpath){
        return array(
            'title' => $result['title'],
            'href' => $this->url->link('lookbook/lookbook', 'lookbookpath=' . $lookbookpath . '&lookbook_id=' . $result['lookbook_id']),
            'image' => $this->model_tool_image->cropSize($result['image'],320,200)
        );
    }

}