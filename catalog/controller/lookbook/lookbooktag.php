<?php
class ControllerLookbookLookbooktag extends Controller {
	public function index() {
		$this->load->language('lookbook/lookbooktag');
		
		$this->load->model('lookbook/lookbook');
		$this->load->model('setting/setting');
		$this->load->model('tool/image');

		if ($this->config->get('lookbook_seo')) {
			require_once(DIR_APPLICATION . 'controller/lookbook/lookbook_seo.php');
			$lookbook_seo = new ControllerlookbooklookbookSeo($this->registry);
			$this->url->addRewrite($lookbook_seo);
		}

		if ($this->config->has('lookbook_path_title')) {
			$tmp_title = $this->config->get('lookbook_path_title');
			$root_title = $tmp_title[$this->config->get('config_language_id')]['path_title'];
		} else {
			$root_title = $this->language->get('text_title');
		}

		if (isset($this->request->get['lookbooktag_id'])) {
			$lookbooks = $this->model_lookbook_lookbook->getlookbooksForTag($this->request->get['lookbooktag_id']);
			$lookbooktag = $this->model_lookbook_lookbook->getlookbooktag($this->request->get['lookbooktag_id']);
			$title = $lookbooktag['title'];
			$meta_title = $lookbooktag['meta_title'];
			$meta_description = $lookbooktag['meta_description'];
			$meta_keyword = $lookbooktag['meta_keyword'];
		} else {
			$lookbooks = $this->model_lookbook_lookbook->getlookbooks();
			
			$meta_title = $root_title;
			$title = $root_title;
			
			if ($this->config->has('config_meta_description_' . $this->session->data['language'])) {
				$meta_description = $title . ' ' . $this->config->get('config_meta_description_' . $this->session->data['language']);
				$meta_keyword = $title . ' ' . $this->config->get('config_meta_keyword_' . $this->session->data['language']);
			} else {
				$meta_description = $title . ' ' . $this->config->get('config_meta_description');
				$meta_keyword = $title . ' ' . $this->config->get('config_meta_keyword');
			}
		}

		if (isset($this->request->get['lookbookpath'])) {
			$path = $this->request->get['lookbookpath'];
		} elseif ($this->config->has('lookbook_path')) {
			$path = $this->config->get('lookbook_path');
		} else {
			$path = 'blogs';
		}
		
		$show_path = $this->config->get('lookbook_show_path');
		
		$data['show_path'] = $show_path;
		
		if ($this->config->has('lookbook_num_columns')) {
			$data['num_columns'] = $this->config->get('lookbook_num_columns');
		} else {
			$data['num_columns'] = '1';
		}
		
		if ($this->config->has('lookbook_show_image')) {
			$data['show_image'] = $this->config->get('lookbook_show_image');
		} else {
			$data['show_image'] = '1';
		}

		if ($data['show_image']) {
			if ($this->config->has('lookbook_width')) {
				$width = $this->config->get('lookbook_width');
			} else {
				$width = 200;
			}
			
			if ($this->config->has('lookbook_height')) {
				$height = $this->config->get('lookbook_height');
			} else {
				$height = 200;
			}
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if ($show_path) {
			$data['breadcrumbs'][] = array(
				'text' => $root_title,
				'href' => $this->url->link('lookbook/lookbooktag', 'lookbookpath=' . $path)
			);
		} 
		
		if ($lookbooks) {
			$this->document->setTitle($meta_title);
			$this->document->setDescription($meta_description);
			$this->document->setKeywords($meta_keyword);

			$data['heading_title'] = $title;

			if (isset($this->request->get['lookbooktag_id'])) {
				$data['breadcrumbs'][] = array(
					'text' => $title,
					'href' => $this->url->link('lookbook/lookbooktag', 'lookbookpath=' . $path . '&lookbooktag_id=' . $this->request->get['lookbooktag_id'])
				);
			}

			foreach ($lookbooks as $lookbook) {
				if ($lookbook['image'] && $data['show_image']) {
					$image = $this->model_tool_image->resize($lookbook['image'], $width, $height);
				} elseif ($data['show_image']) {
					$image = $this->model_tool_image->resize('placeholder.png', $width, $height);
				} else {
					$image = '';
				}

				if ($lookbook['show_description']) {
					$data['lookbooks'][] = array(
						'lookbook_id'  		=> $lookbook['lookbook_id'],
						'thumb'       		=> $image,
						'title'       		=> $lookbook['title'],
						'intro'       		=> html_entity_decode($lookbook['intro'], ENT_QUOTES, 'UTF-8'),
						'show_description' 	=> $lookbook['show_description'],
						'href'        		=> $this->url->link('lookbook/lookbook', 'lookbookpath=' . $path . '&lookbook_id=' . $lookbook['lookbook_id'])
					);
				} else {
					$data['lookbooks'][] = array(
						'lookbook_id'  		=> $lookbook['lookbook_id'],
						'thumb'       		=> $image,
						'title'       		=> $lookbook['title'],
						'intro'       		=> html_entity_decode($lookbook['intro'], ENT_QUOTES, 'UTF-8'),
						'show_description' 	=> $lookbook['show_description'],
						'href'        		=> ''
					);
				}
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
	
			$this->response->setOutput($this->load->view('lookbook/lookbooktag', $data));
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('lookbook/lookbooktag', 'lookbookpath=' . $path . '&lookbooktag_id=' . $this->request->get['lookbooktag_id'])
			);
			
			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}
}