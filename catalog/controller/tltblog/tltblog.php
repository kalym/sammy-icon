<?php

class ControllerTltBlogTltBlog extends Controller
{
    public function index()
    {
        $this->load->language('tltblog/tltblog');

        $this->load->model('tltblog/tltblog');
        $this->load->model('catalog/product');
        $this->load->model('setting/setting');
        $this->load->model('tool/image');

//        carousel
        $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
        $this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

        if ($this->config->get('tltblog_seo')) {
            require_once(DIR_APPLICATION . 'controller/tltblog/tltblog_seo.php');
            $tltblog_seo = new ControllerTltBlogTltBlogSeo($this->registry);
            $this->url->addRewrite($tltblog_seo);
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        if (isset($this->request->get['tltpath'])) {
            $path = $this->request->get['tltpath'];
        } elseif ($this->config->has('tltblog_path')) {
            $path = $this->config->get('tltblog_path');
        } else {
            $path = 'blogs';
        }

        $data['show_path'] = $this->config->get('tltblog_show_path');

        if ($data['show_path']) {
            if ($this->config->has('tltblog_path_title')) {
                $tmp_title = $this->config->get('tltblog_path_title');
                $root_title = $tmp_title[$this->config->get('config_language_id')]['path_title'];
            } else {
                $root_title = $this->language->get('text_title');
            }

            $data['breadcrumbs'][] = array(
                'text' => $root_title,
                'href' => $this->url->link('tltblog/tlttag', 'tltpath=' . $path)
            );
        }

        if (isset($this->request->get['tltblog_id'])) {
            $tltblog_id = (int)$this->request->get['tltblog_id'];
        } else {
            $tltblog_id = 0;
        }

        $tltblog_info = $this->model_tltblog_tltblog->getTltBlog($tltblog_id);

        if ($tltblog_info) {
            $this->document->setTitle($tltblog_info['meta_title']);
            $this->document->setDescription($tltblog_info['meta_description']);
            $this->document->setKeywords($tltblog_info['meta_keyword']);
            $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

            $this->document->addLink($this->url->link('tltblog/tltblog', 'tltpath=' . $path . '&tltblog_id=' . $tltblog_id), 'canonical');

            $data['breadcrumbs'][] = array(
                'text' => $tltblog_info['title'],
                'href' => $this->url->link('tltblog/tltblog', 'tltpath=' . $path . '&tltblog_id=' . $tltblog_id)
            );

            $data['heading_title'] = $tltblog_info['title'];
            $data['show_title'] = $tltblog_info['show_title'];


            $data['blog_image'] = $this->model_tool_image->cropSize($tltblog_info['image'],1100, 400);



            $data['description'] = html_entity_decode($tltblog_info['description'], ENT_QUOTES, 'UTF-8');
            $data['article_pt1'] = html_entity_decode($tltblog_info['article_pt1'], ENT_QUOTES, 'UTF-8');
            $data['article_pt2'] = html_entity_decode($tltblog_info['article_pt2'], ENT_QUOTES, 'UTF-8');
            $data['quote1'] = html_entity_decode($tltblog_info['quote1'], ENT_QUOTES, 'UTF-8');
            $data['quote2'] = html_entity_decode($tltblog_info['quote2'], ENT_QUOTES, 'UTF-8');
            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['text_related'] = $this->language->get('text_related');
            $data['text_tags'] = $this->language->get('text_tags');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));



            $data['tags'] = array();

            $tltblog_tags = $this->model_tltblog_tltblog->getTltTagsForBlog($tltblog_id);

            foreach ($tltblog_tags as $tltblog_tag) {
                $data['tags'][] = array(
                    'title' => $tltblog_tag['title'],
                    'href' => $this->url->link('tltblog/tlttag', 'tltpath=' . $path . '&tlttag_id=' . $tltblog_tag['tlttag_id'])
                );
            }

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

             $data['tltblogs'] = $this->populateRelatedArticles();
            //banner

            $this->load->model('design/banner');

            $data['banners'] = array();

            $results = $this->model_design_banner->getBanner($tltblog_info['banner_id']);

            foreach ($results as $result) {
                if (is_file(DIR_IMAGE . $result['image'])) {
                    $data['banners'][] = array(
                        'title' => $result['title'],
                        'link'  => $result['link'],
                        'image' => $this->model_tool_image->cropSize($result['image'], 1000, 400)
                    );
                }
            }

            $HISTORY_ID=29;

            if(isset($this->request->get['tltblog_id'])&& $this->request->get['tltblog_id']==$HISTORY_ID){
                $this->response->setOutput($this->load->view('tltblog/custom', $data));
            } else{
                $this->response->setOutput($this->load->view('tltblog/tltblog', $data));
            }

        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('tltblog/tltblog', 'tltpath=' . $path . '&tltblog_id=' . $tltblog_id)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');


            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    protected function populateRelatedArticles(){
        if ($this->config->has('tltblog_path')) {
            $tltpath = $this->config->get('tltblog_path');
        } else {
            $tltpath = 'blogs';
        }

        $tltblogs = array();



        $relatedArticles = $this->model_tltblog_tltblog->getTltBlogsBottom();
        $rand_keys = array_rand($relatedArticles, 2);

        foreach ($rand_keys as $key) {
            $tltblogs[] = $this->getRelatedArticleEntry($relatedArticles[$key],$tltpath);
        }
        return $tltblogs;
    }


    protected function getRelatedArticleEntry($result, $tltpath){
        return array(
            'title' => $result['title'],
            'href' => $this->url->link('tltblog/tltblog', 'tltpath=' . $tltpath . '&tltblog_id=' . $result['tltblog_id']),
            'image' => $this->model_tool_image->cropSize($result['image'],320,200)
        );
    }

}