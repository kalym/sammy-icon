<?php
class ControllerFeedPartner extends Controller {
    public function index() {
        $this->load->language('feed/partner');
        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $error = $this->language->get('text_error');
        }else{
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo('pl@sammy-icon.com');
            $mail->setFrom($this->request->post['email']);
            $mail->setSender(html_entity_decode("New partner", ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(html_entity_decode("Нова заявка на партнерство", ENT_QUOTES, 'UTF-8'));
            $mail->setText("Електрона адреса нового бажаючого бути партнером: ".$this->request->post['email']);
            $mail->send();

            $json['success'] = $this->language->get('text_success');
        }

        if (isset($error)) {
            $json['error_email'] = $error;
        } else {
            $json['error_email'] = '';
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}