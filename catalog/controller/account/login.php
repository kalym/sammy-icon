<?php
include_once DIR_ROOT . "oauth/OAuthManager.php";
include_once DIR_ROOT . "oauth/provider/User.php";
include_once DIR_ROOT . "oauth/AbstractUser.php";

class ControllerAccountLogin extends Controller {
	private $error = array();

	protected $oauthManager;

	/**
	 * ControllerOAuth constructor.
	 * @param $oauthManager
	 */
	public function __construct($registry)
	{
		parent::__construct( $registry );
		$this->oauthManager = new OAuthManager();
	}

	public function index() {
		$this->load->model('account/customer');

		// Login override for admin users
		if (!empty($this->request->get['token'])) {
			$this->customer->logout();
			$this->cart->clear();

			unset($this->session->data['order_id']);
			unset($this->session->data['payment_address']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['comment']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);

			$customer_info = $this->model_account_customer->getCustomerByToken($this->request->get['token']);

			if ($customer_info && $this->customer->login([
					'email' => 	$customer_info['email'],
					'password' => ""], true)) {
				// Default Addresses
				$this->load->model('account/address');

				if ($this->config->get('config_tax_customer') == 'payment') {
					$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				if ($this->config->get('config_tax_customer') == 'shipping') {
					$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				$this->response->redirect($this->url->link('account/account', '', true));
			}
		}

		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}


		$this->load->language('account/login');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->userLogin(true);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_login'),
			'href' => $this->url->link('account/login', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_new_customer'] = $this->language->get('text_new_customer');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_register_account'] = $this->language->get('text_register_account');
		$data['text_returning_customer'] = $this->language->get('text_returning_customer');
		$data['text_i_am_returning_customer'] = $this->language->get('text_i_am_returning_customer');
		$data['text_forgotten'] = $this->language->get('text_forgotten');

        // Login Form Text
        $data['text_enter'] = $this->language->get('text_enter');
        $data['text_forgotten'] = $this->language->get('text_forgotten');
        $data['text_login'] = $this->language->get('text_login');
        $data['text_registration'] = $this->language->get('text_registration');
        $data['text_password'] = $this->language->get('text_password');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_signup'] = $this->language->get('text_signup');
        $data['text_login_with'] = $this->language->get('text_login_with');
        $data['url_forgotten'] = $this->url->link('account/forgotten');


        $data['entry_email'] = $this->language->get('entry_email');
		$data['entry_password'] = $this->language->get('entry_password');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_login'] = $this->language->get('button_login');

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/login', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['forgotten'] = $this->url->link('account/forgotten', '', true);

		// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
		if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
			$data['redirect'] = $this->request->post['redirect'];
		} elseif (isset($this->session->data['redirect'])) {
			$data['redirect'] = $this->session->data['redirect'];

			unset($this->session->data['redirect']);
		} else {
			$data['redirect'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/login', $data));
	}
	
	public function redirectToFacebook()
	{
		$url = $this->oauthManager->getFacebookDriver()->redirect();
		$this->response->setOutput($url);
	}

	public function redirectToGooglePlus()
	{
		$url = $this->oauthManager->getGooglePlusDriver()->redirect();
		$this->response->setOutput($url);
	}

	public function redirectToVk()
	{
		$url = $this->oauthManager->getVkDriver()->redirect();
		$this->response->setOutput($url);
	}
	
	public function handleFbCallback()
	{
		$user = $this->oauthManager->getFacebookDriver()->user();
		$this->oauth($user);
	}

	public function handleGooglePlusCallback()
	{
		$user = $this->oauthManager->getGooglePlusDriver()->user();
		$this->oauth($user);

	}

	public function handleVkCallback()
	{
		$user = $this->oauthManager->getVkDriver()->user();
		$this->oauth($user);
	}

	public function oauth($user)
	{
		$this->load->model('account/customer');

		$existUser  = @$this->model_account_customer->getCustomerBySocialId($user->getId());
		
		if (count($existUser) === 0) {
			$existUser = $this->register($user);
		}
		$this->customer->login(['social_id' => $existUser['social_id']], false);

		$this->url->link('account/account', '', true);
		$this->response->redirect($this->url->link('account/account', '', true));
	}

	public function register($user)
	{
		$data = [
			'firstname' => $user->getFirstname(),
			'lastname' => $user->getLastname(),
			'type_provider' =>   $user->getProviderType(),
			'social_id' => $user->getId(),
			'token' => $user->token
		];
		$this->fillForm($data);

		$this->model_account_customer->addCustomer($data);

		return @$this->model_account_customer->getCustomerBySocialId($user->getId());
	}


	public function login(){
		$this->load->model('account/customer');
		$this->load->language('account/login');
		$json = array();

		if ($this->customer->isLogged()) {

			$json['redirect'] = $this->url->link('account/account', '', true);

		}elseif(!empty($this->request->post['email'])){

			$this->userLogin(false);

			if (isset($this->session->data['error'])) {
				$json['error'] = $this->session->data['error'];
				unset($this->session->data['error']);
			} elseif (isset($this->error['warning'])) {
				$json['error'] = $this->error['warning'];
			} else {
				$json['error'] = '';
			}

			if(isset($this->session->data['redirect'])){
				$json['redirect'] = $this->session->data['redirect'];
			}

		}else{
			$json['error'] = $this->language->get('error_login');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	protected function userLogin($redirectNow){
		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) || isset($this->request->get['code'])) {
			// Unset guest
			unset($this->session->data['guest']);

			// Default Shipping Address
			$this->load->model('account/address');

			if ($this->config->get('config_tax_customer') == 'payment') {
				$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			if ($this->config->get('config_tax_customer') == 'shipping' && !isset($this->request->get['code'])) {
				$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			// Wishlist
			if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
				$this->load->model('account/wishlist');

				foreach ($this->session->data['wishlist'] as $key => $product_id) {
					$this->model_account_wishlist->addWishlist($product_id);

					unset($this->session->data['wishlist'][$key]);
				}
			}

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('login', $activity_data);

			if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false) && ($this->request->post['redirect']!=$this->url->link('account/logout', '', true))) {
				$urlToRedirect =  str_replace('&amp;', '&', $this->request->post['redirect']);
			} else {
				$urlToRedirect = $this->url->link('account/account', '', true);
			}

			if($redirectNow)
			{
				$this->response->redirect($urlToRedirect);

			}else{
				$this->session->data['redirect'] = $urlToRedirect;
			}
		}
	}

	protected function validate() {
		// Check how many login attempts have been made.
		$login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

		if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
			$this->error['warning'] = $this->language->get('error_attempts');
		}

		// Check if customer has been approved.
		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['approved']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}

		if (!$this->error) {
			if (!$this->customer->login([
			                        'email' => $this->request->post['email'],
                                    'password' => $this->request->post['password']],
                                    false)) {

				$this->error['warning'] = $this->language->get('error_login');
				$this->model_account_customer->addLoginAttempt($this->request->post['email']);
			} else {
				$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
			}
		}

		return !$this->error;
	}


	protected function fillForm(&$data)
	{

		if (!isset($this->data['email']))
			$data['email'] = '';


		if (!isset($data['telephone']))
			$data['telephone'] = '';


		if (!isset($data['fax']))
			$data['fax'] = '';


		if (!isset($this->request->post['company']))
			$data['company'] = '';


		if (!isset($data['address_1']))
			$data['address_1'] = '';


		if (!isset($data['address_2']))
			$data['address_2'] = '';


		if (!isset($data['postcode'])) {
			$data['postcode'] = '';
		}

		if (!isset($data['city'])) {
			$data['city'] = '';

			if (isset($_SESSION['shipping_address']['country_id'])) {
				$data['country_id'] = $this->session->data['shipping_address']['country_id'];
			} else {
				$data['country_id'] = $this->config->get('config_country_id');
			}

			if (isset($_SESSION['shipping_address']['zone_id'])) {
				$data['zone_id'] = $this->session->data['shipping_address']['zone_id'];
			} else {
				$data['zone_id'] = '';
			}

			$this->load->model('localisation/country');

			$data['countries'] = $this->model_localisation_country->getCountries();

			// Custom Fields
			$this->load->model('account/custom_field');

			$data['custom_fields'] = $this->model_account_custom_field->getCustomFields();

			$data['register_custom_field'] = array();

			$data['password'] = '';


			$data['confirm'] = '';


			$data['newsletter'] = '';

			$data['captcha'] = '';


			if ($this->config->get('config_account_id')) {
				$this->load->model('catalog/information');

				$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

				if ($information_info) {
					$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), true), $information_info['title'], $information_info['title']);
				} else {
					$data['text_agree'] = '';
				}
			} else {
				$data['text_agree'] = '';
			}
		}
	}
}
