<?php
class ControllerModuleLookbooktag extends Controller {
	public function index() {
		$this->load->language('module/lookbooktag');

		$data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('lookbook/lookbook');
		$this->load->model('setting/setting');

		if ($this->config->get('lookbook_seo')) {
			require_once(DIR_APPLICATION . 'controller/lookbook/lookbook_seo.php');
			$lookbook_seo = new ControllerlookbooklookbookSeo($this->registry);
			$this->url->addRewrite($lookbook_seo);
		}

		$data['show_path'] = $this->config->get('lookbook_show_path');

		if (isset($this->request->get['lookbooktpath'])) {
			$path = $this->request->get['lookbooktpath'];
		} elseif ($this->config->has('lookbook_path')) {
			$path = $this->config->get('lookbook_path');
		} else {
			$path = 'blogs';
		}
		
		if (isset($this->request->get['lookbooktag_id'])) {
			$data['current_tag'] = $this->request->get['lookbooktag_id'];
		} else {
			$data['current_tag'] = '';
		}

		$data['tags'] = array();

		$tags = $this->model_lookbook_lookbook->getlookbooktags();
		
		if (!$data['current_tag'] && isset($this->request->get['lookbook_id'])) {
			$tagsforblog = $this->model_lookbook_lookbook->getlookbooktagsForBlog($this->request->get['lookbook_id']);
			if ($tagsforblog) {
				$data['current_tag'] = $tagsforblog[0]['lookbooktag_id'];
			}
		}
		
		foreach ($tags as $tag) {
			$data['tags'][] = array(
				'lookbooktag_id'  => $tag['lookbooktag_id'],
				'title' => $tag['title'],
				'href' => $this->url->link('lookbook/lookbooktag', 'lookbooktpath=' . $path . '&lookbooktag_id=' . $tag['lookbooktag_id'])
			);
		}
		
		return $this->load->view('module/lookbooktag', $data);
	}
}