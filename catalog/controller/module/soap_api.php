<?php
require_once(DIR_SYSTEM . 'SammyPackage/SammyPackageAutoload.php');

class ControllerModuleSoapApi extends Controller
{
    const LANGUAGE_UK = 3;
    const LANGUAGE_EN = 1;
    const BRAND = '0107SMI';
    const PARTNER = 'Sammy-Icon.com';
    const INVOICE_PREF = 'INV-2013-00';
    const STORE_ID = 0;
    const STORE_NAME = 'Sammy Icon';
    const CUSTOMER_ID = 0;
    const CUSTOMER_GROUP_ID = 0;
    const STORE_URL = 'http://www.sammy-icon.com/';

    private $error = array();
    public $username = 'SammyIconAPI';
    public $password = 'SammyIcon';


    public function syncOrders()
    {
        $this->load->model('checkout/order');

        ini_set("soap.wsdl_cache_disabled", "0"); // TODO: disable WSDL cache if required
        ini_set("default_socket_timeout", 600);   // TODO: set appropriate read timeout

        $wsdl = array();
        $wsdl[SammyPackageWsdlClass::WSDL_URL] = 'http://87.110.224.90/UT3/ws/Exchange_ukrmarketplaceservice?wsdl';
        $wsdl[SammyPackageWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
        $wsdl[SammyPackageWsdlClass::WSDL_TRACE] = true;
        $wsdl[SammyPackageWsdlClass::WSDL_LOGIN] = 'SammyIconAPI';
        $wsdl[SammyPackageWsdlClass::WSDL_PASSWD] = 'SammyIcon';

        $sammyPackageServiceGet = new SammyPackageServiceGet($wsdl);

        $_dateFrom = '2017-01-29 04:49:45';
        $_dateTo = '2018-09-19 02:18:33';

        $sammyPackageServiceGet->GetOrders(new SammyPackageStructGetOrders($_dateFrom, $_dateTo));
        $res = ($sammyPackageServiceGet->getResult());

        $resObj = [
            'DataField' => '2018-02-18 12:00:18',
            'OrderId' => '25025',
            'ClientName' => 'Поляков Сергей',
            'Street' => 'Borysa Hmyri Street h.',
            'BuildingNumber' => '1Б',
            'AppartmentNumber' => '6',
            'City' => 'Kyiv',
            'PostCode' => 02000,
            'Region' => null,
            'Phone' => '+380965599520',
            'ShipmentID' => 'e33erer',
            'BranchCarrier' => 'Отделение №5 (до 30 кг на одно место): ул. Академика Филатова, 24',
            'Items' => [
                [
                    'product' => '454',
                    'quantity' => 1,
                    'PriceUAH' => 10,
                ],
                [
                    'product' => '244',
                    'quantity' => 10,
                    'PriceUAH' => 10,
                ]
            ]
        ];
        $totalPrice = 0;
        foreach ($resObj['Items'] as $item) {
            $totalPrice = $totalPrice + $item['PriceUAH'] * $item['quantity'];
        }
        $clientName = explode(' ', trim($resObj['ClientName']));

        $orderResult = [
            'invoice_prefix' => self::INVOICE_PREF,
            'store_id' => self::STORE_ID,
            'store_name' => self::STORE_NAME,
            'store_url' => self::STORE_URL,
            'customer_id' => self::CUSTOMER_ID,
            'customer_group_id' => self::CUSTOMER_GROUP_ID,
            'firstname' => $clientName[1],
            'lastname' => $clientName[0],
            'email' => null,
            'telephone' => $resObj['Phone'],
            'payment_firstname' => null,
            'payment_lastname' => null,
            'payment_company' => null,
            'payment_address_1' => $resObj['Street'] . ' ' . $resObj['BuildingNumber'] . ' ' . $resObj['AppartmentNumber'],
            'payment_address_2' => null,
            'payment_city' => null,
            'payment_postcode' => null,
            'payment_country' => null,
            'payment_country_id' => null,
            'payment_zone' => null,
            'payment_zone_id' => null,
            'payment_method' => null,
            'payment_code' => null,
            'shipping_firstname' => $clientName[1],
            'shipping_lastname' => $clientName[0],
            'shipping_company' => null,
            'shipping_address_1' => $resObj['BranchCarrier'],
            'shipping_address_2' => null,
            'shipping_city' => $resObj['City'],
            'shipping_postcode' => $resObj['PostCode'],
            'shipping_country' => null,
            'shipping_country_id' => null,
            'shipping_zone' => null,
            'shipping_zone_id' => null,
            'shipping_address_format' => null,
            'shipping_method' => null,
            'shipping_code' => null,
            'comment' => null,
            'total' => $totalPrice,
            'affiliate_id' => null,
            'commission' => null,
            'marketing_id' => null,
            'tracking' => $resObj['ShipmentID'],
            'soap_order_id' => $resObj['OrderId'],
            'language_id' => null,
            'currency_id' => null,
            'currency_code' => null,
            'currency_value' => null,
            'ip' => null,
            'forwarded_ip' => null,
            'user_agent' => null,
            'accept_language' => null,
        ];

        $p_items = $this->model_checkout_order->insertProductToOrder($resObj['Items'], $resObj['OrderId']);

        if (!$this->model_checkout_order->getOrderBySoapId($orderResult['soap_order_id'])) {
            $orderDesc = $this->model_checkout_order->addOrder($orderResult);
        };
    }

    public function uploadProduct()
    {
        $gender = $this->request->get['gender'] ? $this->request->get['gender'] : 'male';

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $this->load->model('catalog/productDesc');
        $this->load->model('catalog/option');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        $CATEGORIES = [
            'male' => [60, 61, 74, 62],
            'female' => [66, 68, 73, 69],
            'min' => [73]
        ];

        set_time_limit(0);
        ini_set("soap.wsdl_cache_disabled", "0"); // TODO: disable WSDL cache if required
        ini_set("default_socket_timeout", 6000);   // TODO: set appropriate read timeout

        $wsdl = array();
        $wsdl[SammyPackageWsdlClass::WSDL_URL] = 'http://87.110.224.90/UT3/ws/Exchange_ukrmarketplaceservice?wsdl';
        $wsdl[SammyPackageWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
        $wsdl[SammyPackageWsdlClass::WSDL_TRACE] = true;
        $wsdl[SammyPackageWsdlClass::WSDL_LOGIN] = 'SammyIconAPI';
        $wsdl[SammyPackageWsdlClass::WSDL_PASSWD] = 'SammyIcon';

        $sammyPackageServiceSet = new SammyPackageServiceSet($wsdl);

        $mapping = [
            'female_gender' => '41S0T01',  // gender
            'min_gender' => '41S0T01',  // gender
            'male_gender' => '41S0T02',  // gender
            'productCategory' => '41SAG03', // productCategory
            'structure' => '41S1TAC', // structure
            'female_sex' => '41S2T01',  // sex
            'male_sex' => '41S2T02', // sex
            'min_sex' => '41S2T01', // sex
        ];

        $resProducts = [];

        foreach ($CATEGORIES[$gender] as $category) {
            $results = $this->model_catalog_productDesc->getProducts(['filter_categories' => $category]);
            foreach ($results as $product) {
                $productDesc = $this->model_catalog_productDesc->getProductDescriptions($product['product_id']);
                $options = $this->model_catalog_productDesc->getProductOptions($product['product_id']);
                $options_list = [];
                foreach ($options as $option) {
                    if ($option['name'] == 'Размер') {
                        $option_values = $this->model_catalog_option->getOptionValues($option['option_id']);
                        foreach ($option_values as $option_value) {
                            if (is_string($option_value['name']) ) {
                                array_push($options_list, new SammyPackageStructSize($option_value['name']));
                            }
                        }
                    }
                }

                $file_name = null;
                $imageUrl = DIR_ROOT . 'image/' . $product['image'];
                $file_mime = file_exists($imageUrl) ? mime_content_type($imageUrl) : null;
                if ($file_mime == 'image/png') {
                    $file_name = basename(DIR_ROOT . 'image/' . $product['image'], ".png");
                    $image = imagecreatefrompng(DIR_ROOT . 'image/' . $product['image']);
                    $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
                    imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
                    imagealphablending($bg, TRUE);
                    imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
                    imagedestroy($image);
                    $quality = 90;
                    imagejpeg($bg, DIR_ROOT . 'image/cache/catalog/' . $file_name . ".jpg", $quality);
                    imagedestroy($bg);
                }

                array_push($resProducts, new SammyPackageStructProduct(
                        $product['product_id'],
                        $product['status'],
                        $product['product_id'],
                        $product['name'],
                        $productDesc[self::LANGUAGE_EN]['name'],
                        null,
                        null,
                        $options_list,
                        new SammyPackageStructImage(true, $file_name ?
                            HTTPS_SERVER . 'image/cache/catalog/' . $file_name . ".jpg" :
                            HTTPS_SERVER . 'image/' . $product['image']),
                        new SammyPackageStructProperties(
                            self::BRAND,
                            $mapping[$gender . '_gender'],
                            $mapping['productCategory'],
                            null,
                            null,
                            $mapping['structure'],
                            null,
                            null,
                            $mapping['productCategory'],
                            null,
                            $mapping[$gender . '_sex'],
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null
                        ))
                );
            }
        }

        $setProduct = new SammyPackageStructSetProduct(new SammyPackageStructProducts($resProducts));
        $sammyPackageServiceSet->SetProduct($setProduct);
        $res = $sammyPackageServiceSet->getResult();
        $productCount = count($resProducts);

        echo "<p>Количество товаров: {$productCount}</p>";
        echo "<details>
           <summary>Тело запроса</summary>
           <p><code>" . htmlentities($res->getLastRequest()) . "</code></p>
          </details>";
                echo "<details>
           <summary>Ответ</summary>
           <p><code>" . htmlentities($res->getLastResponse()) . "</code><br/>
           {$res->getLastResponseHeaders()}</p>
          </details>";
    }

    public function getProducts() {
        $id = $this->request->get['id'];

        set_time_limit(0);
        ini_set("soap.wsdl_cache_disabled", "0"); // TODO: disable WSDL cache if required
        ini_set("default_socket_timeout", 6000);   // TODO: set appropriate read timeout

        $wsdl = array();
        $wsdl[SammyPackageWsdlClass::WSDL_URL] = 'http://87.110.224.90/UT3/ws/Exchange_ukrmarketplaceservice?wsdl';
        $wsdl[SammyPackageWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
        $wsdl[SammyPackageWsdlClass::WSDL_TRACE] = true;
        $wsdl[SammyPackageWsdlClass::WSDL_LOGIN] = 'SammyIconAPI';
        $wsdl[SammyPackageWsdlClass::WSDL_PASSWD] = 'SammyIcon';

        $sammyPackageServiceGet = new SammyPackageServiceGet($wsdl);

        $sammyPackageServiceGet->GetProduct(new SammyPackageStructGetProduct(new SammyPackageStructFilter($id)));

        echo $sammyPackageServiceGet->getLastResponse();
    }
}