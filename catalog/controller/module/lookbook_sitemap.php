<?php
class ControllerModulelookbookSitemap extends Controller {
	public function index($setting) {
		$this->load->language('module/lookbook_sitemap');

		$this->load->model('lookbook/lookbook');
		$this->load->model('setting/setting');

		if ($this->config->get('lookbook_seo')) {
			require_once(DIR_APPLICATION . 'controller/lookbook/lookbook_seo.php');
			$lookbook_seo = new ControllerlookbooklookbookSeo($this->registry);
			$this->url->addRewrite($lookbook_seo);
		}

		$data['heading_blogs'] = $this->language->get('heading_blogs');
		$data['heading_tags'] = $this->language->get('heading_tags');

		if ($this->config->has('lookbook_path')) {
			$path = $this->config->get('lookbook_path');
		} else {
			$path = 'blogs';
		}

		if ($this->config->has('lookbook_path_title')) {
			$path_title = $this->config->get('lookbook_path_title');
			$data['path_title'] = $path_title[$this->config->get('config_language_id')]['path_title'];
		} else {
			$data['path_title'] = $this->language->get('heading_title');
		}

		$data['show_path'] = $this->config->get('lookbook_show_path');

		$data['lookbooks'] = array();
		$data['lookbooktags'] = array();
		$data['type'] = $setting['type'];
		$lookbooks = array();
		$lookbooktags = array();

		if ($setting['type'] == 'blogs') {
			$lookbooks = $this->model_lookbook_lookbook->getlookbooks();
		} elseif ($setting['type'] == 'tags') {
			$lookbooktags = $this->model_lookbook_lookbook->getlookbooktags();
		} else {
			$lookbooks = $this->model_lookbook_lookbook->getlookbooks();
			$lookbooktags = $this->model_lookbook_lookbook->getlookbooktags();
		}

		foreach ($lookbooks as $lookbook) {
			if ($lookbook['show_description']) {		
				$data['lookbooks'][] = array(
					'title'     => $lookbook['title'],
					'href'     => $this->url->link('lookbook/lookbook', 'lookbookpath=' . $path . '&lookbook_id=' . $lookbook['lookbook_id'])
				);
			}
		}

		foreach ($lookbooktags as $lookbooktag) {		
			$data['lookbooktags'][] = array(
				'title'     => $lookbooktag['title'],
				'href'     => $this->url->link('lookbook/lookbooktag', 'lookbookpath=' . $path . '&lookbooktag_id=' . $lookbooktag['lookbooktag_id'])
			);
		}

		return $this->load->view('module/lookbook_sitemap', $data);
	}
}