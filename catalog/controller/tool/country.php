<?php
/**
 * Created by IntelliJ IDEA.
 * User: ostfr
 * Date: 24.06.2016
 * Time: 17:41
 */
class ControllerToolCountry extends Controller {

    public function index() {

        if (isset($this->request->get['route'])) {
            $this->document->addLink(HTTP_SERVER, 'canonical');
        }
        $data = array();
        $data['countrySaveLink'] = $this->url->link('tool/country/save');
        $this->response->setOutput($this->load->view('tool/country', $data));
    }

    public function save(){

        if(isset($this->request->get['country'])) {
            $country = $this->request->get['country'];
            $this->session->data['customer_country'] = $country;
            unset($_COOKIE['customer_country']);
            setcookie('customer_country', $country ,time()+3600*24*360);

            if($this->request->get['country']!='Ukraine'){
                $this->session->data['currency'] = 'USD';
                $redirect = '/en';
            }else{
                $this->session->data['currency'] = 'UAH';
                $this->session->data['language'] = 'ua-uk';
                $redirect = '/';
            }

            $this->response->redirect($redirect);

        }else{

            $this->response->redirect($this->url->link('tool/country'));
        }

    }

}