-- Create syntax for TABLE 'oc_lookbook'
CREATE TABLE IF NOT EXISTS `oc_lookbook` (
  `lookbook_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `bottom` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `show_description` tinyint(1) NOT NULL DEFAULT '0',
  `show_title` tinyint(1) NOT NULL DEFAULT '0',  
  PRIMARY KEY (`lookbook_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;







-- Create syntax for TABLE 'oc_lookbook_description'
CREATE TABLE IF NOT EXISTS `oc_lookbook_description` (
  `lookbook_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`lookbook_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Create syntax for TABLE 'oc_lookbook_related'
CREATE TABLE IF NOT EXISTS `oc_lookbook_related` (
  `lookbook_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  PRIMARY KEY (`lookbook_id`,`related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Create syntax for TABLE 'oc_lookbook_to_layout'
CREATE TABLE IF NOT EXISTS `oc_lookbook_to_layout` (
  `lookbook_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`lookbook_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Create syntax for TABLE 'oc_lookbook_to_store'
CREATE TABLE IF NOT EXISTS `oc_lookbook_to_store` (
  `lookbook_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lookbook_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Create syntax for TABLE 'oc_lookbook_to_tag'
CREATE TABLE IF NOT EXISTS `oc_lookbook_to_tag` (
  `lookbook_id` int(11) NOT NULL,
  `lookbooktag_id` int(11) NOT NULL,
  PRIMARY KEY (`lookbook_id`,`lookbooktag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Create syntax for TABLE 'oc_lookbooktag'
CREATE TABLE IF NOT EXISTS `oc_lookbooktag` (
  `lookbooktag_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`lookbooktag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Create syntax for TABLE 'oc_lookbooktag_description'
CREATE TABLE IF NOT EXISTS `oc_lookbooktag_description` (
  `lookbooktag_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`lookbooktag_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Create syntax for TABLE 'oc_lookbooktag_to_store'
CREATE TABLE IF NOT EXISTS `oc_lookbooktag_to_store` (
  `lookbooktag_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lookbooktag_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `oc_lookbook_url_alias` (
  `url_alias_id` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`url_alias_id`),
  KEY `query` (`query`),
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
