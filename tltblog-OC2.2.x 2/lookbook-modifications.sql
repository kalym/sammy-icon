DROP TABLE `oc_lookbook`, `oc_lookbooktag`, `oc_lookbooktag_description`, `oc_lookbooktag_to_store`, `oc_lookbook_description`, `oc_lookbook_related`, `oc_lookbook_to_layout`, `oc_lookbook_to_store`, `oc_lookbook_to_tag`, `oc_lookbook_url_alias`;





CREATE TABLE `oc_lookbook`  (
  `lookbook_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `banner_id` int(11) NOT NULL,
  `bottom` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `show_description` tinyint(1) NOT NULL DEFAULT '0',
  `show_title` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_lookbook`
--



-- --------------------------------------------------------

--
-- Table structure for table `oc_lookbooktag`
--

CREATE TABLE `oc_lookbooktag` (
  `lookbooktag_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_lookbooktag_description`
--

CREATE TABLE `oc_lookbooktag_description` (
  `lookbooktag_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_lookbooktag_to_store`
--

CREATE TABLE `oc_lookbooktag_to_store` (
  `lookbooktag_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_lookbook_description`
--

CREATE TABLE `oc_lookbook_description` (
  `lookbook_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `description` text NOT NULL,
  `article_pt1` text NOT NULL,
  `article_pt2` text NOT NULL,
  `quote1` varchar(100) NOT NULL,
  `quote2` varchar(100) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_lookbook_description`
--


-- --------------------------------------------------------

--
-- Table structure for table `oc_lookbook_related`
--

CREATE TABLE `oc_lookbook_related` (
  `lookbook_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_lookbook_to_layout`
--

CREATE TABLE `oc_lookbook_to_layout` (
  `lookbook_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_lookbook_to_layout`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_lookbook_to_store`
--

CREATE TABLE `oc_lookbook_to_store` (
  `lookbook_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_lookbook_to_store`
--



-- --------------------------------------------------------

--
-- Table structure for table `oc_lookbook_to_tag`
--

CREATE TABLE `oc_lookbook_to_tag` (
  `lookbook_id` int(11) NOT NULL,
  `lookbooktag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_lookbook_url_alias`
--

CREATE TABLE `oc_lookbook_url_alias` (
  `url_alias_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_lookbook_url_alias`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `oc_lookbook`
--
ALTER TABLE `oc_lookbook`
  ADD PRIMARY KEY (`lookbook_id`);

--
-- Indexes for table `oc_lookbooktag`
--
ALTER TABLE `oc_lookbooktag`
  ADD PRIMARY KEY (`lookbooktag_id`);

--
-- Indexes for table `oc_lookbooktag_description`
--
ALTER TABLE `oc_lookbooktag_description`
  ADD PRIMARY KEY (`lookbooktag_id`,`language_id`);

--
-- Indexes for table `oc_lookbooktag_to_store`
--
ALTER TABLE `oc_lookbooktag_to_store`
  ADD PRIMARY KEY (`lookbooktag_id`,`store_id`);

--
-- Indexes for table `oc_lookbook_description`
--
ALTER TABLE `oc_lookbook_description`
  ADD PRIMARY KEY (`lookbook_id`,`language_id`);

--
-- Indexes for table `oc_lookbook_related`
--
ALTER TABLE `oc_lookbook_related`
  ADD PRIMARY KEY (`lookbook_id`,`related_id`);

--
-- Indexes for table `oc_lookbook_to_layout`
--
ALTER TABLE `oc_lookbook_to_layout`
  ADD PRIMARY KEY (`lookbook_id`,`store_id`);

--
-- Indexes for table `oc_lookbook_to_store`
--
ALTER TABLE `oc_lookbook_to_store`
  ADD PRIMARY KEY (`lookbook_id`,`store_id`);

--
-- Indexes for table `oc_lookbook_to_tag`
--
ALTER TABLE `oc_lookbook_to_tag`
  ADD PRIMARY KEY (`lookbook_id`,`lookbooktag_id`);

--
-- Indexes for table `oc_lookbook_url_alias`
--
ALTER TABLE `oc_lookbook_url_alias`
  ADD PRIMARY KEY (`url_alias_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_lookbook`
--
ALTER TABLE `oc_lookbook`
  MODIFY `lookbook_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `oc_lookbooktag`
--
ALTER TABLE `oc_lookbooktag`
  MODIFY `lookbooktag_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_lookbook_url_alias`
--
ALTER TABLE `oc_lookbook_url_alias`
  MODIFY `url_alias_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;