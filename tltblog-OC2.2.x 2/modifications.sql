DROP TABLE `oc_tltblog`, `oc_tltblog_description`, `oc_tltblog_related`, `oc_tltblog_to_layout`, `oc_tltblog_to_store`, `oc_tltblog_to_tag`, `oc_tltblog_url_alias`, `oc_tlttag`, `oc_tlttag_description`, `oc_tlttag_to_store`;

CREATE TABLE `oc_tltblog` (
  `tltblog_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `banner_id` int(11) NOT NULL,
  `bottom` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `show_description` tinyint(1) NOT NULL DEFAULT '0',
  `show_title` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tltblog`
--


-- --------------------------------------------------------

--
-- Table structure for table `oc_tltblog_description`
--

CREATE TABLE `oc_tltblog_description` (
  `tltblog_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `intro` text NOT NULL,
  `description` text NOT NULL,
  `article_pt1` text NOT NULL,
  `article_pt2` text NOT NULL,
  `quote1` varchar(100) NOT NULL,
  `quote2` varchar(100) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tltblog_description`
--



-- --------------------------------------------------------

--
-- Table structure for table `oc_tltblog_related`
--

CREATE TABLE `oc_tltblog_related` (
  `tltblog_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tltblog_to_layout`
--

CREATE TABLE `oc_tltblog_to_layout` (
  `tltblog_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tltblog_to_layout`
--



-- --------------------------------------------------------

--
-- Table structure for table `oc_tltblog_to_store`
--

CREATE TABLE `oc_tltblog_to_store` (
  `tltblog_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tltblog_to_store`
--



-- --------------------------------------------------------

--
-- Table structure for table `oc_tltblog_to_tag`
--

CREATE TABLE `oc_tltblog_to_tag` (
  `tltblog_id` int(11) NOT NULL,
  `tlttag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tltblog_url_alias`
--

CREATE TABLE `oc_tltblog_url_alias` (
  `url_alias_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tltblog_url_alias`
--



-- --------------------------------------------------------

--
-- Table structure for table `oc_tlttag`
--

CREATE TABLE `oc_tlttag` (
  `tlttag_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tlttag_description`
--

CREATE TABLE `oc_tlttag_description` (
  `tlttag_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tlttag_to_store`
--

CREATE TABLE `oc_tlttag_to_store` (
  `tlttag_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oc_tltblog`
--
ALTER TABLE `oc_tltblog`
  ADD PRIMARY KEY (`tltblog_id`);

--
-- Indexes for table `oc_tltblog_description`
--
ALTER TABLE `oc_tltblog_description`
  ADD PRIMARY KEY (`tltblog_id`,`language_id`);

--
-- Indexes for table `oc_tltblog_related`
--
ALTER TABLE `oc_tltblog_related`
  ADD PRIMARY KEY (`tltblog_id`,`related_id`);

--
-- Indexes for table `oc_tltblog_to_layout`
--
ALTER TABLE `oc_tltblog_to_layout`
  ADD PRIMARY KEY (`tltblog_id`,`store_id`);

--
-- Indexes for table `oc_tltblog_to_store`
--
ALTER TABLE `oc_tltblog_to_store`
  ADD PRIMARY KEY (`tltblog_id`,`store_id`);

--
-- Indexes for table `oc_tltblog_to_tag`
--
ALTER TABLE `oc_tltblog_to_tag`
  ADD PRIMARY KEY (`tltblog_id`,`tlttag_id`);

--
-- Indexes for table `oc_tltblog_url_alias`
--
ALTER TABLE `oc_tltblog_url_alias`
  ADD PRIMARY KEY (`url_alias_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Indexes for table `oc_tlttag`
--
ALTER TABLE `oc_tlttag`
  ADD PRIMARY KEY (`tlttag_id`);

--
-- Indexes for table `oc_tlttag_description`
--
ALTER TABLE `oc_tlttag_description`
  ADD PRIMARY KEY (`tlttag_id`,`language_id`);

--
-- Indexes for table `oc_tlttag_to_store`
--
ALTER TABLE `oc_tlttag_to_store`
  ADD PRIMARY KEY (`tlttag_id`,`store_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_tltblog`
--
ALTER TABLE `oc_tltblog`
  MODIFY `tltblog_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `oc_tltblog_url_alias`
--
ALTER TABLE `oc_tltblog_url_alias`
  MODIFY `url_alias_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `oc_tlttag`
--
ALTER TABLE `oc_tlttag`
  MODIFY `tlttag_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
