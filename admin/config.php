<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:8888/admin/');
define('HTTP_CATALOG', 'http://localhost:8888/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:8888/admin/');
define('HTTPS_CATALOG', 'http://localhost:8888/');

// DIR
define('DIR_APPLICATION', '/Applications/MAMP/htdocs/sammy-icon/admin/');
define('DIR_SYSTEM', '/Applications/MAMP/htdocs/sammy-icon/system/');
define('DIR_IMAGE', '/Applications/MAMP/htdocs/sammy-icon/image/');
define('DIR_LANGUAGE', '/Applications/MAMP/htdocs/sammy-icon/admin/language/');
define('DIR_TEMPLATE', '/Applications/MAMP/htdocs/sammy-icon/admin/view/template/');
define('DIR_CONFIG', '/Applications/MAMP/htdocs/sammy-icon/system/config/');
define('DIR_CACHE', '/Applications/MAMP/htdocs/sammy-icon/system/storage/cache/');
define('DIR_DOWNLOAD', '/Applications/MAMP/htdocs/sammy-icon/system/storage/download/');
define('DIR_LOGS', '/Applications/MAMP/htdocs/sammy-icon/system/storage/logs/');
define('DIR_MODIFICATION', '/Applications/MAMP/htdocs/sammy-icon/system/storage/modification/');
define('DIR_UPLOAD', '/Applications/MAMP/htdocs/sammy-icon/system/storage/upload/');
define('DIR_CATALOG', '/Applications/MAMP/htdocs/sammy-icon/catalog/');
define('DIR_ROOT', '/Applications/MAMP/htdocs/sammy-icon/');
// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'sammy');
define('DB_PORT', '8889');
define('DB_PREFIX', 'oc_');
