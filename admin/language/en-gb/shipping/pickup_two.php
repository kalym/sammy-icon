<?php
// Heading
$_['heading_title']    = 'Pickup_two From Store';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified pickup_two from store!';
$_['text_edit']        = 'Edit pickup_two From Store Shipping';

// Entry
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify pickup_two from store!';