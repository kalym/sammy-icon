<?php
// Heading
$_['heading_title'] = 'TLT Multilang';

// Text
$_['text_module'] = 'Modules';
$_['text_success'] = 'Success: You have modified TLT Multilang!';
$_['text_edit'] = 'Settings';

// Entry
$_['entry_meta_title'] = 'Meta Title';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_meta_keyword'] = 'Meta Tag Keywords';

// Error
$_['error_permission'] 	= 'Warning: You do not have permission to modify TLT Multilang!';
$_['error_meta_title']  = 'Title must be between 3 and 70 characters!';
