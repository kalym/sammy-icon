<?php
// Heading
$_['heading_title']    = 'Інпост';

// Text
$_['text_shipping']    = 'Доставка';
$_['text_success']     = 'Успіх: Змінено прехоплення доставки у магазина!';
$_['text_edit']        = 'Змінити прехоплення доставки у магазина';

// Entry
$_['entry_cost']       = 'Вартість';
$_['entry_tax_class']  = 'Клас податку';
$_['entry_geo_zone']   = 'Геозона';
$_['entry_status']     = 'Стан';
$_['entry_sort_order'] = 'Порядок сортування';

// Error
$_['error_permission'] = 'Попередження: Нема дозволу на зміну прехоплення доставки у магазина!';