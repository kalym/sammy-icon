<?php
// Heading
$_['heading_title']    = 'Soap api';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified soap api module!';
$_['text_edit']        = 'Edit soap api Module';

// Entry
$_['entry_status']                   = 'Status';
$_['entry_soap_api_wsdl']            = 'WSDL';
$_['entry_soap_api_getproducts']     = 'Get products function';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify soap api module!';