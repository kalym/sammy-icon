<?php
require_once(DIR_SYSTEM . 'SammyPackage/SammyPackageAutoload.php');

class ControllerModuleSoapApi extends Controller
{
    const LANGUAGE_UK = 3;
    const LANGUAGE_EN = 1;
    const BRAND = 'Sammy';
    const PARTNER = 'Sammy-Icon.com';
    const INVOICE_PREF = 'INV-2013-00';
    const STORE_ID = 0;
    const STORE_NAME = 'Sammy Icon';
    const CUSTOMER_ID = 0;
    const CUSTOMER_GROUP_ID = 0;
    const STORE_URL = 'http://www.sammy-icon.com/';

    private $error = array();
    public $username = 'SammyIconAPI';
    public $password = 'SammyIcon';


    public function index()
    {
        $this->load->model('catalog/product');
        $this->load->model('catalog/option');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        $this->load->model('sale/addorder');

        $CATEGORIES = [
            'чол.' => [60, 61, 74],
            'жін.' => [66, 68, 73]
        ];

        ini_set("soap.wsdl_cache_disabled", "0"); // TODO: disable WSDL cache if required
        ini_set("default_socket_timeout", 600);   // TODO: set appropriate read timeout

        $wsdl = array();
        $wsdl[SammyPackageWsdlClass::WSDL_URL] = 'http://87.110.224.90/UT3/ws/Exchange_ukrmarketplaceservice?wsdl';
        $wsdl[SammyPackageWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
        $wsdl[SammyPackageWsdlClass::WSDL_TRACE] = true;
        $wsdl[SammyPackageWsdlClass::WSDL_LOGIN] = 'SammyIconAPI';
        $wsdl[SammyPackageWsdlClass::WSDL_PASSWD] = 'SammyIcon';

        $sammyPackageServiceGet = new SammyPackageServiceGet($wsdl);

        $_dateFrom = '2017-01-29 04:49:45';
        $_dateTo = '2018-09-19 02:18:33';

        $sammyPackageServiceGet->GetOrders(new SammyPackageStructGetOrders($_dateFrom, $_dateTo));
        $res = ($sammyPackageServiceGet->getResult());

            $resObj = [
                'DataField' => '2018-02-18 12:00:18',
                'OrderId' => '25025',
                'ClientName' => 'Поляков Сергей',
                'Street' => 'Borysa Hmyri Street h.',
                'BuildingNumber' => '1Б',
                'AppartmentNumber' => '6',
                'City' => 'Kyiv',
                'PostCode' => 02000,
                'Region' => null,
                'Phone' => '+380965599520',
                'ShipmentID' => 'e33erer',
                'BranchCarrier' => 'Отделение №5 (до 30 кг на одно место): ул. Академика Филатова, 24',
                'Items' => [
                    [
                        'product' => '454',
                        'quantity' => 1,
                        'PriceUAH' => 10,
                    ],
                    [
                        'product' => '244',
                        'quantity' => 10,
                        'PriceUAH' => 10,
                    ]
                ]
            ];
            $totalPrice = 0;
            foreach ($resObj['Items'] as $item){
                $totalPrice = $totalPrice + $item['PriceUAH'] * $item['quantity'];
            }
            $clientName = explode(' ',trim($resObj['ClientName']));

            $orderResult = [
                'invoice_prefix' => self::INVOICE_PREF,
                'store_id' => self::STORE_ID,
                'store_name' => self::STORE_NAME,
                'store_url' => self::STORE_URL,
                'customer_id' => self::CUSTOMER_ID,
                'customer_group_id' => self::CUSTOMER_GROUP_ID,
                'firstname' => $clientName[1],
                'lastname' => $clientName[0],
                'email' => null,
                'telephone' => $resObj['Phone'],
                'payment_firstname' => null,
                'payment_lastname' => null,
                'payment_company' => null,
                'payment_address_1' => $resObj['Street'] .' '. $resObj['BuildingNumber'] .' '. $resObj['AppartmentNumber'],
                'payment_address_2' => null,
                'payment_city' => null,
                'payment_postcode' => null,
                'payment_country' => null,
                'payment_country_id' => null,
                'payment_zone' => null,
                'payment_zone_id' => null,
                'payment_method' => null,
                'payment_code' => null,
                'shipping_firstname' => $clientName[1],
                'shipping_lastname' => $clientName[0],
                'shipping_company' => null,
                'shipping_address_1' => $resObj['BranchCarrier'],
                'shipping_address_2' => null,
                'shipping_city' => $resObj['City'],
                'shipping_postcode' => $resObj['PostCode'],
                'shipping_country' => null,
                'shipping_country_id' => null,
                'shipping_zone' => null,
                'shipping_zone_id' => null,
                'shipping_address_format' => null,
                'shipping_method' => null,
                'shipping_code' => null,
                'comment' => null,
                'total' => $totalPrice,
                'affiliate_id' => null,
                'commission' => null,
                'marketing_id' => null,
                'tracking' => $resObj['ShipmentID'],
                'soap_order_id' => $resObj['OrderId'],
                'language_id' => null,
                'currency_id' => null,
                'currency_code' => null,
                'currency_value' => null,
                'ip' => null,
                'forwarded_ip' => null,
                'user_agent' => null,
                'accept_language' => null,
            ];


        $p_items = $this->model_sale_addorder->insertProductToOrder($resObj['Items'], $resObj['OrderId']);

        if(!$this->model_sale_addorder->getOrderBySoapId($orderResult['soap_order_id'])){
            $orderDesc = $this->model_sale_addorder->addOrder($orderResult);
        };

//        $sammyPackageServiceSet = new SammyPackageServiceSet($wsdl);
//
//        $resProducts = [];
//
//        foreach ($CATEGORIES as $sex => $categories){
//            foreach ($categories as $category){
//                $results = $this->model_catalog_product->getProducts(['filter_categories' => $category]);
//                $category_data = $this->model_catalog_category->getCategory($category);
//                foreach ($results as $product) {
//                    $productDesc = $this->model_catalog_product->getProductDescriptions($product['product_id']);
//                    $categories = $this->model_catalog_product->getProductCategories($product['product_id']);
//                    $attributes = $this->model_catalog_product->getProductAttributes($product['product_id']);
//                    $options = $this->model_catalog_product->getProductOptions($product['product_id']);
//                    $options_list = [];
//                    foreach ($options as $option) {
//                        if($option['name'] == 'Размер' ){
//                            $option_values = $this->model_catalog_option->getOptionValues($option['option_id']);
//                            foreach ($option_values as $option_value) {
//                                array_push($options_list, new SammyPackageStructSize($option_value['name']));
//                            }
//                        }
//                    }
//                    array_push($resProducts, new SammyPackageStructProduct(
//                            $product['product_id'],
//                            $product['status'],
//                            null,
//                            $product['name'],
//                            $productDesc[self::LANGUAGE_EN]['name'],
//                            null,
//                            null,
//                            $options_list,
//                            new SammyPackageStructImage(true, 'http://sammy-icon/image/' . $product['image']),
//                            new SammyPackageStructProperties(
//                                self::BRAND,
//                                $sex,
//                                $category_data['name'],
//                                self::PARTNER,
//                                null,
//                                isset($attributes[0]) ? $attributes[0]['product_attribute_description'][self::LANGUAGE_UK]['text'] : null,
//                                null, null, null, null, $sex, null, null, null, null, null, null, null, null, null))
//                    );
//                }
//            }
//        }
//
//        $setProduct = new SammyPackageStructSetProduct(new SammyPackageStructProducts($resProducts));
//        $sammyPackageServiceSet->SetProduct($setProduct);
//        $res = $sammyPackageServiceSet->getResult();
//        $error = $sammyPackageServiceSet->getLastError();
//
//        $res = $sammyPackageServiceGet->GetProduct(new SammyPackageStructGetProduct(new SammyPackageStructFilter('244')));

        $this->load->language('module/soap_api');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (!is_dir('../image/api/')) {
            mkdir('../image/api/', 0777, true);
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('soap_api', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_soap_api_wsdl'] = $this->language->get('entry_soap_api_wsdl');
        $data['entry_soap_api_getproducts'] = $this->language->get('entry_soap_api_getproducts');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/soap_api', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['test'] = $this->url->link('module/soap_api/test', 'token=' . $this->session->data['token'], 'SSL');
        $data['import'] = $this->url->link('module/soap_api/import', 'token=' . $this->session->data['token'], 'SSL');
        $data['action'] = $this->url->link('module/soap_api', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['soap_api_wsdl'])) {
            $data['soap_api_wsdl'] = $this->request->post['soap_api_wsdl'];
        } else {
            $data['soap_api_wsdl'] = $this->config->get('soap_api_wsdl');
        }

        if (isset($this->request->post['soap_api_getproducts'])) {
            $data['soap_api_getproducts'] = $this->request->post['soap_api_getproducts'];
        } else {
            $data['soap_api_getproducts'] = $this->config->get('soap_api_getproducts');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/soap_api.tpl', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/soap_api')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function import()
    {
        if (!$this->user->hasPermission('modify', 'module/soap_api')) {
            return 0;
        }
        $wsdl = $this->request->post['soap_api_wsdl'];
        $function = $this->request->post['soap_api_getproducts'];
        ini_set("soap.wsdl_cache_disabled", "0"); // TODO: disable WSDL cache if required
        ini_set("default_socket_timeout", 600);   // TODO: set appropriate read timeout

        try {
            $options = array(
                'trace' => 1,
                "connection_timeout" => 600,
                'login' => 'SammyIconAPI',
                'password' => 'SammyIcon',
            );

            $client = new SoapClient($wsdl, $options); // conneting to SOAP - TODO: set appropriate connect timeout
            $products = call_user_func(array($client, $function));
        } catch (SoapFault $e) {
            echo "Error \n\r";
            var_dump($client->__getLastRequest());
            //print_r($e);
            //print_r($client);
            exit;
        }

        $products_created = 0;
        $languages = $this->getLanguages();

        foreach ($products as $product) {
            $name = $product['name'];
            $id = $product['product_id'];
            if ($this->getProductId($name, 'product_description', 'name')) {
                continue;
            }
            $p_data = $this->generateProductData($product, $id, $languages);
            $created_id = $this->model_catalog_product->addProduct($p_data);
            if ($created_id) {
                $products_created++;
            }
        }
        echo "Created " . $products_created . " new products.";
    }

    public function test()
    {
        if (!$this->user->hasPermission('modify', 'module/soap_api')) {
            return 0;
        }
        $wsdl = $this->request->post['soap_api_wsdl'];
        $getproducts = $this->request->post['soap_api_getproducts'];
        ini_set("soap.wsdl_cache_disabled", "0"); // TODO: disable WSDL cache if required
        ini_set("default_socket_timeout", 600);   // TODO: set appropriate read timeout

        try {
            $client = new SoapClient($wsdl, array('trace' => 1, "connection_timeout" => 600)); // conneting to SOAP - TODO: set appropriate connect timeout
            call_user_func(array($client, $getproducts));
        } catch (SoapFault $e) {
            echo "Error \n\r";
            var_dump($client->__getLastRequest());
            //print_r($e);
            //print_r($client);
        }

    }

    public function generateProductData($product, $id, $languages)
    {
        $img = '';
        /*if($this->URL_exists($product['image'])){
            $img = 'api/'. $id .'.jpg';
            file_put_contents('../image/'.$img, file_get_contents($product['image']));
        }
        */
        foreach ($languages as $language_id) {
            $product_description[$language_id] = array(
                'name' => $product['name'],
                'description' => $product['description'],
                'meta_title' => $product['name'],
                'meta_description' => $product['name'],
                'meta_keyword' => $product['name'],
                'tag' => $product['name']
            );
        }

        $data = array(
            'product_description' => $product_description,
            'model' => $id,
            'sku' => $id,
            'upc' => '',
            'ean' => '',
            'jan' => '',
            'isbn' => '',
            'mpn' => '',
            'location' => '',
            'price' => $product['price'],
            'tax_class_id' => '0',
            'quantity' => '100',
            'minimum' => '1',
            'subtract' => '1',
            'stock_status_id' => '7',
            'shipping' => '1',
            'date_available' => date('Y-m-d'),
            'length' => '',
            'width' => '',
            'height' => '',
            'length_class_id' => '1',
            'weight' => '',
            'weight_class_id' => '1',
            'status' => '1',
            'sort_order' => '1',
            'manufacturer_id' => '0',
            'product_store' => array(0 => '0'),
//'image' => $img,
            'points' => ''
        );

        return $data;
    }

    public function getProductId($value, $table, $identifier = 'name')
    {

        $query = $this->db->query("SELECT  product_id FROM `" . DB_PREFIX . $table . "` WHERE `" . $identifier . "` = '" . $value . "'");

        if ($query->num_rows) {
            return $query->row['product_id'];
        } else {
            return 0;
        }
    }

    public function getLanguages()
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "language WHERE status = '1'");
        foreach ($query->rows as $row) {
            $laguanges[] = $row['language_id'];
        }
        return $laguanges;
    }
}