<?php
class ControllerModuleTltMultilang extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/tltmultilang');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('localisation/language');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('tltmultilang', $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_copyright'] = '&copy; 2016, <a href="http://taiwanleaftea.com" target="_blank" class="alert-link" title="Authentic tea from Taiwan">Taiwanleaftea.com</a>';
		$data['text_donation'] = 'If you find this software usefull and to support further development please buy me a cup of <a href="http://taiwanleaftea.com" class="alert-link" target="_blank" title="Authentic tea from Taiwan">tea</a> using this <a href="https://www.paypal.me/AMamykin/5" class="alert-link" target="_blank" title="Paypal me">link</a>.';

		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/tltmultilang', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('module/tltmultilang', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], true);

		$this->load->model('localisation/language');
		$results = $this->model_localisation_language->getLanguages();
		
		$data['tltmultilang_meta_title'] = array();
		$data['tltmultilang_meta_description'] = array();
		$data['tltmultilang_meta_keyword'] = array();
		$data['languages'] = array();
		
		foreach ($results as $result) {
			$data['languages'][] = array(
				'name'  => $result['name'],
				'code'  => $result['code']
			);
	
			if (isset($this->request->post['tltmultilang_meta_title_' . $result['code']])) {
				$data['tltmultilang_meta_title'][$result['code']] = $this->request->post['tltmultilang_meta_title_' . $result['code']];
			} else {
				$data['tltmultilang_meta_title'][$result['code']] = $this->config->get('tltmultilang_meta_title_' . $result['code']);
			}
	
			if (isset($this->request->post['tltmultilang_meta_description_' . $result['code']])) {
				$data['tltmultilang_meta_description'][$result['code']] = $this->request->post['tltmultilang_meta_description_' . $result['code']];
			} else {
				$data['tltmultilang_meta_description'][$result['code']] = $this->config->get('tltmultilang_meta_description_' . $result['code']);
			}
	
			if (isset($this->request->post['tltmultilang_meta_keyword_' . $result['code']])) {
				$data['tltmultilang_meta_keyword'][$result['code']] = $this->request->post['tltmultilang_meta_keyword_' . $result['code']];
			} else {
				$data['tltmultilang_meta_keyword'][$result['code']] = $this->config->get('tltmultilang_meta_keyword_' . $result['code']);
			}
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/tltmultilang', $data));
	}

	protected function validate() {
		$this->load->model('localisation/language');

		if (!$this->user->hasPermission('modify', 'module/tltmultilang')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$results = $this->model_localisation_language->getLanguages();
		
		foreach ($results as $result) {
			if ((utf8_strlen($this->request->post['tltmultilang_meta_title_' . $result['code']]) < 3) || (utf8_strlen($this->request->post['tltmultilang_meta_title_' . $result['code']]) > 70)) {
				$this->error['meta_title'][$result['code']] = $this->language->get('error_meta_title');
			}
		}

		return !$this->error;
	}

    public function install() {
		$this->load->model('setting/setting');
		$this->load->model('localisation/language');
		
		$metas = array();
		$meta_title = $this->config->get('config_meta_title');
		$meta_description = $this->config->get('config_meta_description');
		$meta_keyword = $this->config->get('config_meta_keyword');
		
		$results = $this->model_localisation_language->getLanguages();

		foreach ($results as $result) {
			$metas['tltmultilang_meta_title_' . $result['code']] = $meta_title;
			$metas['tltmultilang_meta_description_' . $result['code']] = $meta_description;
			$metas['tltmultilang_meta_keyword_' . $result['code']] = $meta_keyword;
		}

		$this->model_setting_setting->editSetting('tltmultilang', $metas);
    }

    public function uninstall() {
		$this->load->model('setting/setting');
		$this->model_setting_setting->deleteSetting('tltmultilang');
    }
}