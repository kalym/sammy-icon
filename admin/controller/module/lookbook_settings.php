<?php
class ControllerModuleLookbookSettings extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/lookbook_settings');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('localisation/language');
		$this->load->model('lookbook/url_alias');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('lookbook', $this->request->post);

			$this->model_lookbook_url_alias->deleteUrlAlias('lookbookpath=');

			$this->model_lookbook_url_alias->saveUrlAlias($this->db->escape($this->request->post['lookbook_path']), 'lookbookpath=' . $this->db->escape($this->request->post['lookbook_path']));
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_copyright'] = '&copy; 2016, <a href="http://taiwanleaftea.com" target="_blank" class="alert-link" title="Authentic tea from Taiwan">Taiwanleaftea.com</a>';
		$data['text_donation'] = 'If you find this software usefull and to support further development please buy me a cup of <a href="http://taiwanleaftea.com" class="alert-link" target="_blank" title="Authentic tea from Taiwan">tea</a> or like us on <a href="https://www.facebook.com/taiwanleaftea" class="alert-link" target="_blank" title="Taiwanleaftea on Facebook">Facebook</a>. You can also buy <a href="http://www.opencart.com/index.php?route=extension/extension/info&extension_id=25345" class="alert-link" target="_blank" title="lookbook Blog Pro">lookbook Blog Pro</a>, which is a professional version of this extension.';

		$data['entry_path'] = $this->language->get('entry_path');
		$data['entry_path_title'] = $this->language->get('entry_path_title');
		$data['entry_show_path'] = $this->language->get('entry_show_path');
		$data['entry_num_columns'] = $this->language->get('entry_num_columns');
		$data['entry_show_image'] = $this->language->get('entry_show_image');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_seo'] = $this->language->get('entry_seo');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['help_path'] = $this->language->get('help_path');
		$data['help_path_title'] = $this->language->get('help_path_title');
		$data['help_show_path'] = $this->language->get('help_show_path');
		$data['help_show_image'] = $this->language->get('help_show_image');
		$data['help_num_columns'] = $this->language->get('help_num_columns');
		$data['help_seo'] = $this->language->get('help_seo');
		$data['help_status'] = $this->language->get('help_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['error_seo_disabled'] = $this->language->get('error_seo_disabled');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['lookbook_path'])) {
			$data['error_path'] = $this->error['lookbook_path'];
		} else {
			$data['error_path'] = '';
		}

		if (isset($this->error['lookbook_path_title'])) {
			$data['error_path_title'] = $this->error['lookbook_path_title'];
		} else {
			$data['error_path_title'] = array();
		}

		if (isset($this->error['lookbook_width'])) {
			$data['error_width'] = $this->error['lookbook_width'];
		} else {
			$data['error_width'] = '';
		}

		if (isset($this->error['lookbook_height'])) {
			$data['error_height'] = $this->error['lookbook_height'];
		} else {
			$data['error_height'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/lookbook_settings', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('module/lookbook_settings', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], true);

		if (isset($this->request->post['lookbook_status'])) {
			$data['lookbook_status'] = $this->request->post['lookbook_status'];
		} else {
			$data['lookbook_status'] = $this->config->get('lookbook_status');
		}

		if (isset($this->request->post['lookbook_path'])) {
			$data['lookbook_path'] = preg_replace('/\s/', '', $this->request->post['lookbook_path']);
		} elseif ($this->config->has('lookbook_path')) {
			$data['lookbook_path'] = $this->config->get('lookbook_path');
		} else {
			$data['lookbook_path'] = 'blogs';
		}

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['lookbook_path_title'])) {
			$data['lookbook_path_title'] = $this->request->post['lookbook_path_title'];
		} elseif ($this->config->has('lookbook_path_title')) {
			$data['lookbook_path_title'] = $this->config->get('lookbook_path_title');
		} else {
			$data['lookbook_path_title'] = array();
		}

		if (isset($this->request->post['lookbook_show_path'])) {
			$data['lookbook_show_path'] = $this->request->post['lookbook_show_path'];
		} elseif ($this->config->has('lookbook_show_path')) {
			$data['lookbook_show_path'] = $this->config->get('lookbook_show_path');
		} else {
			$data['lookbook_show_path'] = '1';
		}

		if (isset($this->request->post['lookbook_num_columns'])) {
			$data['lookbook_num_columns'] = $this->request->post['lookbook_num_columns'];
		} elseif ($this->config->has('lookbook_num_columns')) {
			$data['lookbook_num_columns'] = $this->config->get('lookbook_num_columns');
		} else {
			$data['lookbook_num_columns'] = '1';
		}

		if (isset($this->request->post['lookbook_show_image'])) {
			$data['lookbook_show_image'] = $this->request->post['lookbook_show_image'];
		} elseif ($this->config->has('lookbook_show_image')) {
			$data['lookbook_show_image'] = $this->config->get('lookbook_show_image');
		} else {
			$data['lookbook_show_image'] = '1';
		}

		if (isset($this->request->post['lookbook_width'])) {
			$data['lookbook_width'] = $this->request->post['lookbook_width'];
		} elseif ($this->config->has('lookbook_width')) {
			$data['lookbook_width'] = (int)$this->config->get('lookbook_width');
		} else {
			$data['lookbook_width'] = 200;
		}

		if (isset($this->request->post['lookbook_height'])) {
			$data['lookbook_height'] = $this->request->post['lookbook_height'];
		} elseif ($this->config->has('lookbook_height')) {
			$data['lookbook_height'] = (int)$this->config->get('lookbook_height');
		} else {
			$data['lookbook_height'] = 200;
		}

		if (isset($this->request->post['lookbook_seo'])) {
			$data['lookbook_seo'] = $this->request->post['lookbook_seo'];
		} else {
			$data['lookbook_seo'] = $this->config->get('lookbook_seo');
		}

		// If you have non-standard SEO module, which doesn't use config_seo_url setting simple replace this if ... else contruction with 
		// $data['lookbook_global_seo_off'] = false;
		// <--begin-->
		if ($this->config->get('config_seo_url')) {
			$data['global_seo_off'] = false;
		} else {
			$data['global_seo_off'] = true;
		} 
		// <---end--->

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/lookbook_settings', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/lookbook_settings')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['lookbook_path']) < 3) || (utf8_strlen($this->request->post['lookbook_path']) > 64)) {
			$this->error['lookbook_path'] = $this->language->get('error_path');
		}

		$this->load->model('lookbook/url_alias');

		if (!$this->model_lookbook_url_alias->checkUrlAliasIsFree($this->request->post['lookbook_path'], 'lookbookpath=' . $this->request->post['lookbook_path'])) {
			$this->error['lookbook_path'] = sprintf($this->language->get('error_path_exist'));
		}

		if ($this->request->post['lookbook_show_path']) {
			foreach ($this->request->post['lookbook_path_title'] as $language_id => $value) {
				if ((utf8_strlen($value['path_title']) < 3) || (utf8_strlen($value['path_title']) > 64)) {
					$this->error['lookbook_path_title'][$language_id] = $this->language->get('error_path_title');
				}
			}
		}

		if ($this->request->post['lookbook_show_image']) {
			if (!$this->request->post['lookbook_width'] || ((int)$this->request->post['lookbook_width'] < 1)) {
				$this->error['lookbook_width'] = $this->language->get('error_width');
			}
	
			if (!$this->request->post['lookbook_height'] || ((int)$this->request->post['lookbook_height'] < 1)) {
				$this->error['lookbook_height'] = $this->language->get('error_height');
			}
		}

		return !$this->error;
	}

    public function uninstall() {
		$this->load->model('setting/setting');
		$this->model_setting_setting->deleteSetting('lookbook');
		$this->cache->delete('lookbook');
    }
}