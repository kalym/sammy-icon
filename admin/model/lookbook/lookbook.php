<?php

class ModelLookbookLookbook extends Model
{
    public function addLookbook($data)
    {
        $this->event->trigger('pre.admin.lookbook.add', $data);

        $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook SET sort_order = '" . (int)$data['sort_order'] . "', image = '" . (isset($data['image']) ? $this->db->escape($data['image']) : '') . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', show_title = '" . (isset($data['show_title']) ? (int)$data['show_title'] : 0) . "', status = '" . (int)$data['status'] . "', show_description = '" . (isset($data['show_description']) ? (int)$data['show_description'] : 0) . "'");

        $lookbook_id = $this->db->getLastId();

        foreach ($data['lookbook_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_description SET lookbook_id = '" . (int)$lookbook_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', intro = '" . $this->db->escape($value['intro']) ."', article_pt1 = '" . $this->db->escape($value['article_pt1'])   . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        if (isset($data['lookbook_related'])) {
            foreach ($data['lookbook_related'] as $related_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_related SET lookbook_id = '" . (int)$lookbook_id . "', related_id = '" . (int)$related_id . "'");
            }
        }

        if (isset($data['lookbook_tags'])) {
            foreach ($data['lookbook_tags'] as $lookbooktag_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_to_tag SET lookbook_id = '" . (int)$lookbook_id . "', lookbooktag_id = '" . (int)$lookbooktag_id . "'");
            }
        }

        if (isset($data['lookbook_store'])) {
            foreach ($data['lookbook_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_to_store SET lookbook_id = '" . (int)$lookbook_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['lookbook_layout'])) {
            foreach ($data['lookbook_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_to_layout SET lookbook_id = '" . (int)$lookbook_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        if (isset($data['keyword'])) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_url_alias SET query = 'lookbook_id=" . (int)$lookbook_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('lookbook');

        $this->event->trigger('post.admin.lookbook.add', $lookbook_id);

        return $lookbook_id;
    }

    public function editLookbook($lookbook_id, $data)
    {
        $this->event->trigger('pre.admin.lookbook.edit', $data);

        $this->db->query("UPDATE " . DB_PREFIX . "lookbook SET sort_order = '" . (int)$data['sort_order'] . "', image = '" . (isset($data['image']) ? $this->db->escape($data['image']) : '') . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', show_title = '" . (isset($data['show_title']) ? (int)$data['show_title'] : 0) . "', status = '" . (int)$data['status'] .  "', banner_id = '" . (int)$data['banner_id'] ."', show_description = '" . (isset($data['show_description']) ? (int)$data['show_description'] : 0) . "' WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_description WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        foreach ($data['lookbook_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_description SET lookbook_id = '" . (int)$lookbook_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', intro = '" . $this->db->escape($value['intro']) . "', article_pt1 = '" . $this->db->escape($value['article_pt1'])   ."', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_related WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        if (isset($data['lookbook_related'])) {
            foreach ($data['lookbook_related'] as $related_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_related SET lookbook_id = '" . (int)$lookbook_id . "', related_id = '" . (int)$related_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_to_tag WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        if (isset($data['lookbook_tags'])) {
            foreach ($data['lookbook_tags'] as $lookbooktag_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_to_tag SET lookbook_id = '" . (int)$lookbook_id . "', lookbooktag_id = '" . (int)$lookbooktag_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_to_store WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        if (isset($data['lookbook_store'])) {
            foreach ($data['lookbook_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_to_store SET lookbook_id = '" . (int)$lookbook_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_to_layout WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        if (isset($data['lookbook_layout'])) {
            foreach ($data['lookbook_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_to_layout SET lookbook_id = '" . (int)$lookbook_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_url_alias WHERE query = 'lookbook_id=" . (int)$lookbook_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_url_alias SET query = 'lookbook_id=" . (int)$lookbook_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('lookbook');

        $this->event->trigger('post.admin.lookbook.edit', $lookbook_id);
    }

    public function copyLookbook($lookbook_id)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) WHERE b.lookbook_id = '" . (int)$lookbook_id . "' AND bd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
        if ($query->num_rows) {
            $data = $query->row;

            $data['keyword'] = '';
            $data['status'] = '0';

            $lookbook_descriptions = $this->getLookbookDescription($lookbook_id);

            foreach ($lookbook_descriptions as $result) {
                $lookbook_description_data[$result['language_id']] = array(
                    'language_id' => $result['language_id'],
                    'title' => (strlen('Copy of ' . $result['title']) < 255 ? 'Copy of ' . $result['title'] : $result['title']),
                    'intro' => $result['intro'],
                    'article_pt1' => $result['article_pt1'],
                    'meta_title' => $result['meta_title'],
                    'meta_description' => $result['meta_description'],
                    'meta_keyword' => $result['meta_keyword'],
                );
            }

            $data['lookbook_description'] = $lookbook_description_data;
            $data['lookbook_related'] = $this->getLookbookRelated($lookbook_id);
            $data['lookbook_layout'] = $this->getLookbookLayouts($lookbook_id);
            $data['lookbook_store'] = $this->getLookbookStores($lookbook_id);
            $data['lookbook_tags'] = $this->getLookbookTags($lookbook_id);

            $this->addlookbook($data);
        }
    }

    public function deleteLookbook($lookbook_id)
    {
        $this->event->trigger('pre.admin.lookbook.delete', $lookbook_id);

        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook WHERE lookbook_id = '" . (int)$lookbook_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_description WHERE lookbook_id = '" . (int)$lookbook_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_related WHERE lookbook_id = '" . (int)$lookbook_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_to_store WHERE lookbook_id = '" . (int)$lookbook_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_to_tag WHERE lookbook_id = '" . (int)$lookbook_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_to_layout WHERE lookbook_id = '" . (int)$lookbook_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_url_alias WHERE query = 'lookbook_id=" . (int)$lookbook_id . "'");

        $this->cache->delete('lookbook');

        $this->event->trigger('post.admin.lookbook.delete', $lookbook_id);
    }

    public function getLookbook($lookbook_id)
    {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "lookbook_url_alias WHERE query = 'lookbook_id=" . (int)$lookbook_id . "') AS keyword FROM " . DB_PREFIX . "lookbook WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        return $query->row;
    }

    public function getLookbooks($data = array())
    {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

            if (!empty($data['filter_title'])) {
                $sql .= " AND bd.title LIKE '%" . $this->db->escape($data['filter_title']) . "%'";
            }

            if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
                $sql .= " AND b.status = '" . (int)$data['filter_status'] . "'";
            }

            $sort_data = array(
                'bd.title',
                'b.status',
                'b.sort_order'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY bd.title";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $lookbook_data = $this->cache->get('lookbook.' . (int)$this->config->get('config_language_id'));

            if (!$lookbook_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook b LEFT JOIN " . DB_PREFIX . "lookbook_description bd ON (b.lookbook_id = bd.lookbook_id) WHERE bd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY bd.title");

                $lookbook_data = $query->rows;

                $this->cache->set('lookbook.' . (int)$this->config->get('config_language_id'), $lookbook_data);
            }

            return $lookbook_data;
        }
    }

    public function getLookbookDescription($lookbook_id)
    {
        $lookbook_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook_description WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        foreach ($query->rows as $result) {
            $lookbook_description_data[$result['language_id']] = array(
                'language_id' => $result['language_id'],
                'title' => $result['title'],
                'intro' => $result['intro'],
                'article_pt1' => $result['article_pt1'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
            );
        }

        return $lookbook_description_data;
    }


    public function getLookbookRelated($lookbook_id)
    {
        $lookbook_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook_related WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        foreach ($query->rows as $result) {
            $lookbook_related_data[] = $result['related_id'];
        }

        return $lookbook_related_data;
    }

    public function getLookbookTags($lookbook_id)
    {
        $lookbook_tags_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook_to_tag WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        foreach ($query->rows as $result) {
            $lookbook_tags_data[] = $result['lookbooktag_id'];
        }

        return $lookbook_tags_data;
    }

    public function getLookbookTagsDescription($lookbook_id)
    {
        $lookbook_tags_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbooktag_description td LEFT JOIN " . DB_PREFIX . "lookbook_to_tag btt ON (td.lookbooktag_id = btt.lookbooktag_id) WHERE lookbook_id = '" . (int)$lookbook_id . "' AND td.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY td.title ASC");

        foreach ($query->rows as $result) {
            $lookbook_tags_data[] = array(
                'lookbooktag_id' => $result['lookbooktag_id'],
                'title' => $result['title']
            );
        }

        return $lookbook_tags_data;
    }

    public function getLookbookStores($lookbook_id)
    {
        $lookbook_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook_to_store WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        foreach ($query->rows as $result) {
            $lookbook_store_data[] = $result['store_id'];
        }

        return $lookbook_store_data;
    }

    public function getLookbookLayouts($lookbook_id)
    {
        $lookbook_layout_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbook_to_layout WHERE lookbook_id = '" . (int)$lookbook_id . "'");

        foreach ($query->rows as $result) {
            $lookbook_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $lookbook_layout_data;
    }

    public function getTotalLookbooks()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "lookbook");

        return $query->row['total'];
    }

    public function getTotalLookbooksByLayoutId($layout_id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "lookbook_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

        return $query->row['total'];
    }





}