<?php
class ModelLookbookLookbookTag extends Model {
	public function addLookbookTag($data) {
		$this->event->trigger('pre.admin.lookbooktag.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "lookbooktag SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

		$lookbooktag_id = $this->db->getLastId();

		foreach ($data['lookbooktag_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "lookbooktag_description SET lookbooktag_id = '" . (int)$lookbooktag_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if (isset($data['lookbooktag_store'])) {
			foreach ($data['lookbooktag_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "lookbooktag_to_store SET lookbooktag_id = '" . (int)$lookbooktag_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_url_alias SET query = 'lookbooktag_id=" . (int)$lookbooktag_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('lookbook');

		$this->event->trigger('post.admin.lookbooktag.add', $lookbooktag_id);

		return $lookbooktag_id;
	}

	public function editLookbooktag($lookbooktag_id, $data) {
		$this->event->trigger('pre.admin.lookbooktag.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "lookbooktag SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE lookbooktag_id = '" . (int)$lookbooktag_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "lookbooktag_description WHERE lookbooktag_id = '" . (int)$lookbooktag_id . "'");

		foreach ($data['lookbooktag_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "lookbooktag_description SET lookbooktag_id = '" . (int)$lookbooktag_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "lookbooktag_to_store WHERE lookbooktag_id = '" . (int)$lookbooktag_id . "'");

		if (isset($data['lookbooktag_store'])) {
			foreach ($data['lookbooktag_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "lookbooktag_to_store SET lookbooktag_id = '" . (int)$lookbooktag_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_url_alias WHERE query = 'lookbooktag_id=" . (int)$lookbooktag_id . "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "lookbook_url_alias SET query = 'lookbooktag_id=" . (int)$lookbooktag_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('lookbook');

		$this->event->trigger('post.admin.lookbooktag.edit', $lookbooktag_id);
	}

	public function copyLookbooktag($lookbooktag_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "lookbooktag b LEFT JOIN " . DB_PREFIX . "lookbooktag_description bd ON (b.lookbooktag_id = bd.lookbooktag_id) WHERE b.lookbooktag_id = '" . (int)$lookbooktag_id . "' AND bd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		if ($query->num_rows) {
			$data = $query->row;

			$data['keyword'] = '';
			$data['status'] = '0';

			$lookbooktag_descriptions = $this->getLookbooktagDescription($lookbooktag_id);
			
			foreach ($lookbooktag_descriptions as $result) {
				$lookbooktag_description_data[$result['language_id']] = array(
					'language_id'	   => $result['language_id'],
					'title'            => (strlen('Copy of ' . $result['title']) < 255 ? 'Copy of ' . $result['title'] : $result['title']),
					'meta_title'       => $result['meta_title'],
					'meta_description' => $result['meta_description'],
					'meta_keyword'     => $result['meta_keyword'],
				);
			}

			$data['lookbooktag_description'] = $lookbooktag_description_data;
			$data['lookbooktag_store'] = $this->getLookbooktagStores($lookbooktag_id);

			$this->addLookbooktag($data);
		}
	}

	public function deleteLookbooktag($lookbooktag_id) {
		$this->event->trigger('pre.admin.lookbooktag.delete', $lookbooktag_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "lookbooktag WHERE lookbooktag_id = '" . (int)$lookbooktag_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "lookbooktag_description WHERE lookbooktag_id = '" . (int)$lookbooktag_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_to_tag WHERE lookbooktag_id = '" . (int)$lookbooktag_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "lookbooktag_to_store WHERE lookbooktag_id = '" . (int)$lookbooktag_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "lookbook_url_alias WHERE query = 'lookbooktag_id=" . (int)$lookbooktag_id . "'");

		$this->cache->delete('lookbook');

		$this->event->trigger('post.admin.lookbooktag.delete', $lookbooktag_id);
	}

	public function getLookbooktag($lookbooktag_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "lookbook_url_alias WHERE query = 'lookbooktag_id=" . (int)$lookbooktag_id . "') AS keyword FROM " . DB_PREFIX . "lookbooktag t LEFT JOIN " . DB_PREFIX . "lookbooktag_description td ON (t.lookbooktag_id = td.lookbooktag_id) WHERE t.lookbooktag_id = '" . (int)$lookbooktag_id . "' AND td.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getLookbooktags($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "lookbooktag t LEFT JOIN " . DB_PREFIX . "lookbooktag_description td ON (t.lookbooktag_id = td.lookbooktag_id) WHERE td.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			if (!empty($data['filter_title'])) {
				$sql .= " AND td.title LIKE '%" . $this->db->escape($data['filter_title']) . "%'";
			}

			if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
				$sql .= " AND t.status = '" . (int)$data['filter_status'] . "'";
			}

			$sort_data = array(
				'td.title',
				't.status',
				't.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY td.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$lookbooktag_data = $this->cache->get('lookbook.alllookbooktags.' . (int)$this->config->get('config_language_id'));

			if (!$lookbooktag_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbooktag t LEFT JOIN " . DB_PREFIX . "lookbooktag_description td ON (t.lookbooktag_id = td.lookbooktag_id) WHERE td.language_id = '" . (int)$this->config->get('config_language_id') . "' AND t.status = '1' ORDER BY td.title");

				$lookbooktag_data = $query->rows;

				$this->cache->set('lookbook.alllookbooktags.' . (int)$this->config->get('config_language_id'), $lookbooktag_data);
			}

			return $lookbooktag_data;
		}
	}

	public function getLookbooktagTagDescription($lookbooktag_id) {
		$lookbooktag_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbooktag_description WHERE lookbooktag_id = '" . (int)$lookbooktag_id . "'");

		foreach ($query->rows as $result) {
			$lookbooktag_description_data[$result['language_id']] = array(
				'lookbooktag_id'		   => $result['lookbooktag_id'],
				'language_id'      => $result['language_id'],
				'title'            => $result['title'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
			);
		}

		return $lookbooktag_description_data;
	}

	public function getLookbooktagsForBlog($lookbook_id) {
		$query = $this->db->query("SELECT t.lookbooktag_id AS lookbooktag_id, td.title AS title FROM  " . DB_PREFIX . "lookbook_to_tag b2t LEFT JOIN  " . DB_PREFIX . "lookbooktag t ON (b2t.lookbooktag_id = t.lookbooktag_id) LEFT JOIN  " . DB_PREFIX . "lookbooktag_description td ON (t.lookbooktag_id = td.lookbooktag_id) LEFT JOIN  " . DB_PREFIX . "lookbooktag_to_store t2s ON (t.lookbooktag_id = t2s.lookbooktag_id) WHERE b2t.lookbook_id = '" . (int)$lookbook_id . "' AND t.status = '1' AND td.language_id = '" . (int)$this->config->get('config_language_id') . "' AND t2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY t.sort_order, LCASE(td.title) ASC");

		return $query->rows;
	}

	public function getLookbooktagStores($lookbooktag_id) {
		$lookbooktag_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookbooktag_to_store WHERE lookbooktag_id = '" . (int)$lookbooktag_id . "'");

		foreach ($query->rows as $result) {
			$lookbooktag_store_data[] = $result['store_id'];
		}

		return $lookbooktag_store_data;
	}

	public function getTotalLookbooktags() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "lookbooktag");

		return $query->row['total'];
	}
}