<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="button" onclick="$('#form-soap-api').attr('action', '<?php echo $action; ?>').attr('target', '_self').submit()" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-soap-api" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-soap-api-wsdl"><?php echo $entry_soap_api_wsdl; ?></label>
            <div class="col-sm-10">
              <input type="text" name="soap_api_wsdl" value="<?php echo $soap_api_wsdl; ?>" placeholder="<?php echo $entry_soap_api_wsdl; ?>" id="input-soap-api-wsdl" class="form-control" />
            </div>
          </div>
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-soap-api-getproducts"><?php echo $entry_soap_api_getproducts; ?></label>
            <div class="col-sm-10">
              <input type="text" name="soap_api_getproducts" value="<?php echo $soap_api_getproducts; ?>" placeholder="<?php echo $entry_soap_api_getproducts; ?>" id="input-soap-api-getproducts" class="form-control" />
            </div>
          </div>
        </form>
      </div>
	    <div class="pull-right">
        <button type="button" data-toggle="tooltip" title="Test" class="btn btn-default" onclick="$('#form-soap-api').attr('action', '<?php echo $test; ?>').attr('target', '_blank').submit()">Test</button>
        <button type="button" data-toggle="tooltip" title="Import" class="btn btn-default" onclick="$('#form-soap-api').attr('action', '<?php echo $import; ?>').attr('target', '_blank').submit()">Import</button>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>