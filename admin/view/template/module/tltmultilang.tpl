<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-html" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
	<div class="alert alert-info">
    		<i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_donation; ?><br />
  			<?php echo $text_copyright; ?>
  	</div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-html" class="form-horizontal">
		  <?php foreach ($languages as $language) { ?>		
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-meta-title-<?php echo $language['code']; ?>"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" />&nbsp;<?php echo $entry_meta_title; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="tltmultilang_meta_title_<?php echo $language['code']; ?>" value="<?php echo isset($tltmultilang_meta_title[$language['code']]) ? $tltmultilang_meta_title[$language['code']] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title-<?php echo $language['code']; ?>" class="form-control" />
                 <?php if (isset($error_meta_title[$language['code']])) { ?>
                 <div class="text-danger"><?php echo $error_meta_title[$language['code']]; ?></div>
                 <?php } ?>
                </div>
              </div> 
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-meta-description-<?php echo $language['code']; ?>"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" />&nbsp;<?php echo $entry_meta_description; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="tltmultilang_meta_description_<?php echo $language['code']; ?>" value="<?php echo isset($tltmultilang_meta_description[$language['code']]) ? $tltmultilang_meta_description[$language['code']] : ''; ?>" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description-<?php echo $language['code']; ?>" class="form-control" />
                </div>
              </div> 
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-meta-keyword-<?php echo $language['code']; ?>"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" />&nbsp;<?php echo $entry_meta_keyword; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="tltmultilang_meta_keyword_<?php echo $language['code']; ?>" value="<?php echo isset($tltmultilang_meta_keyword[$language['code']]) ? $tltmultilang_meta_keyword[$language['code']] : ''; ?>" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword-<?php echo $language['code']; ?>" class="form-control" />
                </div>
              </div> 
          <?php } ?>        
        </form>
      </div>
    </div>
  </div>
<?php echo $footer; ?>