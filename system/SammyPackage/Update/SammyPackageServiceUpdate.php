<?php
/**
 * File for class SammyPackageServiceUpdate
 * @package SammyPackage
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageServiceUpdate originally named Update
 * @package SammyPackage
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageServiceUpdate extends SammyPackageWsdlClass
{
    /**
     * Method to call the operation originally named UpdateOrders
     * @uses SammyPackageWsdlClass::getSoapClient()
     * @uses SammyPackageWsdlClass::setResult()
     * @uses SammyPackageWsdlClass::saveLastError()
     * @param SammyPackageStructUpdateOrders $_sammyPackageStructUpdateOrders
     * @return SammyPackageStructUpdateOrdersResponse
     */
    public function UpdateOrders(SammyPackageStructUpdateOrders $_sammyPackageStructUpdateOrders)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->UpdateOrders($_sammyPackageStructUpdateOrders));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see SammyPackageWsdlClass::getResult()
     * @return SammyPackageStructUpdateOrdersResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
