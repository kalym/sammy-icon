<?php
/**
 * File for class SammyPackageStructUpdateOrders
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructUpdateOrders originally named UpdateOrders
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructUpdateOrders extends SammyPackageWsdlClass
{
    /**
     * The OrderId
     * @var string
     */
    public $OrderId;
    /**
     * The ShipmentID
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $ShipmentID;
    /**
     * The Status
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var boolean
     */
    public $Status;
    /**
     * Constructor method for UpdateOrders
     * @see parent::__construct()
     * @param string $_orderId
     * @param string $_shipmentID
     * @param boolean $_status
     * @return SammyPackageStructUpdateOrders
     */
    public function __construct($_orderId = NULL,$_shipmentID = NULL,$_status = NULL)
    {
        parent::__construct(array('OrderId'=>$_orderId,'ShipmentID'=>$_shipmentID,'Status'=>$_status),false);
    }
    /**
     * Get OrderId value
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->OrderId;
    }
    /**
     * Set OrderId value
     * @param string $_orderId the OrderId
     * @return string
     */
    public function setOrderId($_orderId)
    {
        return ($this->OrderId = $_orderId);
    }
    /**
     * Get ShipmentID value
     * @return string|null
     */
    public function getShipmentID()
    {
        return $this->ShipmentID;
    }
    /**
     * Set ShipmentID value
     * @param string $_shipmentID the ShipmentID
     * @return string
     */
    public function setShipmentID($_shipmentID)
    {
        return ($this->ShipmentID = $_shipmentID);
    }
    /**
     * Get Status value
     * @return boolean|null
     */
    public function getStatus()
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @param boolean $_status the Status
     * @return boolean
     */
    public function setStatus($_status)
    {
        return ($this->Status = $_status);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructUpdateOrders
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
