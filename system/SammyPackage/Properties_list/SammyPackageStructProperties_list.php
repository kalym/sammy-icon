<?php
/**
 * File for class SammyPackageStructProperties_list
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructProperties_list originally named Properties_list
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructProperties_list extends SammyPackageWsdlClass
{
    /**
     * The properties
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * @var SammyPackageStructItem_properties
     */
    public $properties;
    /**
     * Constructor method for Properties_list
     * @see parent::__construct()
     * @param SammyPackageStructItem_properties $_properties
     * @return SammyPackageStructProperties_list
     */
    public function __construct($_properties = NULL)
    {
        parent::__construct(array('properties'=>$_properties),false);
    }
    /**
     * Get properties value
     * @return SammyPackageStructItem_properties|null
     */
    public function getProperties()
    {
        return $this->properties;
    }
    /**
     * Set properties value
     * @param SammyPackageStructItem_properties $_properties the properties
     * @return SammyPackageStructItem_properties
     */
    public function setProperties($_properties)
    {
        return ($this->properties = $_properties);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructProperties_list
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
