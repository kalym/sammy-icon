<?php
/**
 * File for class SammyPackageStructProperties
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructProperties originally named Properties
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructProperties extends SammyPackageWsdlClass
{
    /**
     * The brand
     * @var string
     */
    public $brand;
    /**
     * The gender
     * @var string
     */
    public $gender;
    /**
     * The productCategory
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $productCategory;
    /**
     * The marketplacePartner
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $marketplacePartner;
    /**
     * The warehouse
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $warehouse;
    /**
     * The structure
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $structure;
    /**
     * The additionalCharacteristics
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $additionalCharacteristics;
    /**
     * The material
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $material;
    /**
     * The type
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $type;
    /**
     * The heel
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $heel;
    /**
     * The sex
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $sex;
    /**
     * The year
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $year;
    /**
     * The season
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $season;
    /**
     * The season_by_years
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $season_by_years;
    /**
     * The style
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $style;
    /**
     * The country
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $country;
    /**
     * The technology
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $technology;
    /**
     * The color
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $color;
    /**
     * The soleMaterial
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $soleMaterial;
    /**
     * The liningMaterial
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $liningMaterial;
    /**
     * Constructor method for Properties
     * @see parent::__construct()
     * @param string $_brand
     * @param string $_gender
     * @param string $_productCategory
     * @param string $_marketplacePartner
     * @param string $_warehouse
     * @param string $_structure
     * @param string $_additionalCharacteristics
     * @param string $_material
     * @param string $_type
     * @param string $_heel
     * @param string $_sex
     * @param string $_year
     * @param string $_season
     * @param string $_season_by_years
     * @param string $_style
     * @param string $_country
     * @param string $_technology
     * @param string $_color
     * @param string $_soleMaterial
     * @param string $_liningMaterial
     * @return SammyPackageStructProperties
     */
    public function __construct($_brand = NULL,$_gender = NULL,$_productCategory = NULL,$_marketplacePartner = NULL,$_warehouse = NULL,$_structure = NULL,$_additionalCharacteristics = NULL,$_material = NULL,$_type = NULL,$_heel = NULL,$_sex = NULL,$_year = NULL,$_season = NULL,$_season_by_years = NULL,$_style = NULL,$_country = NULL,$_technology = NULL,$_color = NULL,$_soleMaterial = NULL,$_liningMaterial = NULL)
    {
        parent::__construct(array('brand'=>$_brand,'gender'=>$_gender,'productCategory'=>$_productCategory,'marketplacePartner'=>$_marketplacePartner,'warehouse'=>$_warehouse,'structure'=>$_structure,'additionalCharacteristics'=>$_additionalCharacteristics,'material'=>$_material,'type'=>$_type,'heel'=>$_heel,'sex'=>$_sex,'year'=>$_year,'season'=>$_season,'season_by_years'=>$_season_by_years,'style'=>$_style,'country'=>$_country,'technology'=>$_technology,'color'=>$_color,'soleMaterial'=>$_soleMaterial,'liningMaterial'=>$_liningMaterial),false);
    }
    /**
     * Get brand value
     * @return string|null
     */
    public function getBrand()
    {
        return $this->brand;
    }
    /**
     * Set brand value
     * @param string $_brand the brand
     * @return string
     */
    public function setBrand($_brand)
    {
        return ($this->brand = $_brand);
    }
    /**
     * Get gender value
     * @return string|null
     */
    public function getGender()
    {
        return $this->gender;
    }
    /**
     * Set gender value
     * @param string $_gender the gender
     * @return string
     */
    public function setGender($_gender)
    {
        return ($this->gender = $_gender);
    }
    /**
     * Get productCategory value
     * @return string|null
     */
    public function getProductCategory()
    {
        return $this->productCategory;
    }
    /**
     * Set productCategory value
     * @param string $_productCategory the productCategory
     * @return string
     */
    public function setProductCategory($_productCategory)
    {
        return ($this->productCategory = $_productCategory);
    }
    /**
     * Get marketplacePartner value
     * @return string|null
     */
    public function getMarketplacePartner()
    {
        return $this->marketplacePartner;
    }
    /**
     * Set marketplacePartner value
     * @param string $_marketplacePartner the marketplacePartner
     * @return string
     */
    public function setMarketplacePartner($_marketplacePartner)
    {
        return ($this->marketplacePartner = $_marketplacePartner);
    }
    /**
     * Get warehouse value
     * @return string|null
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }
    /**
     * Set warehouse value
     * @param string $_warehouse the warehouse
     * @return string
     */
    public function setWarehouse($_warehouse)
    {
        return ($this->warehouse = $_warehouse);
    }
    /**
     * Get structure value
     * @return string|null
     */
    public function getStructure()
    {
        return $this->structure;
    }
    /**
     * Set structure value
     * @param string $_structure the structure
     * @return string
     */
    public function setStructure($_structure)
    {
        return ($this->structure = $_structure);
    }
    /**
     * Get additionalCharacteristics value
     * @return string|null
     */
    public function getAdditionalCharacteristics()
    {
        return $this->additionalCharacteristics;
    }
    /**
     * Set additionalCharacteristics value
     * @param string $_additionalCharacteristics the additionalCharacteristics
     * @return string
     */
    public function setAdditionalCharacteristics($_additionalCharacteristics)
    {
        return ($this->additionalCharacteristics = $_additionalCharacteristics);
    }
    /**
     * Get material value
     * @return string|null
     */
    public function getMaterial()
    {
        return $this->material;
    }
    /**
     * Set material value
     * @param string $_material the material
     * @return string
     */
    public function setMaterial($_material)
    {
        return ($this->material = $_material);
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $_type the type
     * @return string
     */
    public function setType($_type)
    {
        return ($this->type = $_type);
    }
    /**
     * Get heel value
     * @return string|null
     */
    public function getHeel()
    {
        return $this->heel;
    }
    /**
     * Set heel value
     * @param string $_heel the heel
     * @return string
     */
    public function setHeel($_heel)
    {
        return ($this->heel = $_heel);
    }
    /**
     * Get sex value
     * @return string|null
     */
    public function getSex()
    {
        return $this->sex;
    }
    /**
     * Set sex value
     * @param string $_sex the sex
     * @return string
     */
    public function setSex($_sex)
    {
        return ($this->sex = $_sex);
    }
    /**
     * Get year value
     * @return string|null
     */
    public function getYear()
    {
        return $this->year;
    }
    /**
     * Set year value
     * @param string $_year the year
     * @return string
     */
    public function setYear($_year)
    {
        return ($this->year = $_year);
    }
    /**
     * Get season value
     * @return string|null
     */
    public function getSeason()
    {
        return $this->season;
    }
    /**
     * Set season value
     * @param string $_season the season
     * @return string
     */
    public function setSeason($_season)
    {
        return ($this->season = $_season);
    }
    /**
     * Get season_by_years value
     * @return string|null
     */
    public function getSeason_by_years()
    {
        return $this->season_by_years;
    }
    /**
     * Set season_by_years value
     * @param string $_season_by_years the season_by_years
     * @return string
     */
    public function setSeason_by_years($_season_by_years)
    {
        return ($this->season_by_years = $_season_by_years);
    }
    /**
     * Get style value
     * @return string|null
     */
    public function getStyle()
    {
        return $this->style;
    }
    /**
     * Set style value
     * @param string $_style the style
     * @return string
     */
    public function setStyle($_style)
    {
        return ($this->style = $_style);
    }
    /**
     * Get country value
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }
    /**
     * Set country value
     * @param string $_country the country
     * @return string
     */
    public function setCountry($_country)
    {
        return ($this->country = $_country);
    }
    /**
     * Get technology value
     * @return string|null
     */
    public function getTechnology()
    {
        return $this->technology;
    }
    /**
     * Set technology value
     * @param string $_technology the technology
     * @return string
     */
    public function setTechnology($_technology)
    {
        return ($this->technology = $_technology);
    }
    /**
     * Get color value
     * @return string|null
     */
    public function getColor()
    {
        return $this->color;
    }
    /**
     * Set color value
     * @param string $_color the color
     * @return string
     */
    public function setColor($_color)
    {
        return ($this->color = $_color);
    }
    /**
     * Get soleMaterial value
     * @return string|null
     */
    public function getSoleMaterial()
    {
        return $this->soleMaterial;
    }
    /**
     * Set soleMaterial value
     * @param string $_soleMaterial the soleMaterial
     * @return string
     */
    public function setSoleMaterial($_soleMaterial)
    {
        return ($this->soleMaterial = $_soleMaterial);
    }
    /**
     * Get liningMaterial value
     * @return string|null
     */
    public function getLiningMaterial()
    {
        return $this->liningMaterial;
    }
    /**
     * Set liningMaterial value
     * @param string $_liningMaterial the liningMaterial
     * @return string
     */
    public function setLiningMaterial($_liningMaterial)
    {
        return ($this->liningMaterial = $_liningMaterial);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructProperties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
