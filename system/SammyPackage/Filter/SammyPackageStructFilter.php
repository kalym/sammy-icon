<?php
/**
 * File for class SammyPackageStructFilter
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructFilter originally named Filter
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructFilter extends SammyPackageWsdlClass
{
    /**
     * The productId
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $productId;
    /**
     * The uniqueProductID
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $uniqueProductID;
    /**
     * Constructor method for Filter
     * @see parent::__construct()
     * @param string $_productId
     * @param string $_uniqueProductID
     * @return SammyPackageStructFilter
     */
    public function __construct($_productId = NULL,$_uniqueProductID = NULL)
    {
        parent::__construct(array('productId'=>$_productId,'uniqueProductID'=>$_uniqueProductID),false);
    }
    /**
     * Get productId value
     * @return string|null
     */
    public function getProductId()
    {
        return $this->productId;
    }
    /**
     * Set productId value
     * @param string $_productId the productId
     * @return string
     */
    public function setProductId($_productId)
    {
        return ($this->productId = $_productId);
    }
    /**
     * Get uniqueProductID value
     * @return string|null
     */
    public function getUniqueProductID()
    {
        return $this->uniqueProductID;
    }
    /**
     * Set uniqueProductID value
     * @param string $_uniqueProductID the uniqueProductID
     * @return string
     */
    public function setUniqueProductID($_uniqueProductID)
    {
        return ($this->uniqueProductID = $_uniqueProductID);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructFilter
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
