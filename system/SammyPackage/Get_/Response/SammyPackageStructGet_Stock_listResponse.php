<?php
/**
 * File for class SammyPackageStructGet_Stock_listResponse
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructGet_Stock_listResponse originally named Get_Stock_listResponse
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructGet_Stock_listResponse extends SammyPackageWsdlClass
{
    /**
     * The return
     * @var SammyPackageStructStock_list
     */
    public $return;
    /**
     * The exception
     * @var SammyPackageStructException
     */
    public $exception;
    /**
     * Constructor method for Get_Stock_listResponse
     * @see parent::__construct()
     * @param SammyPackageStructStock_list $_return
     * @param SammyPackageStructException $_exception
     * @return SammyPackageStructGet_Stock_listResponse
     */
    public function __construct($_return = NULL,$_exception = NULL)
    {
        parent::__construct(array('return'=>$_return,'exception'=>$_exception),false);
    }
    /**
     * Get return value
     * @return SammyPackageStructStock_list|null
     */
    public function getReturn()
    {
        return $this->return;
    }
    /**
     * Set return value
     * @param SammyPackageStructStock_list $_return the return
     * @return SammyPackageStructStock_list
     */
    public function setReturn($_return)
    {
        return ($this->return = $_return);
    }
    /**
     * Get exception value
     * @return SammyPackageStructException|null
     */
    public function getException()
    {
        return $this->exception;
    }
    /**
     * Set exception value
     * @param SammyPackageStructException $_exception the exception
     * @return SammyPackageStructException
     */
    public function setException($_exception)
    {
        return ($this->exception = $_exception);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructGet_Stock_listResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
