<?php
/**
 * File for class SammyPackageStructGet_Stock_list
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructGet_Stock_list originally named Get_Stock_list
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructGet_Stock_list extends SammyPackageWsdlClass
{
    /**
     * The filter
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var SammyPackageStructFilter
     */
    public $filter;
    /**
     * Constructor method for Get_Stock_list
     * @see parent::__construct()
     * @param SammyPackageStructFilter $_filter
     * @return SammyPackageStructGet_Stock_list
     */
    public function __construct($_filter = NULL)
    {
        parent::__construct(array('filter'=>$_filter),false);
    }
    /**
     * Get filter value
     * @return SammyPackageStructFilter|null
     */
    public function getFilter()
    {
        return $this->filter;
    }
    /**
     * Set filter value
     * @param SammyPackageStructFilter $_filter the filter
     * @return SammyPackageStructFilter
     */
    public function setFilter($_filter)
    {
        return ($this->filter = $_filter);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructGet_Stock_list
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
