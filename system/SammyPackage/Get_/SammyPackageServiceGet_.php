<?php
/**
 * File for class SammyPackageServiceGet_
 * @package SammyPackage
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageServiceGet_ originally named Get_
 * @package SammyPackage
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageServiceGet_ extends SammyPackageWsdlClass
{
    /**
     * Method to call the operation originally named Get_Stock_list
     * @uses SammyPackageWsdlClass::getSoapClient()
     * @uses SammyPackageWsdlClass::setResult()
     * @uses SammyPackageWsdlClass::saveLastError()
     * @param SammyPackageStructGet_Stock_list $_sammyPackageStructGet_Stock_list
     * @return SammyPackageStructGet_Stock_listResponse
     */
    public function Get_Stock_list(SammyPackageStructGet_Stock_list $_sammyPackageStructGet_Stock_list)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->Get_Stock_list($_sammyPackageStructGet_Stock_list));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see SammyPackageWsdlClass::getResult()
     * @return SammyPackageStructGet_Stock_listResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
