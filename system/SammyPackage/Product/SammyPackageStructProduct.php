<?php
/**
 * File for class SammyPackageStructProduct
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructProduct originally named Product
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructProduct extends SammyPackageWsdlClass
{
    /**
     * The productId
     * @var string
     */
    public $productId;
    /**
     * The active
     * @var boolean
     */
    public $active;
    /**
     * The article
     * @var string
     */
    public $article;
    /**
     * The goodsUnitName
     * @var string
     */
    public $goodsUnitName;
    /**
     * The goodsUnitNameEnglish
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $goodsUnitNameEnglish;
    /**
     * The customsCode
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $customsCode;
    /**
     * The weightKg
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var double
     */
    public $weightKg;
    /**
     * The Sizes
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * @var SammyPackageStructSize
     */
    public $Sizes;
    /**
     * The Images
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var SammyPackageStructImage
     */
    public $Images;
    /**
     * The Properties
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var SammyPackageStructProperties
     */
    public $Properties;
    /**
     * Constructor method for Product
     * @see parent::__construct()
     * @param string $_productId
     * @param boolean $_active
     * @param string $_article
     * @param string $_goodsUnitName
     * @param string $_goodsUnitNameEnglish
     * @param string $_customsCode
     * @param double $_weightKg
     * @param SammyPackageStructSize $_sizes
     * @param SammyPackageStructImage $_images
     * @param SammyPackageStructProperties $_properties
     * @return SammyPackageStructProduct
     */
    public function __construct($_productId = NULL,$_active = NULL,$_article = NULL,$_goodsUnitName = NULL,$_goodsUnitNameEnglish = NULL,$_customsCode = NULL,$_weightKg = NULL,$_sizes = NULL,$_images = NULL,$_properties = NULL)
    {
        parent::__construct(array('productId'=>$_productId,'active'=>$_active,'article'=>$_article,'goodsUnitName'=>$_goodsUnitName,'goodsUnitNameEnglish'=>$_goodsUnitNameEnglish,'customsCode'=>$_customsCode,'weightKg'=>$_weightKg,'Sizes'=>$_sizes,'Images'=>$_images,'Properties'=>$_properties),false);
    }
    /**
     * Get productId value
     * @return string|null
     */
    public function getProductId()
    {
        return $this->productId;
    }
    /**
     * Set productId value
     * @param string $_productId the productId
     * @return string
     */
    public function setProductId($_productId)
    {
        return ($this->productId = $_productId);
    }
    /**
     * Get active value
     * @return boolean|null
     */
    public function getActive()
    {
        return $this->active;
    }
    /**
     * Set active value
     * @param boolean $_active the active
     * @return boolean
     */
    public function setActive($_active)
    {
        return ($this->active = $_active);
    }
    /**
     * Get article value
     * @return string|null
     */
    public function getArticle()
    {
        return $this->article;
    }
    /**
     * Set article value
     * @param string $_article the article
     * @return string
     */
    public function setArticle($_article)
    {
        return ($this->article = $_article);
    }
    /**
     * Get goodsUnitName value
     * @return string|null
     */
    public function getGoodsUnitName()
    {
        return $this->goodsUnitName;
    }
    /**
     * Set goodsUnitName value
     * @param string $_goodsUnitName the goodsUnitName
     * @return string
     */
    public function setGoodsUnitName($_goodsUnitName)
    {
        return ($this->goodsUnitName = $_goodsUnitName);
    }
    /**
     * Get goodsUnitNameEnglish value
     * @return string|null
     */
    public function getGoodsUnitNameEnglish()
    {
        return $this->goodsUnitNameEnglish;
    }
    /**
     * Set goodsUnitNameEnglish value
     * @param string $_goodsUnitNameEnglish the goodsUnitNameEnglish
     * @return string
     */
    public function setGoodsUnitNameEnglish($_goodsUnitNameEnglish)
    {
        return ($this->goodsUnitNameEnglish = $_goodsUnitNameEnglish);
    }
    /**
     * Get customsCode value
     * @return string|null
     */
    public function getCustomsCode()
    {
        return $this->customsCode;
    }
    /**
     * Set customsCode value
     * @param string $_customsCode the customsCode
     * @return string
     */
    public function setCustomsCode($_customsCode)
    {
        return ($this->customsCode = $_customsCode);
    }
    /**
     * Get weightKg value
     * @return double|null
     */
    public function getWeightKg()
    {
        return $this->weightKg;
    }
    /**
     * Set weightKg value
     * @param double $_weightKg the weightKg
     * @return double
     */
    public function setWeightKg($_weightKg)
    {
        return ($this->weightKg = $_weightKg);
    }
    /**
     * Get Sizes value
     * @return SammyPackageStructSize|null
     */
    public function getSizes()
    {
        return $this->Sizes;
    }
    /**
     * Set Sizes value
     * @param SammyPackageStructSize $_sizes the Sizes
     * @return SammyPackageStructSize
     */
    public function setSizes($_sizes)
    {
        return ($this->Sizes = $_sizes);
    }
    /**
     * Get Images value
     * @return SammyPackageStructImage|null
     */
    public function getImages()
    {
        return $this->Images;
    }
    /**
     * Set Images value
     * @param SammyPackageStructImage $_images the Images
     * @return SammyPackageStructImage
     */
    public function setImages($_images)
    {
        return ($this->Images = $_images);
    }
    /**
     * Get Properties value
     * @return SammyPackageStructProperties|null
     */
    public function getProperties()
    {
        return $this->Properties;
    }
    /**
     * Set Properties value
     * @param SammyPackageStructProperties $_properties the Properties
     * @return SammyPackageStructProperties
     */
    public function setProperties($_properties)
    {
        return ($this->Properties = $_properties);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructProduct
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
