<?php
/**
 * File for class SammyPackageStructShipments
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructShipments originally named Shipments
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructShipments extends SammyPackageWsdlClass
{
    /**
     * The shipment
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * @var SammyPackageStructShipment
     */
    public $shipment;
    /**
     * Constructor method for Shipments
     * @see parent::__construct()
     * @param SammyPackageStructShipment $_shipment
     * @return SammyPackageStructShipments
     */
    public function __construct($_shipment = NULL)
    {
        parent::__construct(array('shipment'=>$_shipment),false);
    }
    /**
     * Get shipment value
     * @return SammyPackageStructShipment|null
     */
    public function getShipment()
    {
        return $this->shipment;
    }
    /**
     * Set shipment value
     * @param SammyPackageStructShipment $_shipment the shipment
     * @return SammyPackageStructShipment
     */
    public function setShipment($_shipment)
    {
        return ($this->shipment = $_shipment);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructShipments
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
