<?php
/**
 * File for class SammyPackageStructItems
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructItems originally named Items
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructItems extends SammyPackageWsdlClass
{
    /**
     * The product
     * @var string
     */
    public $product;
    /**
     * The quantity
     * @var integer
     */
    public $quantity;
    /**
     * The PriceUAH
     * @var double
     */
    public $PriceUAH;
    /**
     * The size
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $size;
    /**
     * Constructor method for Items
     * @see parent::__construct()
     * @param string $_product
     * @param integer $_quantity
     * @param double $_priceUAH
     * @param string $_size
     * @return SammyPackageStructItems
     */
    public function __construct($_product = NULL,$_quantity = NULL,$_priceUAH = NULL,$_size = NULL)
    {
        parent::__construct(array('product'=>$_product,'quantity'=>$_quantity,'PriceUAH'=>$_priceUAH,'size'=>$_size),false);
    }
    /**
     * Get product value
     * @return string|null
     */
    public function getProduct()
    {
        return $this->product;
    }
    /**
     * Set product value
     * @param string $_product the product
     * @return string
     */
    public function setProduct($_product)
    {
        return ($this->product = $_product);
    }
    /**
     * Get quantity value
     * @return integer|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    /**
     * Set quantity value
     * @param integer $_quantity the quantity
     * @return integer
     */
    public function setQuantity($_quantity)
    {
        return ($this->quantity = $_quantity);
    }
    /**
     * Get PriceUAH value
     * @return double|null
     */
    public function getPriceUAH()
    {
        return $this->PriceUAH;
    }
    /**
     * Set PriceUAH value
     * @param double $_priceUAH the PriceUAH
     * @return double
     */
    public function setPriceUAH($_priceUAH)
    {
        return ($this->PriceUAH = $_priceUAH);
    }
    /**
     * Get size value
     * @return string|null
     */
    public function getSize()
    {
        return $this->size;
    }
    /**
     * Set size value
     * @param string $_size the size
     * @return string
     */
    public function setSize($_size)
    {
        return ($this->size = $_size);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructItems
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
