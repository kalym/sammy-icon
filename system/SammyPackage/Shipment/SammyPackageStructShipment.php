<?php
/**
 * File for class SammyPackageStructShipment
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructShipment originally named Shipment
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructShipment extends SammyPackageWsdlClass
{
    /**
     * The odrerId
     * @var string
     */
    public $odrerId;
    /**
     * The shipmentDate
     * @var dateTime
     */
    public $shipmentDate;
    /**
     * The trackingNumber
     * @var string
     */
    public $trackingNumber;
    /**
     * The items
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * @var SammyPackageStructItems
     */
    public $items;
    /**
     * Constructor method for Shipment
     * @see parent::__construct()
     * @param string $_odrerId
     * @param dateTime $_shipmentDate
     * @param string $_trackingNumber
     * @param SammyPackageStructItems $_items
     * @return SammyPackageStructShipment
     */
    public function __construct($_odrerId = NULL,$_shipmentDate = NULL,$_trackingNumber = NULL,$_items = NULL)
    {
        parent::__construct(array('odrerId'=>$_odrerId,'shipmentDate'=>$_shipmentDate,'trackingNumber'=>$_trackingNumber,'items'=>$_items),false);
    }
    /**
     * Get odrerId value
     * @return string|null
     */
    public function getOdrerId()
    {
        return $this->odrerId;
    }
    /**
     * Set odrerId value
     * @param string $_odrerId the odrerId
     * @return string
     */
    public function setOdrerId($_odrerId)
    {
        return ($this->odrerId = $_odrerId);
    }
    /**
     * Get shipmentDate value
     * @return dateTime|null
     */
    public function getShipmentDate()
    {
        return $this->shipmentDate;
    }
    /**
     * Set shipmentDate value
     * @param dateTime $_shipmentDate the shipmentDate
     * @return dateTime
     */
    public function setShipmentDate($_shipmentDate)
    {
        return ($this->shipmentDate = $_shipmentDate);
    }
    /**
     * Get trackingNumber value
     * @return string|null
     */
    public function getTrackingNumber()
    {
        return $this->trackingNumber;
    }
    /**
     * Set trackingNumber value
     * @param string $_trackingNumber the trackingNumber
     * @return string
     */
    public function setTrackingNumber($_trackingNumber)
    {
        return ($this->trackingNumber = $_trackingNumber);
    }
    /**
     * Get items value
     * @return SammyPackageStructItems|null
     */
    public function getItems()
    {
        return $this->items;
    }
    /**
     * Set items value
     * @param SammyPackageStructItems $_items the items
     * @return SammyPackageStructItems
     */
    public function setItems($_items)
    {
        return ($this->items = $_items);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructShipment
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
