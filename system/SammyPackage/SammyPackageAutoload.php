<?php
/**
 * File to load generated classes once at once time
 * @package SammyPackage
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * Includes for all generated classes files
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
require_once dirname(__FILE__) . '/SammyPackageWsdlClass.php';
require_once dirname(__FILE__) . '/Get/Price/SammyPackageStructGetPrice.php';
require_once dirname(__FILE__) . '/Get/Response/SammyPackageStructGetPriceResponse.php';
require_once dirname(__FILE__) . '/Set/Price/SammyPackageStructSetPrice.php';
require_once dirname(__FILE__) . '/Set/Response/SammyPackageStructSetProductResponse.php';
require_once dirname(__FILE__) . '/Set/Product/SammyPackageStructSetProduct.php';
require_once dirname(__FILE__) . '/Get/Response/SammyPackageStructGetPropertiesProductResponse.php';
require_once dirname(__FILE__) . '/Get/Product/SammyPackageStructGetProduct.php';
require_once dirname(__FILE__) . '/Get/Response/SammyPackageStructGetProductResponse.php';
require_once dirname(__FILE__) . '/Set/Response/SammyPackageStructSetPriceResponse.php';
require_once dirname(__FILE__) . '/Get_/Stock_list/SammyPackageStructGet_Stock_list.php';
require_once dirname(__FILE__) . '/Get/Response/SammyPackageStructGetOrdersResponse.php';
require_once dirname(__FILE__) . '/Update/Orders/SammyPackageStructUpdateOrders.php';
require_once dirname(__FILE__) . '/Update/Response/SammyPackageStructUpdateOrdersResponse.php';
require_once dirname(__FILE__) . '/Get/Orders/SammyPackageStructGetOrders.php';
require_once dirname(__FILE__) . '/Set/Response/SammyPackageStructSetStock_listResponse.php';
require_once dirname(__FILE__) . '/Get_/Response/SammyPackageStructGet_Stock_listResponse.php';
require_once dirname(__FILE__) . '/Set/Stock_list/SammyPackageStructSetStock_list.php';
require_once dirname(__FILE__) . '/Get/Product/SammyPackageStructGetPropertiesProduct.php';
require_once dirname(__FILE__) . '/Exception/SammyPackageStructException.php';
require_once dirname(__FILE__) . '/Orders/SammyPackageStructOrders.php';
require_once dirname(__FILE__) . '/Price/SammyPackageStructPrice.php';
require_once dirname(__FILE__) . '/Prices/SammyPackageStructPrices.php';
require_once dirname(__FILE__) . '/Order/SammyPackageStructOrder.php';
require_once dirname(__FILE__) . '/Items/SammyPackageStructItems.php';
require_once dirname(__FILE__) . '/Image/SammyPackageStructImage.php';
require_once dirname(__FILE__) . '/Item_properties/SammyPackageStructItem_properties.php';
require_once dirname(__FILE__) . '/Product/SammyPackageStructProduct.php';
require_once dirname(__FILE__) . '/Products/SammyPackageStructProducts.php';
require_once dirname(__FILE__) . '/Size/SammyPackageStructSize.php';
require_once dirname(__FILE__) . '/Stock/SammyPackageStructStock.php';
require_once dirname(__FILE__) . '/Stock_list/SammyPackageStructStock_list.php';
require_once dirname(__FILE__) . '/Shipments/SammyPackageStructShipments.php';
require_once dirname(__FILE__) . '/Shipment/SammyPackageStructShipment.php';
require_once dirname(__FILE__) . '/Properties/SammyPackageStructProperties.php';
require_once dirname(__FILE__) . '/Properties_list/SammyPackageStructProperties_list.php';
require_once dirname(__FILE__) . '/Filter/SammyPackageStructFilter.php';
require_once dirname(__FILE__) . '/Get/SammyPackageServiceGet.php';
require_once dirname(__FILE__) . '/Set/SammyPackageServiceSet.php';
require_once dirname(__FILE__) . '/Get_/SammyPackageServiceGet_.php';
require_once dirname(__FILE__) . '/Update/SammyPackageServiceUpdate.php';
require_once dirname(__FILE__) . '/SammyPackageClassMap.php';
