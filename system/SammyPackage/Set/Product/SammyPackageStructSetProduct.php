<?php
/**
 * File for class SammyPackageStructSetProduct
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructSetProduct originally named SetProduct
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructSetProduct extends SammyPackageWsdlClass
{
    /**
     * The Items
     * @var SammyPackageStructProducts
     */
    public $Items;
    /**
     * Constructor method for SetProduct
     * @see parent::__construct()
     * @param SammyPackageStructProducts $_items
     * @return SammyPackageStructSetProduct
     */
    public function __construct($_items = NULL)
    {
        parent::__construct(array('Items'=>$_items),false);
    }
    /**
     * Get Items value
     * @return SammyPackageStructProducts|null
     */
    public function getItems()
    {
        return $this->Items;
    }
    /**
     * Set Items value
     * @param SammyPackageStructProducts $_items the Items
     * @return SammyPackageStructProducts
     */
    public function setItems($_items)
    {
        return ($this->Items = $_items);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructSetProduct
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
