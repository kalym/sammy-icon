<?php
/**
 * File for class SammyPackageStructSetPrice
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructSetPrice originally named SetPrice
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructSetPrice extends SammyPackageWsdlClass
{
    /**
     * The Prices
     * @var SammyPackageStructPrices
     */
    public $Prices;
    /**
     * The сode_сurrency
     * @var int
     */
    public $__ode___urrency;
    /**
     * Constructor method for SetPrice
     * @see parent::__construct()
     * @param SammyPackageStructPrices $_prices
     * @param int $___ode___urrency
     * @return SammyPackageStructSetPrice
     */
    public function __construct($_prices = NULL,$___ode___urrency = NULL)
    {
        parent::__construct(array('Prices'=>$_prices,'__ode___urrency'=>$___ode___urrency),false);
    }
    /**
     * Get Prices value
     * @return SammyPackageStructPrices|null
     */
    public function getPrices()
    {
        return $this->Prices;
    }
    /**
     * Set Prices value
     * @param SammyPackageStructPrices $_prices the Prices
     * @return SammyPackageStructPrices
     */
    public function setPrices($_prices)
    {
        return ($this->Prices = $_prices);
    }
    /**
     * Get сode_сurrency value
     * @return int|null
     */
    public function get__ode___urrency()
    {
        return $this->{'сode_сurrency'};
    }
    /**
     * Set сode_сurrency value
     * @param int $___ode___urrency the сode_сurrency
     * @return int
     */
    public function set__ode___urrency($___ode___urrency)
    {
        return ($this->__ode___urrency = $this->{'сode_сurrency'} = $___ode___urrency);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructSetPrice
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
