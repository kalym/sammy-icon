<?php
/**
 * File for class SammyPackageServiceSet
 * @package SammyPackage
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageServiceSet originally named Set
 * @package SammyPackage
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageServiceSet extends SammyPackageWsdlClass
{
    /**
     * Method to call the operation originally named SetProduct
     * @uses SammyPackageWsdlClass::getSoapClient()
     * @uses SammyPackageWsdlClass::setResult()
     * @uses SammyPackageWsdlClass::saveLastError()
     * @param SammyPackageStructSetProduct $_sammyPackageStructSetProduct
     * @return SammyPackageStructSetProductResponse
     */
    public function SetProduct(SammyPackageStructSetProduct $_sammyPackageStructSetProduct)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->SetProduct($_sammyPackageStructSetProduct));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named SetPrice
     * @uses SammyPackageWsdlClass::getSoapClient()
     * @uses SammyPackageWsdlClass::setResult()
     * @uses SammyPackageWsdlClass::saveLastError()
     * @param SammyPackageStructSetPrice $_sammyPackageStructSetPrice
     * @return SammyPackageStructSetPriceResponse
     */
    public function SetPrice(SammyPackageStructSetPrice $_sammyPackageStructSetPrice)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->SetPrice($_sammyPackageStructSetPrice));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named SetStock_list
     * @uses SammyPackageWsdlClass::getSoapClient()
     * @uses SammyPackageWsdlClass::setResult()
     * @uses SammyPackageWsdlClass::saveLastError()
     * @param SammyPackageStructSetStock_list $_sammyPackageStructSetStock_list
     * @return SammyPackageStructSetStock_listResponse
     */
    public function SetStock_list(SammyPackageStructSetStock_list $_sammyPackageStructSetStock_list)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->SetStock_list($_sammyPackageStructSetStock_list));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see SammyPackageWsdlClass::getResult()
     * @return SammyPackageStructSetPriceResponse|SammyPackageStructSetProductResponse|SammyPackageStructSetStock_listResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
