<?php
/**
 * File for class SammyPackageStructSetStock_list
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructSetStock_list originally named SetStock_list
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructSetStock_list extends SammyPackageWsdlClass
{
    /**
     * The stock_list
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var SammyPackageStructStock_list
     */
    public $stock_list;
    /**
     * Constructor method for SetStock_list
     * @see parent::__construct()
     * @param SammyPackageStructStock_list $_stock_list
     * @return SammyPackageStructSetStock_list
     */
    public function __construct($_stock_list = NULL)
    {
        parent::__construct(array('stock_list'=>$_stock_list),false);
    }
    /**
     * Get stock_list value
     * @return SammyPackageStructStock_list|null
     */
    public function getStock_list()
    {
        return $this->stock_list;
    }
    /**
     * Set stock_list value
     * @param SammyPackageStructStock_list $_stock_list the stock_list
     * @return SammyPackageStructStock_list
     */
    public function setStock_list($_stock_list)
    {
        return ($this->stock_list = $_stock_list);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructSetStock_list
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
