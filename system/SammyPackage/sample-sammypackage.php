<?php
/**
 * Test with SammyPackage for 'var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml'
 * @package SammyPackage
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
ini_set('memory_limit','512M');
ini_set('display_errors',true);
error_reporting(-1);
/**
 * Load autoload
 */
require_once dirname(__FILE__) . '/SammyPackageAutoload.php';
/**
 * Wsdl instanciation infos. By default, nothing has to be set.
 * If you wish to override the SoapClient's options, please refer to the sample below.
 * 
 * This is an associative array as:
 * - the key must be a SammyPackageWsdlClass constant beginning with WSDL_
 * - the value must be the corresponding key value
 * Each option matches the {@link http://www.php.net/manual/en/soapclient.soapclient.php} options
 * 
 * Here is below an example of how you can set the array:
 * $wsdl = array();
 * $wsdl[SammyPackageWsdlClass::WSDL_URL] = 'var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml';
 * $wsdl[SammyPackageWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
 * $wsdl[SammyPackageWsdlClass::WSDL_TRACE] = true;
 * $wsdl[SammyPackageWsdlClass::WSDL_LOGIN] = 'myLogin';
 * $wsdl[SammyPackageWsdlClass::WSDL_PASSWD] = '**********';
 * etc....
 * Then instantiate the Service class as: 
 * - $wsdlObject = new SammyPackageWsdlClass($wsdl);
 */
/**
 * Examples
 */


/************************************
 * Example for SammyPackageServiceGet
 */
$sammyPackageServiceGet = new SammyPackageServiceGet();
// sample call for SammyPackageServiceGet::GetPropertiesProduct()
if($sammyPackageServiceGet->GetPropertiesProduct())
    print_r($sammyPackageServiceGet->getResult());
else
    print_r($sammyPackageServiceGet->getLastError());
// sample call for SammyPackageServiceGet::GetProduct()
if($sammyPackageServiceGet->GetProduct(new SammyPackageStructGetProduct(/*** update parameters list ***/)))
    print_r($sammyPackageServiceGet->getResult());
else
    print_r($sammyPackageServiceGet->getLastError());
// sample call for SammyPackageServiceGet::GetPrice()
if($sammyPackageServiceGet->GetPrice(new SammyPackageStructGetPrice(/*** update parameters list ***/)))
    print_r($sammyPackageServiceGet->getResult());
else
    print_r($sammyPackageServiceGet->getLastError());
// sample call for SammyPackageServiceGet::GetOrders()
if($sammyPackageServiceGet->GetOrders(new SammyPackageStructGetOrders(/*** update parameters list ***/)))
    print_r($sammyPackageServiceGet->getResult());
else
    print_r($sammyPackageServiceGet->getLastError());

/************************************
 * Example for SammyPackageServiceSet
 */
$sammyPackageServiceSet = new SammyPackageServiceSet();
// sample call for SammyPackageServiceSet::SetProduct()
if($sammyPackageServiceSet->SetProduct(new SammyPackageStructSetProduct(/*** update parameters list ***/)))
    print_r($sammyPackageServiceSet->getResult());
else
    print_r($sammyPackageServiceSet->getLastError());
// sample call for SammyPackageServiceSet::SetPrice()
if($sammyPackageServiceSet->SetPrice(new SammyPackageStructSetPrice(/*** update parameters list ***/)))
    print_r($sammyPackageServiceSet->getResult());
else
    print_r($sammyPackageServiceSet->getLastError());
// sample call for SammyPackageServiceSet::SetStock_list()
if($sammyPackageServiceSet->SetStock_list(new SammyPackageStructSetStock_list(/*** update parameters list ***/)))
    print_r($sammyPackageServiceSet->getResult());
else
    print_r($sammyPackageServiceSet->getLastError());

/*************************************
 * Example for SammyPackageServiceGet_
 */
$sammyPackageServiceGet_ = new SammyPackageServiceGet_();
// sample call for SammyPackageServiceGet_::Get_Stock_list()
if($sammyPackageServiceGet_->Get_Stock_list(new SammyPackageStructGet_Stock_list(/*** update parameters list ***/)))
    print_r($sammyPackageServiceGet_->getResult());
else
    print_r($sammyPackageServiceGet_->getLastError());

/***************************************
 * Example for SammyPackageServiceUpdate
 */
$sammyPackageServiceUpdate = new SammyPackageServiceUpdate();
// sample call for SammyPackageServiceUpdate::UpdateOrders()
if($sammyPackageServiceUpdate->UpdateOrders(new SammyPackageStructUpdateOrders(/*** update parameters list ***/)))
    print_r($sammyPackageServiceUpdate->getResult());
else
    print_r($sammyPackageServiceUpdate->getLastError());
