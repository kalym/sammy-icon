<?php
/**
 * File for class SammyPackageStructStock
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructStock originally named Stock
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructStock extends SammyPackageWsdlClass
{
    /**
     * The productId
     * @var string
     */
    public $productId;
    /**
     * The size
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $size;
    /**
     * The quantity
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var int
     */
    public $quantity;
    /**
     * Constructor method for Stock
     * @see parent::__construct()
     * @param string $_productId
     * @param string $_size
     * @param int $_quantity
     * @return SammyPackageStructStock
     */
    public function __construct($_productId = NULL,$_size = NULL,$_quantity = NULL)
    {
        parent::__construct(array('productId'=>$_productId,'size'=>$_size,'quantity'=>$_quantity),false);
    }
    /**
     * Get productId value
     * @return string|null
     */
    public function getProductId()
    {
        return $this->productId;
    }
    /**
     * Set productId value
     * @param string $_productId the productId
     * @return string
     */
    public function setProductId($_productId)
    {
        return ($this->productId = $_productId);
    }
    /**
     * Get size value
     * @return string|null
     */
    public function getSize()
    {
        return $this->size;
    }
    /**
     * Set size value
     * @param string $_size the size
     * @return string
     */
    public function setSize($_size)
    {
        return ($this->size = $_size);
    }
    /**
     * Get quantity value
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    /**
     * Set quantity value
     * @param int $_quantity the quantity
     * @return int
     */
    public function setQuantity($_quantity)
    {
        return ($this->quantity = $_quantity);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructStock
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
