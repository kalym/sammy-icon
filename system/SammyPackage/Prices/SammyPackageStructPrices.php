<?php
/**
 * File for class SammyPackageStructPrices
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructPrices originally named Prices
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructPrices extends SammyPackageWsdlClass
{
    /**
     * The price
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * - nillable : true
     * @var SammyPackageStructPrice
     */
    public $price;
    /**
     * Constructor method for Prices
     * @see parent::__construct()
     * @param SammyPackageStructPrice $_price
     * @return SammyPackageStructPrices
     */
    public function __construct($_price = NULL)
    {
        parent::__construct(array('price'=>$_price),false);
    }
    /**
     * Get price value
     * @return SammyPackageStructPrice|null
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param SammyPackageStructPrice $_price the price
     * @return SammyPackageStructPrice
     */
    public function setPrice($_price)
    {
        return ($this->price = $_price);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructPrices
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
