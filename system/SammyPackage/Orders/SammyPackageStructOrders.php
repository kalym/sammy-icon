<?php
/**
 * File for class SammyPackageStructOrders
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructOrders originally named Orders
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructOrders extends SammyPackageWsdlClass
{
    /**
     * The order
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * @var SammyPackageStructOrder
     */
    public $order;
    /**
     * Constructor method for Orders
     * @see parent::__construct()
     * @param SammyPackageStructOrder $_order
     * @return SammyPackageStructOrders
     */
    public function __construct($_order = NULL)
    {
        parent::__construct(array('order'=>$_order),false);
    }
    /**
     * Get order value
     * @return SammyPackageStructOrder|null
     */
    public function getOrder()
    {
        return $this->order;
    }
    /**
     * Set order value
     * @param SammyPackageStructOrder $_order the order
     * @return SammyPackageStructOrder
     */
    public function setOrder($_order)
    {
        return ($this->order = $_order);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructOrders
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
