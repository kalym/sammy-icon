<?php
/**
 * File for class SammyPackageStructException
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructException originally named exception
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructException extends SammyPackageWsdlClass
{
    /**
     * The errors
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * - minOccurs : 0
     * @var string
     */
    public $errors;
    /**
     * Constructor method for exception
     * @see parent::__construct()
     * @param string $_errors
     * @return SammyPackageStructException
     */
    public function __construct($_errors = NULL)
    {
        parent::__construct(array('errors'=>$_errors),false);
    }
    /**
     * Get errors value
     * @return string|null
     */
    public function getErrors()
    {
        return $this->errors;
    }
    /**
     * Set errors value
     * @param string $_errors the errors
     * @return string
     */
    public function setErrors($_errors)
    {
        return ($this->errors = $_errors);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructException
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
