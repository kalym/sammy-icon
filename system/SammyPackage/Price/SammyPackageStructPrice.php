<?php
/**
 * File for class SammyPackageStructPrice
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructPrice originally named Price
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructPrice extends SammyPackageWsdlClass
{
    /**
     * The productId
     * @var string
     */
    public $productId;
    /**
     * The priceBase
     * @var decimal
     */
    public $priceBase;
    /**
     * The pricePromotional
     * @var decimal
     */
    public $pricePromotional;
    /**
     * Constructor method for Price
     * @see parent::__construct()
     * @param string $_productId
     * @param decimal $_priceBase
     * @param decimal $_pricePromotional
     * @return SammyPackageStructPrice
     */
    public function __construct($_productId = NULL,$_priceBase = NULL,$_pricePromotional = NULL)
    {
        parent::__construct(array('productId'=>$_productId,'priceBase'=>$_priceBase,'pricePromotional'=>$_pricePromotional),false);
    }
    /**
     * Get productId value
     * @return string|null
     */
    public function getProductId()
    {
        return $this->productId;
    }
    /**
     * Set productId value
     * @param string $_productId the productId
     * @return string
     */
    public function setProductId($_productId)
    {
        return ($this->productId = $_productId);
    }
    /**
     * Get priceBase value
     * @return decimal|null
     */
    public function getPriceBase()
    {
        return $this->priceBase;
    }
    /**
     * Set priceBase value
     * @param decimal $_priceBase the priceBase
     * @return decimal
     */
    public function setPriceBase($_priceBase)
    {
        return ($this->priceBase = $_priceBase);
    }
    /**
     * Get pricePromotional value
     * @return decimal|null
     */
    public function getPricePromotional()
    {
        return $this->pricePromotional;
    }
    /**
     * Set pricePromotional value
     * @param decimal $_pricePromotional the pricePromotional
     * @return decimal
     */
    public function setPricePromotional($_pricePromotional)
    {
        return ($this->pricePromotional = $_pricePromotional);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructPrice
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
