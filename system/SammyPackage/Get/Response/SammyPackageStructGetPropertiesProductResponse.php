<?php
/**
 * File for class SammyPackageStructGetPropertiesProductResponse
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructGetPropertiesProductResponse originally named GetPropertiesProductResponse
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructGetPropertiesProductResponse extends SammyPackageWsdlClass
{
    /**
     * The return
     * @var SammyPackageStructProperties_list
     */
    public $return;
    /**
     * Constructor method for GetPropertiesProductResponse
     * @see parent::__construct()
     * @param SammyPackageStructProperties_list $_return
     * @return SammyPackageStructGetPropertiesProductResponse
     */
    public function __construct($_return = NULL)
    {
        parent::__construct(array('return'=>$_return),false);
    }
    /**
     * Get return value
     * @return SammyPackageStructProperties_list|null
     */
    public function getReturn()
    {
        return $this->return;
    }
    /**
     * Set return value
     * @param SammyPackageStructProperties_list $_return the return
     * @return SammyPackageStructProperties_list
     */
    public function setReturn($_return)
    {
        return ($this->return = $_return);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructGetPropertiesProductResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
