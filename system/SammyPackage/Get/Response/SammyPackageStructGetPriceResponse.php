<?php
/**
 * File for class SammyPackageStructGetPriceResponse
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructGetPriceResponse originally named GetPriceResponse
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructGetPriceResponse extends SammyPackageWsdlClass
{
    /**
     * The return
     * @var SammyPackageStructPrices
     */
    public $return;
    /**
     * The exception
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var SammyPackageStructException
     */
    public $exception;
    /**
     * Constructor method for GetPriceResponse
     * @see parent::__construct()
     * @param SammyPackageStructPrices $_return
     * @param SammyPackageStructException $_exception
     * @return SammyPackageStructGetPriceResponse
     */
    public function __construct($_return = NULL,$_exception = NULL)
    {
        parent::__construct(array('return'=>$_return,'exception'=>$_exception),false);
    }
    /**
     * Get return value
     * @return SammyPackageStructPrices|null
     */
    public function getReturn()
    {
        return $this->return;
    }
    /**
     * Set return value
     * @param SammyPackageStructPrices $_return the return
     * @return SammyPackageStructPrices
     */
    public function setReturn($_return)
    {
        return ($this->return = $_return);
    }
    /**
     * Get exception value
     * @return SammyPackageStructException|null
     */
    public function getException()
    {
        return $this->exception;
    }
    /**
     * Set exception value
     * @param SammyPackageStructException $_exception the exception
     * @return SammyPackageStructException
     */
    public function setException($_exception)
    {
        return ($this->exception = $_exception);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructGetPriceResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
