<?php
/**
 * File for class SammyPackageServiceGet
 * @package SammyPackage
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageServiceGet originally named Get
 * @package SammyPackage
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageServiceGet extends SammyPackageWsdlClass
{
    /**
     * Method to call the operation originally named GetPropertiesProduct
     * @uses SammyPackageWsdlClass::getSoapClient()
     * @uses SammyPackageWsdlClass::setResult()
     * @uses SammyPackageWsdlClass::saveLastError()
     * @param SammyPackageStructGetPropertiesProduct $_sammyPackageStructGetPropertiesProduct
     * @return SammyPackageStructGetPropertiesProductResponse
     */
    public function GetPropertiesProduct()
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetPropertiesProduct());
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetProduct
     * @uses SammyPackageWsdlClass::getSoapClient()
     * @uses SammyPackageWsdlClass::setResult()
     * @uses SammyPackageWsdlClass::saveLastError()
     * @param SammyPackageStructGetProduct $_sammyPackageStructGetProduct
     * @return SammyPackageStructGetProductResponse
     */
    public function GetProduct(SammyPackageStructGetProduct $_sammyPackageStructGetProduct)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetProduct($_sammyPackageStructGetProduct));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetPrice
     * @uses SammyPackageWsdlClass::getSoapClient()
     * @uses SammyPackageWsdlClass::setResult()
     * @uses SammyPackageWsdlClass::saveLastError()
     * @param SammyPackageStructGetPrice $_sammyPackageStructGetPrice
     * @return SammyPackageStructGetPriceResponse
     */
    public function GetPrice(SammyPackageStructGetPrice $_sammyPackageStructGetPrice)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetPrice($_sammyPackageStructGetPrice));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetOrders
     * @uses SammyPackageWsdlClass::getSoapClient()
     * @uses SammyPackageWsdlClass::setResult()
     * @uses SammyPackageWsdlClass::saveLastError()
     * @param SammyPackageStructGetOrders $_sammyPackageStructGetOrders
     * @return SammyPackageStructGetOrdersResponse
     */
    public function GetOrders(SammyPackageStructGetOrders $_sammyPackageStructGetOrders)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetOrders($_sammyPackageStructGetOrders));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see SammyPackageWsdlClass::getResult()
     * @return SammyPackageStructGetOrdersResponse|SammyPackageStructGetPriceResponse|SammyPackageStructGetProductResponse|SammyPackageStructGetPropertiesProductResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
