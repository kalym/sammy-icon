<?php
/**
 * File for class SammyPackageStructGetOrders
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructGetOrders originally named GetOrders
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructGetOrders extends SammyPackageWsdlClass
{
    /**
     * The DateFrom
     * @var dateTime
     */
    public $DateFrom;
    /**
     * The DateTo
     * @var dateTime
     */
    public $DateTo;
    /**
     * Constructor method for GetOrders
     * @see parent::__construct()
     * @param dateTime $_dateFrom
     * @param dateTime $_dateTo
     * @return SammyPackageStructGetOrders
     */
    public function __construct($_dateFrom = NULL,$_dateTo = NULL)
    {
        parent::__construct(array('DateFrom'=>$_dateFrom,'DateTo'=>$_dateTo),false);
    }
    /**
     * Get DateFrom value
     * @return dateTime|null
     */
    public function getDateFrom()
    {
        return $this->DateFrom;
    }
    /**
     * Set DateFrom value
     * @param dateTime $_dateFrom the DateFrom
     * @return dateTime
     */
    public function setDateFrom($_dateFrom)
    {
        return ($this->DateFrom = $_dateFrom);
    }
    /**
     * Get DateTo value
     * @return dateTime|null
     */
    public function getDateTo()
    {
        return $this->DateTo;
    }
    /**
     * Set DateTo value
     * @param dateTime $_dateTo the DateTo
     * @return dateTime
     */
    public function setDateTo($_dateTo)
    {
        return ($this->DateTo = $_dateTo);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructGetOrders
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
