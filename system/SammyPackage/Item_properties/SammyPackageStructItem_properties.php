<?php
/**
 * File for class SammyPackageStructItem_properties
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructItem_properties originally named Item_properties
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructItem_properties extends SammyPackageWsdlClass
{
    /**
     * The name
     * @var string
     */
    public $name;
    /**
     * The descript
     * @var string
     */
    public $descript;
    /**
     * The value
     * @var string
     */
    public $value;
    /**
     * The comment
     * @var string
     */
    public $comment;
    /**
     * Constructor method for Item_properties
     * @see parent::__construct()
     * @param string $_name
     * @param string $_descript
     * @param string $_value
     * @param string $_comment
     * @return SammyPackageStructItem_properties
     */
    public function __construct($_name = NULL,$_descript = NULL,$_value = NULL,$_comment = NULL)
    {
        parent::__construct(array('name'=>$_name,'descript'=>$_descript,'value'=>$_value,'comment'=>$_comment),false);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $_name the name
     * @return string
     */
    public function setName($_name)
    {
        return ($this->name = $_name);
    }
    /**
     * Get descript value
     * @return string|null
     */
    public function getDescript()
    {
        return $this->descript;
    }
    /**
     * Set descript value
     * @param string $_descript the descript
     * @return string
     */
    public function setDescript($_descript)
    {
        return ($this->descript = $_descript);
    }
    /**
     * Get value value
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param string $_value the value
     * @return string
     */
    public function setValue($_value)
    {
        return ($this->value = $_value);
    }
    /**
     * Get comment value
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * Set comment value
     * @param string $_comment the comment
     * @return string
     */
    public function setComment($_comment)
    {
        return ($this->comment = $_comment);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructItem_properties
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
