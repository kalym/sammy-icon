<?php
/**
 * File for class SammyPackageStructProducts
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructProducts originally named Products
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructProducts extends SammyPackageWsdlClass
{
    /**
     * The product
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * @var SammyPackageStructProduct
     */
    public $product;
    /**
     * Constructor method for Products
     * @see parent::__construct()
     * @param SammyPackageStructProduct $_product
     * @return SammyPackageStructProducts
     */
    public function __construct($_product = NULL)
    {
        parent::__construct(array('product'=>$_product),false);
    }
    /**
     * Get product value
     * @return SammyPackageStructProduct|null
     */
    public function getProduct()
    {
        return $this->product;
    }
    /**
     * Set product value
     * @param SammyPackageStructProduct $_product the product
     * @return SammyPackageStructProduct
     */
    public function setProduct($_product)
    {
        return ($this->product = $_product);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructProducts
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
