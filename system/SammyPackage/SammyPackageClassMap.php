<?php
/**
 * File for the class which returns the class map definition
 * @package SammyPackage
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * Class which returns the class map definition by the static method SammyPackageClassMap::classMap()
 * @package SammyPackage
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageClassMap
{
    /**
     * This method returns the array containing the mapping between WSDL structs and generated classes
     * This array is sent to the SoapClient when calling the WS
     * @return array
     */
    final public static function classMap()
    {
        return array (
  'Filter' => 'SammyPackageStructFilter',
  'GetOrders' => 'SammyPackageStructGetOrders',
  'GetOrdersResponse' => 'SammyPackageStructGetOrdersResponse',
  'GetPrice' => 'SammyPackageStructGetPrice',
  'GetPriceResponse' => 'SammyPackageStructGetPriceResponse',
  'GetProduct' => 'SammyPackageStructGetProduct',
  'GetProductResponse' => 'SammyPackageStructGetProductResponse',
  'GetPropertiesProduct' => 'SammyPackageStructGetPropertiesProduct',
  'GetPropertiesProductResponse' => 'SammyPackageStructGetPropertiesProductResponse',
  'Get_Stock_list' => 'SammyPackageStructGet_Stock_list',
  'Get_Stock_listResponse' => 'SammyPackageStructGet_Stock_listResponse',
  'Image' => 'SammyPackageStructImage',
  'Item_properties' => 'SammyPackageStructItem_properties',
  'Items' => 'SammyPackageStructItems',
  'Order' => 'SammyPackageStructOrder',
  'Orders' => 'SammyPackageStructOrders',
  'Price' => 'SammyPackageStructPrice',
  'Prices' => 'SammyPackageStructPrices',
  'Product' => 'SammyPackageStructProduct',
  'Products' => 'SammyPackageStructProducts',
  'Properties' => 'SammyPackageStructProperties',
  'Properties_list' => 'SammyPackageStructProperties_list',
  'SetPrice' => 'SammyPackageStructSetPrice',
  'SetPriceResponse' => 'SammyPackageStructSetPriceResponse',
  'SetProduct' => 'SammyPackageStructSetProduct',
  'SetProductResponse' => 'SammyPackageStructSetProductResponse',
  'SetStock_list' => 'SammyPackageStructSetStock_list',
  'SetStock_listResponse' => 'SammyPackageStructSetStock_listResponse',
  'Shipment' => 'SammyPackageStructShipment',
  'Shipments' => 'SammyPackageStructShipments',
  'Size' => 'SammyPackageStructSize',
  'Stock' => 'SammyPackageStructStock',
  'Stock_list' => 'SammyPackageStructStock_list',
  'UpdateOrders' => 'SammyPackageStructUpdateOrders',
  'UpdateOrdersResponse' => 'SammyPackageStructUpdateOrdersResponse',
  'exception' => 'SammyPackageStructException',
);
    }
}
