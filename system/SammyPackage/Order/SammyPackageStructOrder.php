<?php
/**
 * File for class SammyPackageStructOrder
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructOrder originally named Order
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructOrder extends SammyPackageWsdlClass
{
    /**
     * The DataField
     * @var dateTime
     */
    public $DataField;
    /**
     * The OrderId
     * @var string
     */
    public $OrderId;
    /**
     * The ClientName
     * @var string
     */
    public $ClientName;
    /**
     * The Street
     * @var string
     */
    public $Street;
    /**
     * The BuildingNumber
     * @var string
     */
    public $BuildingNumber;
    /**
     * The AppartmentNumber
     * @var string
     */
    public $AppartmentNumber;
    /**
     * The City
     * @var string
     */
    public $City;
    /**
     * The Country
     * @var string
     */
    public $Country;
    /**
     * The PostCode
     * @var anyType
     */
    public $PostCode;
    /**
     * The Region
     * @var string
     */
    public $Region;
    /**
     * The District
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $District;
    /**
     * The Phone
     * @var string
     */
    public $Phone;
    /**
     * The ShipmentID
     * @var string
     */
    public $ShipmentID;
    /**
     * The BranchCarrier
     * @var string
     */
    public $BranchCarrier;
    /**
     * The Items
     * Meta informations extracted from the WSDL
     * - maxOccurs : unbounded
     * @var SammyPackageStructItems
     */
    public $Items;
    /**
     * Constructor method for Order
     * @see parent::__construct()
     * @param dateTime $_dataField
     * @param string $_orderId
     * @param string $_clientName
     * @param string $_street
     * @param string $_buildingNumber
     * @param string $_appartmentNumber
     * @param string $_city
     * @param string $_country
     * @param anyType $_postCode
     * @param string $_region
     * @param string $_district
     * @param string $_phone
     * @param string $_shipmentID
     * @param string $_branchCarrier
     * @param SammyPackageStructItems $_items
     * @return SammyPackageStructOrder
     */
    public function __construct($_dataField = NULL,$_orderId = NULL,$_clientName = NULL,$_street = NULL,$_buildingNumber = NULL,$_appartmentNumber = NULL,$_city = NULL,$_country = NULL,$_postCode = NULL,$_region = NULL,$_district = NULL,$_phone = NULL,$_shipmentID = NULL,$_branchCarrier = NULL,$_items = NULL)
    {
        parent::__construct(array('DataField'=>$_dataField,'OrderId'=>$_orderId,'ClientName'=>$_clientName,'Street'=>$_street,'BuildingNumber'=>$_buildingNumber,'AppartmentNumber'=>$_appartmentNumber,'City'=>$_city,'Country'=>$_country,'PostCode'=>$_postCode,'Region'=>$_region,'District'=>$_district,'Phone'=>$_phone,'ShipmentID'=>$_shipmentID,'BranchCarrier'=>$_branchCarrier,'Items'=>$_items),false);
    }
    /**
     * Get DataField value
     * @return dateTime|null
     */
    public function getDataField()
    {
        return $this->DataField;
    }
    /**
     * Set DataField value
     * @param dateTime $_dataField the DataField
     * @return dateTime
     */
    public function setDataField($_dataField)
    {
        return ($this->DataField = $_dataField);
    }
    /**
     * Get OrderId value
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->OrderId;
    }
    /**
     * Set OrderId value
     * @param string $_orderId the OrderId
     * @return string
     */
    public function setOrderId($_orderId)
    {
        return ($this->OrderId = $_orderId);
    }
    /**
     * Get ClientName value
     * @return string|null
     */
    public function getClientName()
    {
        return $this->ClientName;
    }
    /**
     * Set ClientName value
     * @param string $_clientName the ClientName
     * @return string
     */
    public function setClientName($_clientName)
    {
        return ($this->ClientName = $_clientName);
    }
    /**
     * Get Street value
     * @return string|null
     */
    public function getStreet()
    {
        return $this->Street;
    }
    /**
     * Set Street value
     * @param string $_street the Street
     * @return string
     */
    public function setStreet($_street)
    {
        return ($this->Street = $_street);
    }
    /**
     * Get BuildingNumber value
     * @return string|null
     */
    public function getBuildingNumber()
    {
        return $this->BuildingNumber;
    }
    /**
     * Set BuildingNumber value
     * @param string $_buildingNumber the BuildingNumber
     * @return string
     */
    public function setBuildingNumber($_buildingNumber)
    {
        return ($this->BuildingNumber = $_buildingNumber);
    }
    /**
     * Get AppartmentNumber value
     * @return string|null
     */
    public function getAppartmentNumber()
    {
        return $this->AppartmentNumber;
    }
    /**
     * Set AppartmentNumber value
     * @param string $_appartmentNumber the AppartmentNumber
     * @return string
     */
    public function setAppartmentNumber($_appartmentNumber)
    {
        return ($this->AppartmentNumber = $_appartmentNumber);
    }
    /**
     * Get City value
     * @return string|null
     */
    public function getCity()
    {
        return $this->City;
    }
    /**
     * Set City value
     * @param string $_city the City
     * @return string
     */
    public function setCity($_city)
    {
        return ($this->City = $_city);
    }
    /**
     * Get Country value
     * @return string|null
     */
    public function getCountry()
    {
        return $this->Country;
    }
    /**
     * Set Country value
     * @param string $_country the Country
     * @return string
     */
    public function setCountry($_country)
    {
        return ($this->Country = $_country);
    }
    /**
     * Get PostCode value
     * @return anyType|null
     */
    public function getPostCode()
    {
        return $this->PostCode;
    }
    /**
     * Set PostCode value
     * @param anyType $_postCode the PostCode
     * @return anyType
     */
    public function setPostCode($_postCode)
    {
        return ($this->PostCode = $_postCode);
    }
    /**
     * Get Region value
     * @return string|null
     */
    public function getRegion()
    {
        return $this->Region;
    }
    /**
     * Set Region value
     * @param string $_region the Region
     * @return string
     */
    public function setRegion($_region)
    {
        return ($this->Region = $_region);
    }
    /**
     * Get District value
     * @return string|null
     */
    public function getDistrict()
    {
        return $this->District;
    }
    /**
     * Set District value
     * @param string $_district the District
     * @return string
     */
    public function setDistrict($_district)
    {
        return ($this->District = $_district);
    }
    /**
     * Get Phone value
     * @return string|null
     */
    public function getPhone()
    {
        return $this->Phone;
    }
    /**
     * Set Phone value
     * @param string $_phone the Phone
     * @return string
     */
    public function setPhone($_phone)
    {
        return ($this->Phone = $_phone);
    }
    /**
     * Get ShipmentID value
     * @return string|null
     */
    public function getShipmentID()
    {
        return $this->ShipmentID;
    }
    /**
     * Set ShipmentID value
     * @param string $_shipmentID the ShipmentID
     * @return string
     */
    public function setShipmentID($_shipmentID)
    {
        return ($this->ShipmentID = $_shipmentID);
    }
    /**
     * Get BranchCarrier value
     * @return string|null
     */
    public function getBranchCarrier()
    {
        return $this->BranchCarrier;
    }
    /**
     * Set BranchCarrier value
     * @param string $_branchCarrier the BranchCarrier
     * @return string
     */
    public function setBranchCarrier($_branchCarrier)
    {
        return ($this->BranchCarrier = $_branchCarrier);
    }
    /**
     * Get Items value
     * @return SammyPackageStructItems|null
     */
    public function getItems()
    {
        return $this->Items;
    }
    /**
     * Set Items value
     * @param SammyPackageStructItems $_items the Items
     * @return SammyPackageStructItems
     */
    public function setItems($_items)
    {
        return ($this->Items = $_items);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructOrder
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
