<?php
/**
 * File for class SammyPackageStructImage
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
/**
 * This class stands for SammyPackageStructImage originally named Image
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/46913-21b828da235e1805eb1ae53f63989772/wsdl.xml
 * @package SammyPackage
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2018-06-05
 */
class SammyPackageStructImage extends SammyPackageWsdlClass
{
    /**
     * The deff
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var boolean
     */
    public $deff;
    /**
     * The url
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $url;
    /**
     * Constructor method for Image
     * @see parent::__construct()
     * @param boolean $_deff
     * @param string $_url
     * @return SammyPackageStructImage
     */
    public function __construct($_deff = NULL,$_url = NULL)
    {
        parent::__construct(array('deff'=>$_deff,'url'=>$_url),false);
    }
    /**
     * Get deff value
     * @return boolean|null
     */
    public function getDeff()
    {
        return $this->deff;
    }
    /**
     * Set deff value
     * @param boolean $_deff the deff
     * @return boolean
     */
    public function setDeff($_deff)
    {
        return ($this->deff = $_deff);
    }
    /**
     * Get url value
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * Set url value
     * @param string $_url the url
     * @return string
     */
    public function setUrl($_url)
    {
        return ($this->url = $_url);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see SammyPackageWsdlClass::__set_state()
     * @uses SammyPackageWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return SammyPackageStructImage
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
