<?php
class SkuOption {
    private $option_name;

    private $value_name;

    public function __construct($option_name,$value_name)
    {
        $this->option_name = $option_name;
        $this->value_name = $value_name;
    }

    /**
     * @return mixed
     */
    public function getOptionName()
    {
        return $this->option_name;
    }

    /**
     * @param mixed $option_name
     */
    public function setOptionName($option_name)
    {
        $this->option_name = $option_name;
    }

    /**
     * @return mixed
     */
    public function getValueName()
    {
        return $this->value_name;
    }

    /**
     * @param mixed $value_name
     */
    public function setValueName($value_name)
    {
        $this->value_name = $value_name;
    }
}