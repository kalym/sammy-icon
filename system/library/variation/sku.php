<?php
class Sku {
    private $sku_id;
    
    private $sku;
    
    private $options;

    public function __construct($sku_id,$sku)
    {
        //может здесь использовать сеттеры?
        $this->sku_id = $sku_id;
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getSkuId()
    {
        return $this->sku_id;
    }

    /**
     * @param mixed $sku_id
     */
    public function setSkuId($sku_id)
    {
        $this->sku_id = $sku_id;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @param mixed $option
     */
    public function addToOptions($option, $key)
    {
        $this->options[$key]= $option;
    }
    
    
    
}