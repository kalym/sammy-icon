<?php
/**
 * Created by PhpStorm.
 * User: dgmax
 * Date: 02.07.2016
 * Time: 19:47
 */


use GuzzleHttp\ClientInterface;

include_once DIR_ROOT . "oauth/provider/AbstractProvider.php";
include_once DIR_ROOT . "oauth/provider/ProviderInterface.php";
include_once DIR_ROOT . "oauth/Helper.php";
include_once DIR_ROOT . "oauth/provider/User.php";




class VKProvider extends AbstractProvider implements ProviderInterface
{
    const AUTHORIZE_URL = 'https://oauth.vk.com/authorize';

    const ACCESS_TOKEN_URL = 'https://oauth.vk.com/access_token';

    const API_VERSION = "5.26";

    private $vkBaseUrl = "https://api.vk.com/method/";

    protected $scopes = ["offline"];

    protected $oauthResponse;

    protected function getAuthUrl($state = null)
    {
        $url = $this->buildAuthUrlFromBase(self::AUTHORIZE_URL);
        return $url;
    }

    protected function getScopes()
    {
        // TODO: Implement getScopes() method.
        return $this->scopes;
    }


    protected function getTokenUrl()
    {
        return self::ACCESS_TOKEN_URL;
    }

    protected function getAccessTokenResponse($code)
    {
        $postKey = (version_compare(ClientInterface::VERSION, '6') === 1) ? 'form_params' : 'body';
        $url = $this->getTokenUrl();
        $response = $this->getHttpClient()->post($url, [
            $postKey => $this->getTokenFields($code)
        ])->getBody()->getContents();


        return json_decode($response, true);
    }

    protected function extractUserFromResponse($response)
    {
        $meUrl = $this->vkBaseUrl.'users.get?user_ids='.$response['user_id'].'&name_case=nom&access_token='.$response['access_token'];
        $vkResponse = $this->getHttpClient()->get($meUrl)->getBody()->getContents();
        $profile = json_decode($vkResponse, true);

        return array_merge($response, $profile['response'][0]);
    }


    protected function mapUserToObject(array $user)
    {
        $user=  new User(
            $user['access_token'],
            $user['access_token'],
            $user['expires_in'],
            $user['user_id'],
            $user['first_name'],
            $user['last_name'],
            null
        );
        $user->setProviderType('vkontakte');

        return $user;
    }
}