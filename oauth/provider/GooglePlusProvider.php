<?php
/**
 * Created by PhpStorm.
 * User: dgmax
 * Date: 06.07.2016
 * Time: 23:05
 */

use GuzzleHttp\ClientInterface;

include_once DIR_ROOT . "oauth/provider/AbstractProvider.php";
include_once DIR_ROOT . "oauth/provider/ProviderInterface.php";
include_once DIR_ROOT . "oauth/Helper.php";
include_once DIR_ROOT . "oauth/provider/User.php";

class GooglePlusProvider extends AbstractProvider implements ProviderInterface
{
    protected $scopes = [
        'profile'
    ];


    protected function getAuthUrl($state = null)
    {
        return $this->buildAuthUrlFromBase('https://accounts.google.com/o/oauth2/auth');
    }

    protected function getTokenUrl()
    {
        return 'https://accounts.google.com/o/oauth2/token';
    }

    protected function getScopes()
    {
        // TODO: Implement getScopes() method.
        return $this->scopes;
    }


    protected function getTokenFields($code)
    {
        return array_merge(parent::getTokenFields($code), ['grant_type' => 'authorization_code']);
    }

    protected function extractUserFromResponse($data)
    {
        $response = $this->getHttpClient()->get('https://www.googleapis.com/plus/v1/people/me?', [
            'query' => [
                'prettyPrint' => 'false',
            ],
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$data['access_token'],
            ],
        ]);
        return array_merge(json_decode($response->getBody(), true), $data);
    }

    protected function mapUserToObject(array $data)
    {
        $explodedName = explode(' ', $data['displayName']);
        $user = new User($data['access_token'],
            $data['access_token'],
            $data['expires_in'],
            $data['id'],
            $explodedName[0],
            $explodedName[1],
            isset($data['emails']) ? $data['emails'] : null
        );
        $user->setProviderType('google');
        
        return $user;
    }

    public function getAccessTokenResponse($code)
    {
        $postKey = (version_compare(ClientInterface::VERSION, '6') === 1) ? 'form_params' : 'body';
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers' => ['Accept' => 'application/json'],
            $postKey => $this->getTokenFields($code),
        ]);
        return json_decode($response->getBody(), true);
    }
}