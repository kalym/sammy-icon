<?php


/**
 * Created by PhpStorm.
 * User: maxim.drobonoh
 * Date: 02.07.2016
 * Time: 17:13
 */
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\TransferStats;



abstract class AbstractProvider
{
    /**
     * The HTTP client instance
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $clientSecret;

    /**
     * @var string
     */
    protected $redirectUrl;



    protected $scopeSeparator = ',';


    protected $encodingType = PHP_QUERY_RFC1738;



    abstract protected function getAuthUrl($state = null);

    /**
     * Get the token URL for the provider.
     *
     * @return string
     */
    abstract protected function getTokenUrl();
    
    abstract protected function extractUserFromResponse($response);



    /**
     * Map the raw user array to a Socialite User instance.
     *
     * @param  array $user
     * @return User
     */
    abstract protected function mapUserToObject(array $user);

    /**
     * AbstractProvider constructor.
     * @param $clientId
     * @param $clientSecret
     * @param $redirectUrl
     */
    public function __construct($clientId, $clientSecret, $redirectUrl)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @return Client
     */
    protected function getHttpClient()
    {
        if (is_null($this->httpClient)) {
            $this->httpClient = new Client();
        }
        return $this->httpClient;
    }

    /**
     * @param Client $client
     * @return Client
     */
    protected function setHttpClient(Client $client)
    {
        $this->httpClient = $client;

        return $this->httpClient;
    }
    


    /**
     * @return string
     */
    public function redirect()
    {
        $url = $this->getAuthUrl();
        return $url;
    }
    
    

    /**
     * Get the access token response for the given code.
     *
     * @param  string  $code
     * @return array
     */
    protected abstract function getAccessTokenResponse($code);

    /**
     * Get the authentication URL for the provider.
     *
     * @param  string $url
     * @param  string $state
     * @return string
     */
    protected function buildAuthUrlFromBase($url)
    {
        return $url . '?' . http_build_query($this->getCodeFields(), '', '&', $this->encodingType);
    }

    /**
     * Get the GET parameters for the code request.
     *
     * @return array
     */
    protected function getCodeFields()
    {
        $fields = [
            'client_id' => $this->clientId, 'redirect_uri' => $this->redirectUrl,
            'scope' => $this->formatScopes($this->getScopes(), $this->scopeSeparator),
            'response_type' => 'code',
        ];


        return $fields;
    }
    
    protected abstract function getScopes();

    public function user()
    {
        $response = $this->getAccessTokenResponse($this->getCode());
        
        $user = $this->mapUserToObject($this->extractUserFromResponse($response));
        

        return $user;
    }

    protected function getCode()
    {
        return $_REQUEST['code'];
    }

    /**
     * Format the given scopes.
     *
     * @param  array $scopes
     * @param  string $scopeSeparator
     * @return string
     * @return string
     * @return string
     */
    protected function formatScopes(array $scopes, $scopeSeparator)
    {
        return implode($scopeSeparator, $scopes);
    }

    /**
     * Get the POST fields for the token request.
     *
     * @param  string  $code
     * @return array
     */
    protected function getTokenFields($code)
    {
        $tokenFields= [
            'client_id' => $this->clientId, 'client_secret' => $this->clientSecret,
            'code' => $code, 'redirect_uri' => $this->redirectUrl,
        ];
        return $tokenFields;
    }

}