<?php
/**
 * Created by PhpStorm.
 * User: dgmax
 * Date: 03.07.2016
 * Time: 14:40
 */



interface ProviderInterface
{
    function redirect();

    function user();
}