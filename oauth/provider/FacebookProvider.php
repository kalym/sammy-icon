<?php
/**
 * Created by PhpStorm.
 * User: dgmax
 * Date: 06.07.2016
 * Time: 0:05
 */

use GuzzleHttp\ClientInterface;

include_once DIR_ROOT . "oauth/provider/AbstractProvider.php";
include_once DIR_ROOT . "oauth/provider/ProviderInterface.php";
include_once DIR_ROOT . "oauth/Helper.php";
include_once DIR_ROOT . "oauth/provider/User.php";

class FacebookProvider extends AbstractProvider implements ProviderInterface {
    /**
     * The base Facebook Graph URL.
     *
     * @var string
     */
    protected $graphUrl = 'https://graph.facebook.com';
    /**
     * The Graph API version for the request.
     *
     * @var string
     */
    protected $version = 'v2.6';
    /**
     * The user fields being requested.
     *
     * @var array
     */
    protected $fields = ['name', 'email', 'gender', 'verified', 'link'];
    /**
     * The scopes being requested.
     *
     * @var array
     */
    protected $scopes = ['email'];
    /**
     * Display the dialog in a popup view.
     *
     * @var bool
     */
    protected $popup = false;
    /**
     * Re-request a declined permission.
     *
     * @var bool
     */
    protected $reRequest = false;
    /**
     * {@inheritdoc}
     */
    protected function getAuthUrl($state = null)
    {
        return $this->buildAuthUrlFromBase('https://www.facebook.com/'.$this->version.'/dialog/oauth', $state);
    }
    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return $this->graphUrl.'/oauth/access_token';
    }

    protected function getAccessTokenResponse($code)
    {
        $postKey = (version_compare(ClientInterface::VERSION, '6') === 1) ? 'form_params' : 'body';
        $url = $this->getTokenUrl();
        $response = $this->getHttpClient()->post($url, [
            $postKey => $this->getTokenFields($code)
        ])->getBody()->getContents();

        $data = [];
        parse_str($response, $data);

        return $data;
    }
 
    /**
     * {@inheritdoc}
     */
    protected function extractUserFromResponse($data)
    {
        $meUrl = $this->graphUrl.'/'.$this->version.'/me?access_token='.$data['access_token'].'&fields='.implode(',', $this->fields);
        if (! empty($this->clientSecret)) {
            $appSecretProof = hash_hmac('sha256', $data['access_token'], $this->clientSecret);
            $meUrl .= '&appsecret_proof='.$appSecretProof;
        }
        $response = $this->getHttpClient()->get($meUrl, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);
        return array_merge(json_decode($response->getBody(), true), $data);
    }
    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $data)
    {
        $explodedName = explode(' ', $data['name']);
        $user= new User($data['access_token'],
            $data['access_token'],
            $data['expires'],
            $data['id'],
            $explodedName[0],
            $explodedName[1],
            $data['email'] ? $data['email'] : null
        );
        $user->setProviderType('facebook');
        
        return $user;
        
        

        /*
        $avatarUrl = $this->graphUrl.'/'.$this->version.'/'.$data['id'].'/picture';
        $user = (new User)->setRaw($data)->map([
            'id' => $data['id'], 'nickname' => null, 'name' => isset($data['name']) ? $data['name'] : null,
            'email' => isset($data['email']) ? $data['email'] : null, 'avatar' => $avatarUrl.'?type=normal',
            'avatar_original' => $avatarUrl.'?width=1920',
            'profileUrl' => isset($data['link']) ? $data['link'] : null,
        ]);
        $user->token = $data['access_token'];
        $user->expiresIn = $data['expires'];

        $explodedName = explode(' ', $data['name']);
        
        $user->setFirstname($explodedName[0]);
        $user->setLastname($explodedName[1]);

        return $user;
        */
    }

    protected function getScopes()
    {
        return $this->scopes;
    }

    /**
     * {@inheritdoc}
     */
    protected function getCodeFields($state = null)
    {
        $fields = parent::getCodeFields($state);
        if ($this->popup) {
            $fields['display'] = 'popup';
        }
        if ($this->reRequest) {
            $fields['auth_type'] = 'rerequest';
        }
        return $fields;
    }
    /**
     * Set the user fields to request from Facebook.
     *
     * @param  array  $fields
     * @return $this
     */
    public function fields(array $fields)
    {
        $this->fields = $fields;
        return $this;
    }
    /**
     * Set the dialog to be displayed as a popup.
     *
     * @return $this
     */
    public function asPopup()
    {
        $this->popup = true;
        return $this;
    }
    /**
     * Re-request permissions which were previously declined.
     *
     * @return $this
     */
    public function reRequest()
    {
        $this->reRequest = true;
    }
}